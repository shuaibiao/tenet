package com.yjb.tenet.boot.framework.mybatis.plus.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.DataPermissionInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.OptimisticLockerInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import com.yjb.tenet.boot.framework.mybatis.plus.core.datascope.DataScopeAspect;
import com.yjb.tenet.boot.framework.mybatis.plus.core.datascope.DataScopePermissionHandler;
import com.yjb.tenet.boot.framework.mybatis.plus.core.handler.AutoFillMetaObjectHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/21 16:06
 * @version 1.0
 */
@Configuration
public class MybatisPlusConfiguration {

	/**
	 * mp 插件
	 * @return
	 */
	@Bean
	public MybatisPlusInterceptor mybatisPlusInterceptor() {
		MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();

		// 数据权限插件
		DataPermissionInterceptor dataPermissionInterceptor = new DataPermissionInterceptor();
		dataPermissionInterceptor.setDataPermissionHandler(new DataScopePermissionHandler());
		interceptor.addInnerInterceptor(dataPermissionInterceptor);
		// 分页插件，自动计算总数并分页
		interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MYSQL));
		// 乐观锁插件
		interceptor.addInnerInterceptor(new OptimisticLockerInnerInterceptor());
		return interceptor;
	}

	@Bean
	public DataScopeAspect dataScopeAspect(){
		return new DataScopeAspect();
	}

	@Bean
	public AutoFillMetaObjectHandler autoFillMetaObjectHandler(){
		return new AutoFillMetaObjectHandler();
	}
}
