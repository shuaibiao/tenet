package com.yjb.tenet.boot.framework.mybatis.plus.core.enums;

import lombok.Getter;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/25 9:19
 * @version 1.0
 */
@Getter
public enum DataScopeEnum {

	DATA_SCOPE_ALL("0","全部"),
	DATA_SCOPE_DEPT("1","本级"),
	DATA_SCOPE_CHILD("2","下级"),
	DATA_SCOPE_DEPT_AND_CHILD("3","本级及下级"),
	DATA_SCOPE_SELF("4","本人"),
	DATA_SCOPE_CUSTOM("5","自定义"),
	;

	private final String value;

	private final String desc;

	DataScopeEnum(String value, String desc) {
		this.value = value;
		this.desc = desc;
	}

}
