package com.yjb.tenet.boot.system.infrastructure.util;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * mp生成器
 * 生成的代码在src目录下,需要手动挪到对应目录
 * 使用教程 https://mp.baomidou.com/guide/wrapper.html
 */
class CodeGeneratorUtil {

    public static void main(String[] args) {
		FastAutoGenerator.create("jdbc:mysql://49.234.219.109:3306/tenet?serverTimezone=GMT%2B8&useUnicode=true&characterEncoding=utf-8&useSSL=false", "root", "permission_2022..")
				.globalConfig(builder -> {
					builder.author("yin.jinbiao") // 设置作者
							//.enableSwagger() // 开启 swagger 模式
							.fileOverride() // 覆盖已生成文件
							.outputDir("D://"); // 指定输出目录
				})
				.packageConfig(builder -> {
					builder.parent("com.yjb.tenet.boot.biz") // 设置父包名
							.moduleName("") // 设置父包模块名
							.pathInfo(Collections.singletonMap(OutputFile.mapperXml, "D://mapper")); // 设置mapperXml生成路径
				})
				.strategyConfig(builder -> {
					builder.addInclude("biz_process_leave") // 设置需要生成的表名
							.addTablePrefix(); // 设置过滤表前缀
				})
				.templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
				.execute();
	}

}


