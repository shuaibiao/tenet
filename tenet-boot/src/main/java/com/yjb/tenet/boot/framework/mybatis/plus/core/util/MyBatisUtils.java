package com.yjb.tenet.boot.framework.mybatis.plus.core.util;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.pojo.PageParam;

import java.util.ArrayList;
import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/21 10:49
 * @version 1.0
 */
public class MyBatisUtils {

	/**
	 * 构造分页
	 * @param pageParam
	 * @param <T>
	 * @return
	 */
	public static <T> Page<T> buildPage(PageParam pageParam) {
		// 页码 + 数量
		if(pageParam.getCurrent()==null){
			pageParam.setCurrent(1);
		}
		if(pageParam.getSize()==null){
			pageParam.setSize(10);
		}
		Page<T> page = new Page<>(pageParam.getCurrent(), pageParam.getSize());
		return page;
	}

	/**
	 * 将拦截器添加到链中
	 * 由于 MybatisPlusInterceptor 不支持添加拦截器，所以只能全量设置
	 *
	 * @param interceptor 链
	 * @param inner 拦截器
	 * @param index 位置
	 */
	public static void addInterceptor(MybatisPlusInterceptor interceptor, InnerInterceptor inner, int index) {
		List<InnerInterceptor> inners = new ArrayList<>(interceptor.getInterceptors());
		inners.add(index, inner);
		interceptor.setInterceptors(inners);
	}

}
