package com.yjb.tenet.boot.system.domain.vo;

import lombok.Data;

import java.util.Set;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/20 11:36
 * @version 1.0
 */
@Data
public class SysRolePageVO {

	private Long id;

	private String roleCode;

	private String roleName;

	private String remark;

	private String dataScope;

	private Set<Long> dataScopeDeptIds;
}
