package com.yjb.tenet.boot.framework.mybatis.plus.core.datascope;

import java.lang.annotation.*;

/**
 * @author yinjinbiao
 * @version 1.0
 * @description
 * @create 2022/5/27 13:47
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface DataScope {

	/**
	 * 部门表的别名
	 */
	String deptAlias() default "";

	/**
	 * 用户表的别名
	 */
	String userAlias() default "";
}
