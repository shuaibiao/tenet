package com.yjb.tenet.boot.system.application.manager;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.framework.mybatis.plus.core.util.MyBatisUtils;
import com.yjb.tenet.boot.system.application.checker.SysRoleChecker;
import com.yjb.tenet.boot.system.application.convert.SysRoleConvert;
import com.yjb.tenet.boot.system.application.service.SysRoleMenuService;
import com.yjb.tenet.boot.system.application.service.SysRoleService;
import com.yjb.tenet.boot.system.domain.dto.CheckMsg;
import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRole;
import com.yjb.tenet.boot.system.domain.entity.SysRoleMenu;
import com.yjb.tenet.boot.system.domain.form.SysRoleAddForm;
import com.yjb.tenet.boot.system.domain.form.SysRoleEditForm;
import com.yjb.tenet.boot.system.domain.query.SysRolePageQuery;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/20 11:47
 * @version 1.0
 */
@Component
public class SysRoleManager {

	@Resource
	private SysRoleService sysRoleService;

	@Resource
	private SysRoleMenuService sysRoleMenuService;

	@Resource
	private SysRoleChecker sysRoleChecker;

	/**
	 * 分页
	 * @param query
	 * @return
	 */
	public PageResult<SysRolePageDTO> page(SysRolePageQuery query){
		IPage<SysRole> page = MyBatisUtils.buildPage(query);
		sysRoleService.page(page, Wrappers.<SysRole>query().lambda()
				.eq(SysRole::getDeleted, GlobalConstants.NOT_DELETED)
				.eq(StrUtil.isNotBlank(query.getRoleCode()), SysRole::getRoleCode, query.getRoleCode())
				.like(StrUtil.isNotBlank(query.getRoleName()), SysRole::getRoleName, query.getRoleName())
		);
		return new PageResult<SysRolePageDTO>(SysRoleConvert.INSTANCE.convert(page.getRecords()), page.getTotal());
	}

	/**
	 * 新增角色
	 * @param sysRoleAddForm
	 * @return
	 */
	public boolean save(SysRoleAddForm sysRoleAddForm){
		SysRole sysRole = SysRoleConvert.INSTANCE.convert(sysRoleAddForm);
		return sysRoleService.save(sysRole);
	}

	/**
	 * 删除
	 * @param id
	 * @return
	 */
	public boolean delete(Long id){
		SysRole sysRole = sysRoleChecker.checkForOperate(id);
		sysRole.setDeleted(id);
		return sysRoleService.updateById(sysRole);
	}

	/**
	 * 编辑用户
	 * @param sysRoleEditForm
	 * @return
	 */
	public boolean edit(SysRoleEditForm sysRoleEditForm){
		sysRoleChecker.checkForOperate(sysRoleEditForm.getId());
		SysRole sysRole = SysRoleConvert.INSTANCE.convert(sysRoleEditForm);
		return sysRoleService.updateById(sysRole);
	}

	/**
	 * 校验角色是否存在
	 * @param roleCode
	 * @return
	 */
	public CheckMsg checkRoleCode(String roleCode){
		CheckMsg msg = new CheckMsg();
		SysRole one = sysRoleService.lambdaQuery().eq(SysRole::getDeleted, GlobalConstants.NOT_DELETED)
				.eq(SysRole::getRoleCode, roleCode)
				.one();
		if(ObjectUtil.isNull(one)){
			msg.setPass(true);
		}
		return msg;
	}

	/**
	 * 查询所有角色
	 * @return
	 */
	public List<SysRolePageDTO> list() {
		List<SysRole> list = sysRoleService.lambdaQuery().eq(SysRole::getDeleted, GlobalConstants.NOT_DELETED).list();
		return SysRoleConvert.INSTANCE.convert(list);
	}

	/**
	 * 设置数据权限
	 * @param roleId
	 * @param dataScope
	 * @param DeptIds
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setDataScope(Long roleId, String dataScope, Set<Long> DeptIds) {
		SysRole sysRole = sysRoleChecker.checkForOperate(roleId);
		sysRole.setDataScope(dataScope);
		sysRole.setDataScopeDeptIds(DeptIds);
		return sysRoleService.updateById(sysRole);
	}

	/**
	 * 设置功能权限
	 * @param roleId
	 * @param menuIds
	 * @return
	 */
	@Transactional(rollbackFor = Exception.class)
	public boolean setFunctionScope(Long roleId, List<Long> menuIds){
		sysRoleMenuService.remove(Wrappers.<SysRoleMenu>query().lambda().eq(SysRoleMenu::getRoleId, roleId));
		List<SysRoleMenu> roleMenuList = menuIds.stream().map(menuId -> {
			SysRoleMenu roleMenu = new SysRoleMenu();
			roleMenu.setRoleId(roleId);
			roleMenu.setMenuId(menuId);
			return roleMenu;
		}).collect(Collectors.toList());
		return sysRoleMenuService.saveBatch(roleMenuList);
	}

}
