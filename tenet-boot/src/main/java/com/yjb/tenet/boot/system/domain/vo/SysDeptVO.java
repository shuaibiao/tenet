package com.yjb.tenet.boot.system.domain.vo;

import lombok.Data;

/**
 * 部门
 */
@Data
public class SysDeptVO {

    private Long deptId;

    /** 父级ID */
    private Long parentId;

    /** 部门名称 */
    private String deptName;

	/** 部门负责人账号 */
	private String deptLeader;

    /** 备注 */
    private String remark;

    /** 排序 */
    private Long orderNo;

    private String deptAlias;


}
