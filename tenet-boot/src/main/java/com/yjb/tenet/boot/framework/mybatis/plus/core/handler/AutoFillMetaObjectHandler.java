package com.yjb.tenet.boot.framework.mybatis.plus.core.handler;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.yjb.tenet.boot.framework.satoken.core.utils.SecurityFrameworkUtils;
import org.apache.ibatis.reflection.MetaObject;

/**
 * @Description MyBatis-Plus 自动填充
 * @Author yin.jinbiao
 * @Date 2021/3/29 10:04
 * @Version 1.0
 */
public class AutoFillMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
    	if(ObjectUtil.isNotEmpty(SecurityFrameworkUtils.getLoginUser())){
		    this.strictInsertFill(metaObject, "createBy", String.class, SecurityFrameworkUtils.getLoginUser().getUsername());
		    this.strictInsertFill(metaObject, "updateBy", String.class, SecurityFrameworkUtils.getLoginUser().getUsername());
		    this.strictInsertFill(metaObject, "deptId", Long.class, SecurityFrameworkUtils.getLoginUser().getDeptId());
		    this.strictInsertFill(metaObject, "userId", Long.class, SecurityFrameworkUtils.getLoginUser().getId());
	    }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
	    // 修改人
	    if(ObjectUtil.isNotEmpty(SecurityFrameworkUtils.getLoginUser())){
		    this.strictUpdateFill(metaObject, "updateBy", String.class, SecurityFrameworkUtils.getLoginUser().getUsername());
	    }
	}

}
