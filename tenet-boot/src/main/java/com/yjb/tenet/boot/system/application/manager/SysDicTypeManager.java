package com.yjb.tenet.boot.system.application.manager;

import com.baomidou.mybatisplus.core.toolkit.ObjectUtils;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.system.application.checker.SysDicTypeChecker;
import com.yjb.tenet.boot.system.application.convert.SysDicTypeConvert;
import com.yjb.tenet.boot.system.application.service.SysDicDataService;
import com.yjb.tenet.boot.system.application.service.SysDicTypeService;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeEditForm;
import com.yjb.tenet.boot.system.infrastructure.constant.SysRedisKeyConstants;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

/**
 * 字典
 * @Author 贾栋栋
 * @Date 2023/3/7 16:49
 **/
@Component
public class SysDicTypeManager {

    @Resource
    private SysDicTypeService sysDicTypeService;

    @Resource
    private SysDicTypeChecker sysDicTypeChecker;

    @Resource
    private RedisTemplate redisTemplate;

    @Resource
    private SysDicDataService sysDicDataService;


    /**
     * 新增字典类型
     */
    @Transactional(rollbackFor = Exception.class)
    public void addType(SysDicTypeAddForm sysDicAddForm) {
        sysDicTypeChecker.checkForExist(sysDicAddForm.getDicCode());
        sysDicTypeService.save(SysDicTypeConvert.INSTANCE.convert(sysDicAddForm));

        redisTemplate.delete(SysRedisKeyConstants.SYS_DIC_KEY_ALL);
    }

    /**
     * 修改字典类型
     */
    @Transactional(rollbackFor = Exception.class)
    public void editType(SysDicTypeEditForm sysDicEditForm) {
        SysDicType byId = sysDicTypeService.getById(sysDicEditForm.getId());

        SysDicType convert = SysDicTypeConvert.INSTANCE.convert(sysDicEditForm);
        sysDicTypeService.updateById(convert);

        redisTemplate.delete(SysRedisKeyConstants.SYS_DIC_KEY_ALL);
        redisTemplate.delete(SysRedisKeyConstants.SYS_DIC_KEY_ITEM + byId.getDicCode());
    }

    /**
     * 删除字典
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteType(Long id) {
        SysDicType one = sysDicTypeService.getById(id);

        boolean result = false;

        if (ObjectUtils.isNotNull(one)){
            one.setDeleted(one.getId());
            sysDicTypeService.updateById(one);
            // 删除字典项
            List<SysDicData> SysDicDataList = sysDicDataService.lambdaQuery()
                    .eq(SysDicData::getDeleted, GlobalConstants.NOT_DELETED)
                    .eq(SysDicData::getDicCode, one.getDicCode()).list();

            SysDicDataList.forEach(item -> item.setDeleted(item.getId()));
            sysDicDataService.updateBatchById(SysDicDataList);

            // 删除缓存
            redisTemplate.delete(SysRedisKeyConstants.SYS_DIC_KEY_ALL);
            result = redisTemplate.delete(SysRedisKeyConstants.SYS_DIC_KEY_ITEM + one.getDicCode());

        }

        return result;
    }
}
