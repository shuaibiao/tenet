package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典项/字典数据
 * @Author 贾栋栋
 * @Date 2023/3/8 14:12
 **/
@Data
public class SysDicDataEditForm {

    @NotNull(message = "id不能为空")
    private Long id;

    @NotBlank(message = "字典标签不能为空")
    @Length(max = 50, message = "字典标签不能超出50字符")
    private String dicLabel;

    @NotBlank(message = "字典值不能为空")
    @Length(max = 50, message = "字典值不能超出50字符")
    private String dicValue;

    @NotNull(message = "序号不能为空")
    private Long orderNo;

    @Length(max = 200, message = "备注不能超出200字符")
    private String remark;

}
