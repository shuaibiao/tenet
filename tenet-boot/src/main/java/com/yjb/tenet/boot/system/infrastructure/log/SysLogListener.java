package com.yjb.tenet.boot.system.infrastructure.log;


import com.yjb.tenet.boot.system.application.service.SysLogService;
import com.yjb.tenet.boot.system.domain.entity.SysLog;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 异步监听日志事件
 */
@Slf4j
@Component
public class SysLogListener {

	@Resource
	private SysLogService sysLogService;

	@Async
	@Order
	@EventListener(SysLogEvent.class)
	public void saveSysLog(SysLogEvent event) {
		SysLog sysLog = (SysLog) event.getSource();
		sysLogService.save(sysLog);
	}

}
