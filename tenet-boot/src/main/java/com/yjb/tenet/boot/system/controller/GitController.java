package com.yjb.tenet.boot.system.controller;

import cn.dev33.satoken.annotation.SaIgnore;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.infrastructure.util.GitUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author yjb
 * @create 2025/1/6 16:42
 */
@RestController
@RequestMapping("/git")
public class GitController {

    @Resource
    private GitUtil gitUtil;

    /**
     * 获取当前部署版本
     * @return
     */
    @SaIgnore
    @GetMapping("/version")
    public ResponseResult version(){
        Map<String, String> gitVersion = new LinkedHashMap<>();
        gitVersion.put("GIT分支", gitUtil.getBranch());
        gitVersion.put("GIT版本", gitUtil.getGitCommitId());
        gitVersion.put("提交日期", gitUtil.getCommitTime());
        gitVersion.put("构建日期", gitUtil.getBuildDate());
        gitVersion.put("构建版本", gitUtil.getBuildVersion());
        gitVersion.put("GIT地址", gitUtil.getGitUrl());
        return ResponseResult.ok(gitVersion);
    }

}
