package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysDept;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zpc
 * @since 2022-05-20
 */
public interface SysDeptMapper extends BaseMapper<SysDept> {

}
