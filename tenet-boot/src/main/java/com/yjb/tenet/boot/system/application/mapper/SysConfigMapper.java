package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysConfig;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zpc
 * @since 2022-05-25
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {

}
