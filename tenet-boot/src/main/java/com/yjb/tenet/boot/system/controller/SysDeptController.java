package com.yjb.tenet.boot.system.controller;


import cn.hutool.core.util.StrUtil;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.manager.SysDeptManager;
import com.yjb.tenet.boot.system.domain.form.SysDeptAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDeptEditForm;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 部门管理
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController {

    @Resource
    private SysDeptManager sysDeptManager;

    /**
     * 部门树
     */
    @GetMapping("/tree")
    public ResponseResult tree(String name){
        if (StrUtil.isNotBlank(name)) {
            return ResponseResult.ok(sysDeptManager.list(name));
        }
        return ResponseResult.ok(sysDeptManager.treeDept());
    }

    /**
     * 新增部门
     */
    @Log("新增部门")
    @PostMapping
    public ResponseResult add(@Valid @RequestBody SysDeptAddForm form){
        return ResponseResult.ok(sysDeptManager.save(form));
    }
    /**
     * 修改部门
     */
    @Log("修改部门")
    @PutMapping
    public ResponseResult edit(@Valid @RequestBody SysDeptEditForm form){
        return ResponseResult.ok(sysDeptManager.edit(form));
    }
    /**
     * 删除部门
     */
    @Log("删除部门")
    @DeleteMapping("/{id:\\d+}")
    public ResponseResult delete(@PathVariable Long id){
        return ResponseResult.ok(sysDeptManager.delete(id));
    }

}
