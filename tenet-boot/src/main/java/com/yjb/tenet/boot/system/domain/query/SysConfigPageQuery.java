package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;

/**
 * @Author 贾栋栋
 * @Date 2023/6/9 9:24
 **/
@Data
public class SysConfigPageQuery extends PageParam {

    private String configName;
}
