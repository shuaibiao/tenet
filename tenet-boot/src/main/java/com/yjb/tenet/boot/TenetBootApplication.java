package com.yjb.tenet.boot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @description 
 * @author yinjinbiao
 * @create 2021/12/15 14:17
 * @version 1.0
 */
@EnableAsync
@SpringBootApplication
@MapperScan("com.yjb.tenet.boot.**.application.mapper")
@EnableAspectJAutoProxy(proxyTargetClass = true, exposeProxy = true)
public class TenetBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(TenetBootApplication.class, args);
	}

}
