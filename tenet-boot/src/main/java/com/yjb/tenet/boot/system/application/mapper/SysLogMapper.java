package com.yjb.tenet.boot.system.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-23
 */
public interface SysLogMapper extends BaseMapper<SysLog> {

}
