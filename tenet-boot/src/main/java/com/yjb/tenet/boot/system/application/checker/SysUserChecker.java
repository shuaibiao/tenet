package com.yjb.tenet.boot.system.application.checker;

import cn.hutool.core.util.ObjectUtil;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.enums.DataTypeEnum;
import com.yjb.tenet.boot.common.exception.CustomException;
import com.yjb.tenet.boot.common.exception.enums.ErrorCodeEnum;
import com.yjb.tenet.boot.system.application.service.SysUserService;
import com.yjb.tenet.boot.system.domain.entity.SysUser;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/23 17:12
 * @version 1.0
 */
@Component
public class SysUserChecker {

	@Resource
	private SysUserService sysUserService;

	public void checkForExist(Long id, String username){
		SysUser one = sysUserService.lambdaQuery()
				.eq(SysUser::getDeleted, GlobalConstants.NOT_DELETED)
				.eq(SysUser::getUsername, username)
				.ne(ObjectUtil.isNotNull(id), SysUser::getId, id)
				.one();
		if(ObjectUtil.isNotEmpty(one)){
			throw new CustomException(ErrorCodeEnum.BUSINESS_FAIL.getCode(), "用户名已存在！");
		}
	}



	public SysUser checkForOperate(Long userId){
		SysUser sysUser = sysUserService.getById(userId);
		if(DataTypeEnum.SYSTEM.getValue().equals(sysUser.getType())){
			throw new CustomException(ErrorCodeEnum.PERMISSION_DENY.getCode(), "内置用户不允许操作！");
		}
		return sysUser;
	}
}
