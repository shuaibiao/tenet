package com.yjb.tenet.boot.common.constants;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/16 9:30
 * @version 1.0
 */
public class GlobalConstants {

	/**
	 * 未删除标志
	 */
	public static final Long NOT_DELETED = 0L;
}
