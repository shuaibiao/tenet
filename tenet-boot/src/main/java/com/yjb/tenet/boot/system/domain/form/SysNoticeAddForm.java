package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author zpc
 * @version 1.0
 * @description 系统通知新增表单
 * @create 2022/6/2 14:20
 */
@Data
public class SysNoticeAddForm {

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空")
    @Length(max = 50,message = "标题不能超过50个字符")
    private String title;

    /**
     * 作者姓名
     */
    @NotBlank(message = "作者姓名不能为空")
    @Length(max = 20,message = "作者姓名不能超过20个字符")
    private String author;

    /**
     * 内容
     */
    @NotBlank(message = "内容不能为空")
    private String content;

    /**
     * 封面
     */
    @NotBlank(message = "封面不能为空")
    @Length(max = 200,message = "封面不能超过20个字符")
    private String cover;

    /**
     * 摘要
     */
    @Length(max = 200,message = "摘要不能超过200个字符")
    private String digest;

    /**
     * 是否发布
     */
    private String published;
}
