package com.yjb.tenet.boot.system.controller;

import cn.hutool.core.util.StrUtil;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.manager.SysMenuManager;
import com.yjb.tenet.boot.system.domain.form.SysMenuAddForm;
import com.yjb.tenet.boot.system.domain.form.SysMenuEditForm;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 菜单管理
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RequestMapping("/system/menu")
@RestController
public class SysMenuController {

	@Resource
	private SysMenuManager sysMenuManager;

	/**
	 * 获取所有菜单
	 * @return
	 */
	@GetMapping(value = "/tree")
	public ResponseResult getTree(String name) {
		if(StrUtil.isNotBlank(name)){
			return ResponseResult.ok(sysMenuManager.getTreeNode(name));
		}
		return ResponseResult.ok(sysMenuManager.treeMenu());
	}

	/**
	 * 保存菜单
	 * @param sysMenuAddForm
	 * @return
	 */
	@Log("新增菜单")
	@PostMapping
	public ResponseResult save(@Valid @RequestBody SysMenuAddForm sysMenuAddForm){
		sysMenuManager.save(sysMenuAddForm);
		return ResponseResult.ok();
	}

	/**
	 * 编辑菜单
	 * @param sysMenuEditForm
	 * @return
	 */
	@Log("编辑菜单")
	@PutMapping
	public ResponseResult edit(@Valid @RequestBody SysMenuEditForm sysMenuEditForm){
		sysMenuManager.edit(sysMenuEditForm);
		return ResponseResult.ok();
	}

	/**
	 * 删除菜单
	 * @param id
	 * @return
	 */
	@Log("删除菜单")
	@DeleteMapping("/{id:\\d+}")
	public ResponseResult delete(@PathVariable Long id){
		sysMenuManager.delete(id);
		return ResponseResult.ok();
	}

	/**
	 * 获取我的菜单树
	 * @return
	 */
	@GetMapping("/me")
	public ResponseResult getMyMenu(){
		return ResponseResult.ok(sysMenuManager.getMyMenu());
	}
}
