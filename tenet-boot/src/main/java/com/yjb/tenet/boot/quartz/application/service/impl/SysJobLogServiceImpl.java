package com.yjb.tenet.boot.quartz.application.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.quartz.application.mapper.SysJobLogMapper;
import com.yjb.tenet.boot.quartz.application.service.SysJobLogService;
import com.yjb.tenet.boot.quartz.domain.entity.SysJobLog;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 定时任务调度日志表 服务实现类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
@Service
public class SysJobLogServiceImpl extends ServiceImpl<SysJobLogMapper, SysJobLog> implements SysJobLogService {

}
