package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @description 账号密码登录表单
 * @author yinjinbiao
 * @create 2021/12/15 17:25
 * @version 1.0
 */
@Data
public class UsernamePasswordLoginForm {

	@NotBlank(message = "账号不能为空")
	private String username;

	@NotBlank(message = "密码不能为空")
	private String password;

}
