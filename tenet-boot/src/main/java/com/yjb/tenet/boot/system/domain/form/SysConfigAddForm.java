package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * @author zpc
 * @version 1.0
 * @description 系统配置新增表单
 * @create 2022/5/25 16:02
 */
@Data
public class SysConfigAddForm {

    @NotBlank(message = "配置名称不能为空")
    @Length(max = 20,message = "不能超过20个字符")
    private String configName;

    @NotBlank(message = "key不能为空")
    @Length(max = 50,message = "不能超过50个字符")
    private String configKey;

    @NotBlank(message = "value不能为空")
    @Length(max = 500,message = "不能超过500个字符")
    private String configValue;

    private String configSys;

    @Length(max = 200,message = "不能超过200个字符")
    private String remark;
}
