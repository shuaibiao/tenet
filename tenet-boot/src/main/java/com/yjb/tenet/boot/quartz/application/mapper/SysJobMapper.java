package com.yjb.tenet.boot.quartz.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.quartz.domain.entity.SysJob;
import com.yjb.tenet.boot.quartz.domain.dto.SysJobPageDTO;
import com.yjb.tenet.boot.quartz.domain.query.SysJobPageQuery;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 定时任务调度表 Mapper 接口
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
public interface SysJobMapper extends BaseMapper<SysJob> {

	IPage<SysJobPageDTO> page(Page<SysJobPageDTO> page, @Param("query") SysJobPageQuery param);
}
