package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysRoleMenuMapper;
import com.yjb.tenet.boot.system.application.service.SysRoleMenuService;
import com.yjb.tenet.boot.system.domain.entity.SysRoleMenu;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-15
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
