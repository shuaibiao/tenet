package com.yjb.tenet.boot.common.constants;

/**
 * 区域常量
 * @Author 贾栋栋
 * @Date 2023/3/10 9:39
 **/
public class SysAreaConstants {

    /** 树根节点 ID */
    public static final Long TREE_ROOT_ID = 0L;

}
