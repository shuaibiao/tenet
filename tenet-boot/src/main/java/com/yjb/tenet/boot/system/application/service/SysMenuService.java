package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysMenu;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-15
 */
public interface SysMenuService extends IService<SysMenu> {

}
