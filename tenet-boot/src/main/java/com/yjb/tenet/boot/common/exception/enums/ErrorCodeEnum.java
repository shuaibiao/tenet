package com.yjb.tenet.boot.common.exception.enums;

import com.yjb.tenet.boot.common.exception.ErrorCode;
import lombok.Getter;

/**
 * @Description 错误码
 * @Author yin.jinbiao
 * @Date 2021/9/27 14:31
 * @Version 1.0
 */
@Getter
public enum ErrorCodeEnum implements ErrorCode {

    /**
     * 接口调用成功
     */
    SUCCESS("10000","请求成功！"),
    /**
     * 服务不可用
     */
    UNKNOWN_ERROR("20000","未知错误！"),
    /**
     * 授权权限不足
     */
    UNKNOWN_PERMISSION("20001","未知权限！"),

	/**
	 * 未登录
	 */
	UNAUTHORIZED("30000", "登录状态失效"),

    /**
     * 缺少必选参数
     */
    PARAM_MISS("40001","参数缺失！"),
    /**
     * 非法的参数
     */
    PARAM_ILLEGAL("40002","参数非法！"),
    /**
     * 业务处理失败
     */
    BUSINESS_FAIL("40004","业务处理失败！"),
    /**
     * 权限不足
     */
    PERMISSION_DENY("40006","权限不足！"),
    /**
     * 调用频次超限
     */
    CALL_LIMITED("40005","调用频次超限！")
    ;

    private String code;
    private String msg;

    ErrorCodeEnum(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

}
