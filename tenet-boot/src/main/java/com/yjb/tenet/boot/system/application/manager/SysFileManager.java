package com.yjb.tenet.boot.system.application.manager;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.exception.CustomException;
import com.yjb.tenet.boot.common.exception.enums.ErrorCodeEnum;
import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.utils.FileUtils;
import com.yjb.tenet.boot.framework.file.core.client.FileClient;
import com.yjb.tenet.boot.framework.file.core.utils.FileTypeUtils;
import com.yjb.tenet.boot.framework.mybatis.plus.core.util.MyBatisUtils;
import com.yjb.tenet.boot.system.application.service.SysFileService;
import com.yjb.tenet.boot.system.domain.entity.SysFile;
import com.yjb.tenet.boot.system.domain.query.SysFilePageQuery;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/6/8 17:45
 * @version 1.0
 */
@Component
public class SysFileManager {

	@Resource
	private SysFileService sysFileService;

	@Resource
	private FileClient fileClient;

	@SneakyThrows
	@Transactional(rollbackFor = Exception.class)
	public String createFile(String name, String path, byte[] content) {
		// 计算默认的 path 名
		String type = FileTypeUtils.getMineType(content, name);
		if (StrUtil.isEmpty(path)) {
			path = FileUtils.generatePath(content, name);
		}
		// 如果 name 为空，则使用 path 填充
		if (StrUtil.isEmpty(name)) {
			name = path;
		}

		// 上传到文件存储器
		String url = fileClient.upload(content, path, type);

		// 保存到数据库
		SysFile file = new SysFile();
		file.setName(name);
		file.setPath(path);
		file.setUrl(url);
		file.setType(type);
		file.setSize(content.length);
		sysFileService.save(file);
		return url;
	}

	public void deleteFile(Long id) throws Exception {
		// 校验存在
		SysFile file = sysFileService.getById(id);
		if(ObjectUtil.isNull(file)){
			throw new CustomException(ErrorCodeEnum.PARAM_MISS.getCode(), "文件不存在！");
		}

		// 从文件存储器中删除
		fileClient.delete(file.getPath());

		// 删除记录
		sysFileService.removeById(id);
	}

	public byte[] getFileContent(String path) throws Exception {
		return fileClient.getContent(path);
	}

	public PageResult<SysFile> page(SysFilePageQuery query) {
		IPage<SysFile> page = MyBatisUtils.buildPage(query);
		sysFileService.page(page, Wrappers.<SysFile>query().lambda()
				.eq(SysFile::getDeleted, GlobalConstants.NOT_DELETED)
				.like(StrUtil.isNotBlank(query.getName()), SysFile::getName, query.getName())
				.ge(ObjectUtil.isNotNull(query.getCreateTimeStart()), SysFile::getCreateTime, query.getCreateTimeStart())
				.le(ObjectUtil.isNotNull(query.getCreateTimeEnd()), SysFile::getCreateTime, query.getCreateTimeEnd())
				.orderByDesc(SysFile::getCreateTime)
		);
		return new PageResult<>(page.getRecords(), page.getTotal());
	}

}
