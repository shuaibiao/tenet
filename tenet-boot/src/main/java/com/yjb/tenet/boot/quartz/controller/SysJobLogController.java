package com.yjb.tenet.boot.quartz.controller;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.quartz.application.service.SysJobLogService;
import com.yjb.tenet.boot.quartz.domain.entity.SysJobLog;
import com.yjb.tenet.boot.quartz.domain.query.SysJobLogPageQuery;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 定时任务日志
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/joblog")
public class SysJobLogController {

	@Resource
	private SysJobLogService sysJobLogService;

	/**
	 * 分页查询
	 * @param page
	 * @param query
	 * @return
	 */
	@GetMapping("/page")
	public ResponseResult page(Page<SysJobLog> page, SysJobLogPageQuery query){
		return ResponseResult.ok(sysJobLogService.page(page, Wrappers.<SysJobLog>query().lambda()
				.like(StrUtil.isNotBlank(query.getJobName()), SysJobLog::getJobName, query.getJobName())
				.eq(StrUtil.isNotBlank(query.getJobGroup()), SysJobLog::getJobGroup, query.getJobGroup())
				.eq(StrUtil.isNotBlank(query.getStatus()), SysJobLog::getStatus, query.getStatus())
				.ge(ObjectUtil.isNotNull(query.getBeginDate()), SysJobLog::getCreateTime, query.getBeginDate())
				.le(ObjectUtil.isNotNull(query.getEndDate()), SysJobLog::getCreateTime, query.getEndDate())
				.orderByDesc(SysJobLog::getCreateTime)
		));
	}

}
