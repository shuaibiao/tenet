package com.yjb.tenet.boot.framework.captcha.config;

import com.xingyuv.captcha.service.CaptchaCacheService;
import com.yjb.tenet.boot.framework.captcha.core.service.impl.RedisCaptchaServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.StringRedisTemplate;

/**
 * AJ Captcha
 */
@Configuration
public class CaptchaConfiguration {

    @Bean
    public CaptchaCacheService captchaCacheService(StringRedisTemplate stringRedisTemplate) {
        return new RedisCaptchaServiceImpl(stringRedisTemplate);
    }

}
