package com.yjb.tenet.boot.system.infrastructure.enums;

import lombok.Getter;

/**
 * @Description
 * @Author yin.jinbiao
 * @Date 2022/5/23 21:50
 * @Version 1.0
 */
@Getter
public enum FlagEnums {

    YES("Y","是"),
    NO("N","否");

    private final String value;

    private final String desc;

    FlagEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
