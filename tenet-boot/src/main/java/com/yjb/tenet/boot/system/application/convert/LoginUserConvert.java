package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.framework.satoken.core.LoginUser;
import com.yjb.tenet.boot.system.domain.entity.SysUser;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/18 20:09
 * @version 1.0
 */
@Mapper
public interface LoginUserConvert {

	LoginUserConvert INSTANCE = Mappers.getMapper(LoginUserConvert.class);

	/**
	 * sysUser -> loginUser
	 * @param sysUser
	 * @return
	 */
	LoginUser convert(SysUser sysUser);

/*	*//**
	 * sysUserDTO -> loginUser
	 * @param loginUser
	 * @return
	 *//*
	LoginUserDTO convert(LoginUser loginUser);*/
}
