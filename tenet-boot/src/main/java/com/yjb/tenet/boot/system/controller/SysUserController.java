package com.yjb.tenet.boot.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.manager.SysUserManager;
import com.yjb.tenet.boot.system.domain.dto.SysUserPageDTO;
import com.yjb.tenet.boot.system.domain.form.MyInfoEditForm;
import com.yjb.tenet.boot.system.domain.form.SysUserAddForm;
import com.yjb.tenet.boot.system.domain.form.SysUserEditForm;
import com.yjb.tenet.boot.system.domain.query.SysUserPageQuery;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 用户管理
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/user")
public class SysUserController {

	@Resource
	private SysUserManager sysUserManager;

	/**
	 * 分页查询，带角色信息
	 * @param page
	 * @param query
	 * @return
	 */
	@GetMapping("/page")
	public ResponseResult page(Page<SysUserPageDTO> page, SysUserPageQuery query){
		return ResponseResult.ok(sysUserManager.page(page,query));
	}

	/**
	 * 新增用户，同时指定角色
	 * @param sysUserAddForm
	 * @return
	 */
	@Log("新增用户")
	@PostMapping
	public ResponseResult add(@Valid @RequestBody SysUserAddForm sysUserAddForm){
		sysUserManager.save(sysUserAddForm);
		return ResponseResult.ok();
	}

	/**
	 * 编辑用户，同时指定角色
	 * @param sysUserEditForm
	 * @return
	 */
	@Log("编辑用户")
	@PutMapping
	public ResponseResult edit(@Valid @RequestBody SysUserEditForm sysUserEditForm){
		sysUserManager.edit(sysUserEditForm);
		return ResponseResult.ok();
	}

	/**
	 * 删除用户
	 * @param id
	 * @return
	 */
	@Log("删除用户")
	@DeleteMapping("/{id:\\d+}")
	public ResponseResult delete(@PathVariable Long id){
		sysUserManager.delete(id);
		return ResponseResult.ok();
	}

	/**
	 * 重置密码
	 * @param id
	 * @return
	 */
	@Log("重置密码")
	@PutMapping("/reset/{id:\\d+}")
	public ResponseResult reset(@PathVariable Long id){
		sysUserManager.reset(id);
		return ResponseResult.ok();
	}

	/**
	 * 检查用户名是否存在
	 * @param username
	 * @return
	 */
	@GetMapping("/check_username/{username}")
	public ResponseResult checkUsername(@PathVariable String username){
		return ResponseResult.ok(sysUserManager.checkUsername(username));
	}

	/**
	 * 查询个人信息
	 * @return
	 */
	@GetMapping("/myInfo")
	public ResponseResult myInfo(){
		return ResponseResult.ok(sysUserManager.getMyInfo());
	}

	/**
	 * 修改个人信息，要验证密码
	 * @param myInfoEditForm
	 * @return
	 */
	@PostMapping("/editMyInfo")
	public ResponseResult editMyInfo(@RequestBody @Valid MyInfoEditForm myInfoEditForm){
		return ResponseResult.ok(sysUserManager.editMyInfo(myInfoEditForm));
	}

	/**
	 * 获取部门用户树
	 * @return
	 */
	@GetMapping("/deptUserTree")
	public ResponseResult deptUserTree(){
		return ResponseResult.ok(sysUserManager.deptUserTree());
	}
}
