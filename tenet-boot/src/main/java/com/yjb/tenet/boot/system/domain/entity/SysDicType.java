package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典类型
 * @author YJB
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_type")
public class SysDicType extends BaseEntity {

	@TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 字典名称 */
    private String dicName;

    /** 字典编码 */
    private String dicCode;

    /** 备注 */
    private String remark;

}
