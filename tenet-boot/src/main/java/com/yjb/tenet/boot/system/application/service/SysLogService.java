package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysLog;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-23
 */
public interface SysLogService extends IService<SysLog> {

}
