package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.dto.SysNoticePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysNotice;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
public interface SysNoticeService extends IService<SysNotice> {

    IPage<SysNoticePageDTO> getPage(IPage<SysNoticePageDTO> page, String title);

}
