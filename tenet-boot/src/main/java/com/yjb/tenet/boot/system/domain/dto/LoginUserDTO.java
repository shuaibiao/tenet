package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/18 21:52
 * @version 1.0
 */
@Data
public class LoginUserDTO implements Serializable {

	private Long id;

	private String username;

	private String nickname;

	private Long tenantId;

	private Long deptId;

	private Set<String> roleCodes;

	private Set<String> permissionCodes;

	private Set<String> dataScopes;

	private Set<Long> dataScopesDeptIds;
}
