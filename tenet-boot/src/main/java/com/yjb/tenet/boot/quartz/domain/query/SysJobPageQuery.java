package com.yjb.tenet.boot.quartz.domain.query;

import lombok.Data;

/**
 * @Description
 * @Author yin.jinbiao
 * @Date 2022/5/4 15:25
 * @Version 1.0
 */
@Data
public class SysJobPageQuery {

    private String jobName;

    private String jobGroup;

    private String pause;
}
