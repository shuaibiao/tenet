package com.yjb.tenet.boot.system.infrastructure.enums;

import lombok.Getter;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/21 16:19
 * @version 1.0
 */
@Getter
public enum SysDeptRootEnum {

	ROOT(0L,"部门根节点");

	private final Long value;

	private final String desc;

	SysDeptRootEnum(Long value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
