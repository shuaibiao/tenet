package com.yjb.tenet.boot.system.controller;


import cn.dev33.satoken.annotation.SaIgnore;
import cn.dev33.satoken.stp.SaTokenInfo;
import cn.dev33.satoken.stp.StpUtil;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.manager.LoginManager;
import com.yjb.tenet.boot.system.domain.form.UsernamePasswordLoginForm;
import com.xingyuv.captcha.model.vo.CaptchaVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 登录相关接口
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/auth")
public class LoginController {

	@Resource
	private LoginManager loginManager;

	/**
	 * 账号密码登录
	 * @param form
	 * @param form
	 * @return
	 */
	@SaIgnore
	@PostMapping("/login")
	public ResponseResult<SaTokenInfo> login(@RequestBody UsernamePasswordLoginForm form){

		loginManager.login(form.getUsername(), form.getPassword());
		// 获取 Token  相关参数 返回给前端
		SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
		return ResponseResult.ok(tokenInfo);
	}

	/**
	 * 账号密码+滑块验证码登录
	 * @param form
	 * @param captchaVerification
	 * @return
	 */
	@SaIgnore
	@PostMapping("/captcha-login")
	public ResponseResult<SaTokenInfo> login(@RequestBody UsernamePasswordLoginForm form, @RequestParam("captchaVerification") String captchaVerification){
		CaptchaVO captchaVO = new CaptchaVO();
		captchaVO.setCaptchaVerification(captchaVerification);

		loginManager.login(form.getUsername(), form.getPassword(), captchaVO);
		// 获取 Token  相关参数 返回给前端
		SaTokenInfo tokenInfo = StpUtil.getTokenInfo();
		return ResponseResult.ok(tokenInfo);
	}

	/**
	 * 退出
	 * @return
	 */
	@GetMapping("/logout")
	public ResponseResult logout(){
		StpUtil.logout();
		return ResponseResult.ok();
	}
}
