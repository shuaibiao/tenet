package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysConfig;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zpc
 * @since 2022-05-25
 */
public interface SysConfigService extends IService<SysConfig> {

}
