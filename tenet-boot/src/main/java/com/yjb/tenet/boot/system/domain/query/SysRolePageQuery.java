package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/20 11:35
 * @version 1.0
 */
@Data
public class SysRolePageQuery extends PageParam {

	private String roleCode;

	private String roleName;

}

