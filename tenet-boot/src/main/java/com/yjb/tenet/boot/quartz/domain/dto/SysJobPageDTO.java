package com.yjb.tenet.boot.quartz.domain.dto;

import cn.hutool.core.util.StrUtil;
import com.yjb.tenet.boot.quartz.infrastructure.utils.CronUtils;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/5 14:30
 * @version 1.0
 */
@Data
public class SysJobPageDTO {

	private Long id;

	/** 任务名称 */
	private String jobName;

	/** 任务组名 */
	private String jobGroup;

	/** 调用目标字符串 */
	private String invokeTarget;

	/** cron执行表达式 */
	private String cronExpression;

	/** cron计划策略 0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行*/
	private String misfirePolicy;

	/** 是否并发执行（Y允许 N禁止） */
	private String concurrent;

	/** 任务状态（Y暂停 N正常） */
	private String pause;

	/**
	 * 备注信息
	 */
	private String remark;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	public Date getNextValidTime() {
		if (StrUtil.isNotEmpty(cronExpression)) {
			return CronUtils.getNextExecution(cronExpression);
		}
		return null;
	}
}
