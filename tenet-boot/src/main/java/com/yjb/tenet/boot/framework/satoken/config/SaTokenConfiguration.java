package com.yjb.tenet.boot.framework.satoken.config;

import cn.dev33.satoken.stp.StpInterface;
import com.yjb.tenet.boot.framework.satoken.core.filter.LoginUserWebFilter;
import com.yjb.tenet.boot.framework.satoken.core.init.PermitAllUrlInitializingBean;
import com.yjb.tenet.boot.framework.satoken.core.permission.StpInterfaceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description Sa-Token 权限认证 配置类 ，过滤所有请求，检验 token 的合法性
 *
 * @author yinjinbiao
 * @create 2023/2/17 13:55
 * @version 1.0
 */
@Configuration
public class SaTokenConfiguration {

	@Bean
	public PermitAllUrlInitializingBean permitAllUrlInitializingBean(){
		return new PermitAllUrlInitializingBean();
	}

	/**
	 * 注册 Sa-Token 全局过滤器
	 * @return
	 */
	@Bean
	public LoginUserWebFilter loginUserWebFilter(PermitAllUrlInitializingBean permitAllUrlInitializingBean) {
		return new LoginUserWebFilter(permitAllUrlInitializingBean);
	}

	@Bean
	public StpInterface stpInterface(){
		return new StpInterfaceImpl();
	}

}
