package com.yjb.tenet.boot.system.domain.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/21 16:22
 * @version 1.0
 */
@Data
public class SysDeptPageVO {

	@TableId(value = "dept_id", type = IdType.AUTO)
	private Long deptId;

	private Long parentId;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 部门负责人账号
	 */
	private String deptLeader;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 排序号
	 */
	private Long orderNo;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

}
