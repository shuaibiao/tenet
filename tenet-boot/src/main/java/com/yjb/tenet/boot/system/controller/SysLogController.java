package com.yjb.tenet.boot.system.controller;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.service.SysLogService;
import com.yjb.tenet.boot.system.domain.entity.SysLog;
import com.yjb.tenet.boot.system.domain.query.SysLogPageQuery;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * 日志相关
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/log")
public class SysLogController {

	@Resource
	private SysLogService sysLogService;

	/**
	 * 日志分页
	 * @param page
	 * @param query
	 * @return
	 */
	@GetMapping("/page")
	public ResponseResult page(Page<SysLog> page, SysLogPageQuery query){
		page = sysLogService.page(page, Wrappers.<SysLog>query().lambda()
				.eq(StrUtil.isNotBlank(query.getType()), SysLog::getType, query.getType())
				.ge(ObjectUtil.isNotNull(query.getBeginDate()), SysLog::getCreateTime, query.getBeginDate())
				.le(ObjectUtil.isNotNull(query.getEndDate()), SysLog::getCreateTime, query.getEndDate()));
		return ResponseResult.ok(page);
	}

	/**
	 * 删除日志
	 * @param id
	 * @return
	 */
	@DeleteMapping("/{id}")
	public ResponseResult del(@PathVariable Long id){
		sysLogService.removeById(id);
		return ResponseResult.ok();
	}

}
