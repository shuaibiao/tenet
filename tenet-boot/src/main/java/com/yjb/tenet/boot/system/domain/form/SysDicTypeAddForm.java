package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/7 16:51
 **/
@Data
public class SysDicTypeAddForm {

    @NotBlank(message = "字典名称不能为空")
    @Length(max = 20, message = "字典名称不能超出20字符")
    private String dicName;

    @NotBlank(message = "字典编码不能为空")
    @Length(max = 50, message = "字典编码不能超出50字符")
    private String dicCode;

    @Length(max = 200, message = "备注不能超出200字符")
    private String remark;

}