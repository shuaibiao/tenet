package com.yjb.tenet.boot.system.domain.vo;

import com.yjb.tenet.boot.system.infrastructure.enums.LoginLogTypeEnum;
import com.yjb.tenet.boot.system.infrastructure.enums.LoginResultEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/3/14 16:05
 * @version 1.0
 */
@Data
public class SysLoginLogPageVO {

	/**
	 * 日志主键
	 */
	private Long id;
	/**
	 * 日志类型
	 *
	 * 枚举 {@link LoginLogTypeEnum}
	 */
	private Integer logType;
	/**
	 * 用户账号
	 *
	 * 冗余，因为账号可以变更
	 */
	private String username;
	/**
	 * 用户 IP
	 */
	private String userIp;
	/**
	 * 浏览器 UA
	 */
	private String userAgent;
	/**
	 * 登录结果
	 *
	 * 枚举 {@link LoginResultEnum}
	 */
	private Integer result;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;
}
