package com.yjb.tenet.boot.system.controller;

import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.convert.SysRoleConvert;
import com.yjb.tenet.boot.system.application.manager.SysRoleManager;
import com.yjb.tenet.boot.system.application.service.SysRoleMenuService;
import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRoleMenu;
import com.yjb.tenet.boot.system.domain.form.SysRoleAddForm;
import com.yjb.tenet.boot.system.domain.form.SysRoleDataSopeForm;
import com.yjb.tenet.boot.system.domain.form.SysRoleEditForm;
import com.yjb.tenet.boot.system.domain.form.SysRoleFunctionScopeForm;
import com.yjb.tenet.boot.system.domain.query.SysRolePageQuery;
import com.yjb.tenet.boot.system.domain.vo.SysRolePageVO;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.stream.Collectors;

/**
 * 角色管理
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@Slf4j
@RequestMapping("/system/role")
public class SysRoleController {

    @Resource
    private SysRoleManager sysRoleManager;

	@Resource
	private SysRoleMenuService sysRoleMenuService;


	/**
	 * 分页查询
	 * @param query
	 * @param query
	 * @return
	 */
	@GetMapping("/page")
	public ResponseResult<PageResult<SysRolePageVO>> page(SysRolePageQuery query){
		PageResult<SysRolePageDTO> page = sysRoleManager.page(query);
		return ResponseResult.ok(new PageResult<>(SysRoleConvert.INSTANCE.convert01(page.getRecords()), page.getTotal()));
	}


    /**
     * 新增角色
     * @param sysRoleAddForm
     * @return
     */
    @Log("新增角色")
    @PostMapping
    public ResponseResult add(@Valid @RequestBody SysRoleAddForm sysRoleAddForm){
        sysRoleManager.save(sysRoleAddForm);
        return ResponseResult.ok();
    }

    /**
     * 编辑角色
     * @param sysRoleEditForm
     * @return
     */
	@Log("编辑角色")
    @PutMapping
    public ResponseResult edit(@Valid @RequestBody SysRoleEditForm sysRoleEditForm){
        sysRoleManager.edit(sysRoleEditForm);
        return ResponseResult.ok();
    }

    /**
     * 删除角色
     * @param id
     * @return
     */
	@Log("删除角色")
    @DeleteMapping("/{id:\\d+}")
    public ResponseResult delete(@PathVariable Long id){
        sysRoleManager.delete(id);
        return ResponseResult.ok();
    }

	/**
	 * 检查角色是否存在
	 * @param roleCode
	 * @return
	 */
	@GetMapping("/check_rolecode/{roleCode}")
	public ResponseResult checkUsername(@PathVariable String roleCode){
		return ResponseResult.ok(sysRoleManager.checkRoleCode(roleCode));
	}

	/**
	 * 查询所有角色
	 * @return
	 */
	@GetMapping("/list")
	public ResponseResult list(){
		return ResponseResult.ok(sysRoleManager.list());
	}


	/**
	 * 设置数据权限
	 * @param sysRoleMenuForm
	 * @return
	 */
	@Log("设置数据权限")
	@PostMapping("/scope/data")
	public ResponseResult setFunctionScope(@RequestBody SysRoleDataSopeForm sysRoleMenuForm) {
		sysRoleManager.setDataScope(sysRoleMenuForm.getRoleId(), sysRoleMenuForm.getDataScope(), sysRoleMenuForm.getDeptIds());
		return ResponseResult.ok();
	}

	/**
	 * 查询功能权限
	 * @param roleId 角色ID
	 * @return 属性集合
	 */
	@GetMapping("/scope/function/{roleId}")
	public ResponseResult getFunctionScope(@PathVariable Long roleId) {
		return ResponseResult.ok(sysRoleMenuService.lambdaQuery().eq(SysRoleMenu::getRoleId, roleId).list().stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList()));
	}

	/**
	 * 设置功能权限
	 * @param sysRoleFunctionScopeForm
	 * @return
	 */
	@Log("设置功能权限")
	@PostMapping("/scope/function")
	public ResponseResult setFunctionScope(@RequestBody SysRoleFunctionScopeForm sysRoleFunctionScopeForm) {
		sysRoleManager.setFunctionScope(sysRoleFunctionScopeForm.getRoleId(), sysRoleFunctionScopeForm.getMenuIds());
		return ResponseResult.ok();
	}


}
