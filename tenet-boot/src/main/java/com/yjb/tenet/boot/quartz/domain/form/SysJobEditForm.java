package com.yjb.tenet.boot.quartz.domain.form;


import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @Description
 * @Author yin.jinbiao
 * @Date 2022/5/4 15:55
 * @Version 1.0
 */
@Data
public class SysJobEditForm {

	@NotNull(message = "任务id不能为空")
	private Long id;

	@NotBlank(message = "任务名称不能为空")
	@Size(min = 0, max = 64, message = "任务名称不能超过64个字符")
	private String jobName;

	@NotBlank(message = "任务分组不能为空")
	private String jobGroup;

	@NotBlank(message = "调用目标字符串")
	private String invokeTarget;

	@NotBlank(message = "cron表达式")
	private String cronExpression;

	private String misfirePolicy;

	private String concurrent;

	private String pause;

	@Length(max = 200,message = "不能超过200个字符")
	private String remark;
}
