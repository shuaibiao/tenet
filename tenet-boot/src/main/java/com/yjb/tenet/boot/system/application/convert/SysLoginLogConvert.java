package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.system.domain.dto.SysLoginLogDTO;
import com.yjb.tenet.boot.system.domain.entity.SysLoginLog;
import com.yjb.tenet.boot.system.domain.vo.SysLoginLogPageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/3/14 16:25
 * @version 1.0
 */
@Mapper
public interface SysLoginLogConvert {

	SysLoginLogConvert INSTANCE = Mappers.getMapper(SysLoginLogConvert.class);

	List<SysLoginLogDTO> convertList(List<SysLoginLog> list);

	PageResult<SysLoginLogPageVO> convertPage(PageResult<SysLoginLogDTO> page);
}
