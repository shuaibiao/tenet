package com.yjb.tenet.boot.framework.satoken.core.utils;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.stp.StpUtil;
import com.yjb.tenet.boot.framework.satoken.core.LoginUser;
import org.springframework.lang.Nullable;

/**
 * @description 公共web安全服务工具
 * @author yinjinbiao
 * @create 2023/2/18 23:38
 * @version 1.0
 */
public class SecurityFrameworkUtils {

	/**
	 * redis 缓存 session key 与 线程上下文 key
	 */
	public static final String LOGIN_USER_KEY = "loginUser";

	/**
	 * 获取当前用户
	 *
	 * @return 当前用户
	 */
	@Nullable
	public static LoginUser getLoginUser() {
		Object object = null;
		try {
			object = StpUtil.getSession().get(LOGIN_USER_KEY);
		}catch (NotLoginException e){
			return null;
		}
		if (object == null) {
			return null;
		}
		return object instanceof LoginUser ? (LoginUser) object : null;
	}
}
