package com.yjb.tenet.boot.quartz.domain.form;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/5 9:38
 * @version 1.0
 */
@Data
public class SysJobStatusForm {

	@NotNull(message = "任务id不能为空")
	private Long id;

	@NotBlank(message = "任务状态不能为空")
	private String pause;

}
