package com.yjb.tenet.boot.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.framework.mybatis.plus.core.util.MyBatisUtils;
import com.yjb.tenet.boot.system.application.convert.SysDicTypeConvert;
import com.yjb.tenet.boot.system.application.manager.SysDicTypeManager;
import com.yjb.tenet.boot.system.application.service.SysDicTypeService;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeEditForm;
import com.yjb.tenet.boot.system.domain.query.SysDicTypePageQuery;
import com.yjb.tenet.boot.system.domain.vo.SysDicTypeVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 字典类型相关
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/dic/type")
public class SysDicTypeController {

    @Resource
    private SysDicTypeService sysDicTypeService;

    @Resource
    private SysDicTypeManager sysDicTypeManager;


    /**
     * 分页
     */
    @GetMapping("/page")
    public ResponseResult page(SysDicTypePageQuery query){

        Page<SysDicType> pageResult = sysDicTypeService.page(MyBatisUtils.buildPage(query),
                Wrappers.<SysDicType>query().lambda()
                        .eq(SysDicType::getDeleted, GlobalConstants.NOT_DELETED)
                        .like(StrUtil.isNotBlank(query.getDicName()), SysDicType::getDicName, query.getDicName())
                        .eq(StrUtil.isNotBlank(query.getDicCode()), SysDicType::getDicCode, query.getDicCode())
        );
        List<SysDicTypeVO> list = SysDicTypeConvert.INSTANCE.convert(pageResult.getRecords());

        return ResponseResult.ok(new PageResult<>(list, pageResult.getTotal()));
    }

    /**
     * 新增字典
     */
    @SaCheckPermission("sys.dic.type.add")
    @PostMapping
    public ResponseResult add(@Valid @RequestBody SysDicTypeAddForm sysDicAddForm){
        sysDicTypeManager.addType(sysDicAddForm);
        return ResponseResult.ok();
    }

    /**
     * 修改字典
     */
    @SaCheckPermission("sys.dic.type.edit")
    @PutMapping
    public ResponseResult edit(@Valid @RequestBody SysDicTypeEditForm sysDicEditForm){
        sysDicTypeManager.editType(sysDicEditForm);
        return ResponseResult.ok();
    }

    /**
     * 删除字典
     */
    @SaCheckPermission("sys.dic.type.del")
    @DeleteMapping("/{id}")
    public ResponseResult delete(@PathVariable Long id){
        sysDicTypeManager.deleteType(id);
        return ResponseResult.ok();
    }



}
