package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/20 11:49
 * @version 1.0
 */
@Data
public class SysRoleEditForm {

	@NotNull(message = "id不能为空")
	private Long id;

	@NotBlank(message = "角色编码不能为空")
	@Length(max = 20, message = "角色编码不能超出20字符")
	private String roleCode;

	@NotBlank(message = "角色名称不能为空")
	@Length(max = 20, message = "角色名称不能超出20字符")
	private String roleName;

	@Length(max = 200, message = "备注不能超出200字符")
	private String remark;

}
