package com.yjb.tenet.boot.system.application.manager;

import cn.hutool.core.util.ObjectUtil;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.system.application.convert.SysConfigConvert;
import com.yjb.tenet.boot.system.application.service.SysConfigService;
import com.yjb.tenet.boot.system.domain.entity.SysConfig;
import com.yjb.tenet.boot.system.domain.form.SysConfigAddForm;
import com.yjb.tenet.boot.system.domain.form.SysConfigEditForm;
import com.yjb.tenet.boot.system.infrastructure.constant.SysRedisKeyConstants;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/**
 * @author zpc
 * @version 1.0
 * @description 系统配置manager
 * @create 2022/5/25 15:51
 */
@Component
public class SysConfigManager {

    @Resource
    private SysConfigService sysConfigService;

    @Resource
    private StringRedisTemplate stringRedisTemplate;


	/**
	 * 新增，无需添加缓存，用时缓存
	 * @param form
	 * @return
	 */
	public boolean add(SysConfigAddForm form) {
		SysConfig sysConfig = SysConfigConvert.INSTANCE.convert(form);
		return sysConfigService.save(sysConfig);
	}

    /**
     * 修改，修改成功后，删除旧缓存
     * @param form
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean edit(SysConfigEditForm form) {
        SysConfig sysConfig = SysConfigConvert.INSTANCE.convert(form);
        sysConfigService.updateById(sysConfig);
	    return stringRedisTemplate.delete(SysRedisKeyConstants.SYS_CONFIG_KEY + sysConfig.getConfigKey());
    }

    /**
     * 删除，删除缓存
     * @param id
     * @return
     */
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id){
        SysConfig one = sysConfigService.getById(id);
        one.setDeleted(one.getId());
        sysConfigService.updateById(one);
        return stringRedisTemplate.delete(SysRedisKeyConstants.SYS_CONFIG_KEY + one.getConfigKey());
    }

    /**
     * 根据key查找
     * @param key
     * @return
     */
    public String getValueByKey(String key){
        String configValue = stringRedisTemplate.opsForValue().get(SysRedisKeyConstants.SYS_CONFIG_KEY + key);
        if(ObjectUtil.isNotNull(configValue)){
            return configValue;
        }
        SysConfig sysConfig = sysConfigService.lambdaQuery()
                .eq(SysConfig::getDeleted, GlobalConstants.NOT_DELETED)
                .eq(SysConfig::getConfigKey, key)
                .one();
	    stringRedisTemplate.opsForValue().set(SysRedisKeyConstants.SYS_CONFIG_KEY + sysConfig.getConfigKey(), sysConfig.getConfigValue());
        return sysConfig.getConfigValue();
    }

	/**
	 * 从数据库读取，缓存到 redis
	 */
	public void refresh() {
		List<SysConfig> list = sysConfigService.lambdaQuery()
				.eq(SysConfig::getDeleted, GlobalConstants.NOT_DELETED)
				.list();
		Set keys = stringRedisTemplate.keys(SysRedisKeyConstants.SYS_CONFIG_KEY + "*");
		stringRedisTemplate.delete(keys);
		list.forEach(sysConfig -> {
			stringRedisTemplate.opsForValue().set(SysRedisKeyConstants.SYS_CONFIG_KEY + sysConfig.getConfigKey(), sysConfig.getConfigValue());
		});
	}
}
