package com.yjb.tenet.boot.system.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysDeptRelation;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-30
 */
public interface SysDeptRelationMapper extends BaseMapper<SysDeptRelation> {

	/**
	 * 删除部门关系
	 * @param sysDeptRelation
	 */
	void deleteDeptRelations(SysDeptRelation sysDeptRelation);

	/**
	 * 新增部门节点关系
	 * @param deptRelation 待新增的部门节点关系
	 */
	void insertDeptRelations(SysDeptRelation deptRelation);

}
