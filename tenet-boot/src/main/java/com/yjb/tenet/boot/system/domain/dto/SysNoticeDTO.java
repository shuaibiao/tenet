package com.yjb.tenet.boot.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

/**
 * @author zpc
 * @version 1.0
 * @description 通知简略详情
 * @create 2022/6/2 15:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysNoticeDTO {
    private Long id;
    private String title;
    private String author;
    private String cover;
    private String digest;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
    private String isRead;
}
