package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

/**
 * 字典数据
 */
@Data
public class SysDicDataDTO {

    private Long id;

    /** 字典编码 */
    private String dicCode;

    /** 字典标签 */
    private String dicLabel;

    /** 字典值 */
    private String dicValue;

    /** 排序 */
    private Long orderNo;

    /** 备注 */
    private String remark;

}
