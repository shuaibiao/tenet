package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/8 9:20
 **/
@Data
public class SysDicTypeEditForm {

    @NotNull(message = "id不能为空")
    private Long id;

    @NotBlank(message = "字典名称不能为空")
    @Length(max = 20, message = "字典名称不能超出20字符")
    private String dicName;

    @Length(max = 200, message = "备注不能超出200字符")
    private String remark;

}
