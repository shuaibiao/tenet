package com.yjb.tenet.boot.framework.mybatis.plus.core.datascope;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/30 14:28
 * @version 1.0
 */
public class DataScopeContextHolder {

	private static final ThreadLocal<DataScope> contextHolder = new ThreadLocal();

	public static DataScope getContext() {
		return contextHolder.get();
	}

	public static void setContext(DataScope context) {
		contextHolder.set(context);
	}

	public static void clearContext() {
		contextHolder.remove();
	}


}
