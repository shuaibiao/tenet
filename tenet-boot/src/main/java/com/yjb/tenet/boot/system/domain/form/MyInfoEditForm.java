package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/27 10:25
 * @version 1.0
 */
@Data
public class MyInfoEditForm {

	@NotBlank(message = "昵称不能为空")
	@Length(max=8, message = "昵称不能超出8个字符")
	private String nickname;

	@Pattern(regexp = "^(1[3-9]\\d{9}$)", message = "手机号格式不正确")
	@Length(max=11, message = "手机号不能超出11字符")
	private String mobile;

	@Email(message = "邮箱格式不正确")
	@Length(max=50, message = "邮箱不能超出50字符")
	private String email;

	private String sex;

	@NotBlank(message = "请输入密码完成修改")
	private String password;

	@Pattern(regexp = "(?!^(\\d+|[a-zA-Z]+|[~!@#$%^&*()_.]+)$)^[\\w~!@#$%^&*()_.]{6,20}$" , message = "密码应为字母，数字，特殊符号(~!@#$%^&*()_.)，两种及以上组合，8-16位字符串，如：xyl37@baa")
	private String newPassword;
}
