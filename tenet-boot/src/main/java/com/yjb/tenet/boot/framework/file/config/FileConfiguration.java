package com.yjb.tenet.boot.framework.file.config;

import com.yjb.tenet.boot.framework.file.core.client.FileClient;
import com.yjb.tenet.boot.framework.file.core.client.s3.S3FileClient;
import com.yjb.tenet.boot.framework.file.core.client.s3.S3FileClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Description
 * @Author yin.jinbiao
 * @Date 2023/2/25 23:44
 * @Version 1.0
 */
@Configuration
public class FileConfiguration {


    @Bean
    public S3FileClientProperties s3FileClientProperties(){
        return new S3FileClientProperties();
    }


    @Bean
    public FileClient s3FileClient(S3FileClientProperties s3FileClientProperties){
        return new S3FileClient(s3FileClientProperties);
    }
}
