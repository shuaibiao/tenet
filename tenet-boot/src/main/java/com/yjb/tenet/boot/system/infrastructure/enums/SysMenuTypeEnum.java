package com.yjb.tenet.boot.system.infrastructure.enums;

import lombok.Getter;

/**
 * 菜单类型 枚举
 * @author YJB
 */
@Getter
public enum SysMenuTypeEnum {

    MENU("M","菜单"),
    BUTTON("B","按钮");

    private final String value;

    private final String desc;

	SysMenuTypeEnum(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }
}
