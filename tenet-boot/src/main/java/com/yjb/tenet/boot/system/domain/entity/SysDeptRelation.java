package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 部门子集关系
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dept_relation")
public class SysDeptRelation implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long ancestor;

    private Long descendant;
}
