package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author zpc
 * @version 1.0
 * @description 新增部门表单
 * @create 2022/5/20 16:07
 */
@Data
public class SysDeptAddForm {

    /**
     * 父节点id
     */
    @NotNull(message = "父菜单不能为空")
    private Long parentId;

    /**
     * 部门名称
     */
    @NotBlank(message = "部门名称不能为空")
    @Length(max = 20,message = "字符长度不能超过20")
    private String deptName;

	/**
	 * 部门负责人
	 */
	@Length(max = 50,message = "部门负责人长度不能超过50")
	private String deptLeader;

    /**
     * 备注
     */
    @Length(max = 200,message = "备注长度不能超过200")
    private String remark;

    /**
     * 排序号
     */
    @NotNull(message = "排序号不能为空")
    private Long orderNo;

}
