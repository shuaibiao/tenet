package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import com.yjb.tenet.boot.framework.mybatis.plus.core.handler.JsonLongSetTypeHandler;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * 角色
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "sys_role", autoResultMap = true)
public class SysRole extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

	/** 角色编码 */
    private String roleCode;

	/** 角色名 */
    private String roleName;

	/** 数据类型，1内置角色，2自定义 */
	private String type;

	/** 数据权限 */
	private String dataScope;

	/** 数据范围(指定部门数组) */
	@TableField(typeHandler = JsonLongSetTypeHandler.class)
	private Set<Long> dataScopeDeptIds;

	/** 备注 */
	private String remark;

}
