package com.yjb.tenet.boot.quartz.application.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.quartz.domain.dto.SysJobPageDTO;
import com.yjb.tenet.boot.quartz.domain.entity.SysJob;
import com.yjb.tenet.boot.quartz.domain.query.SysJobPageQuery;


/**
 * <p>
 * 定时任务调度表 服务类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
public interface SysJobService extends IService<SysJob> {

	/**
	 * 分页方法
	 * @param page
	 * @param param
	 * @return
	 */
	IPage<SysJobPageDTO> page(Page<SysJobPageDTO> page, SysJobPageQuery param);
}
