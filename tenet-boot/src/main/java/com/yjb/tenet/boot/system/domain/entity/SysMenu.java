package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 菜单
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_menu")
public class SysMenu extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 菜单名 */
    private String name;

    /** 权限标识符 */
	private String permission;

    /** 路径 */
	private String path;

    /** 父级ID */
    private Long parentId;

    /** 图标 */
	private String icon;

    /** 排序 */
    private Long orderNo;

    /** 类型  M/B*/
    private String type;

    /** 是否隐藏 */
    private String hidden;

    /** 是否路由缓冲，Y-是，N-否 */
    private String keepAlive;

    /** 打开方式，1-内部，2-外部 */
    private String openType;

    /** 备注 */
    private String remark;



}
