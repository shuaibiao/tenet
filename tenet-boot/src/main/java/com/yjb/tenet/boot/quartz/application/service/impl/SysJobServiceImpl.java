package com.yjb.tenet.boot.quartz.application.service.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.quartz.application.mapper.SysJobMapper;
import com.yjb.tenet.boot.quartz.application.service.SysJobService;
import com.yjb.tenet.boot.quartz.domain.dto.SysJobPageDTO;
import com.yjb.tenet.boot.quartz.domain.entity.SysJob;
import com.yjb.tenet.boot.quartz.domain.query.SysJobPageQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 定时任务调度表 服务实现类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
@Service
public class SysJobServiceImpl extends ServiceImpl<SysJobMapper, SysJob> implements SysJobService {

	@Resource
	private SysJobMapper sysJobMapper;

	@Override
	public IPage<SysJobPageDTO> page(Page<SysJobPageDTO> page, SysJobPageQuery param) {
		return sysJobMapper.page(page, param);
	}
}
