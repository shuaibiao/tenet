package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

import java.util.Set;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/27 17:08
 * @version 1.0
 */
@Data
public class SysUserDetails {

	private Long id;

	/**
	 * 部门 id
	 */
	private Long deptId;

	private String username;

	private String nickname;

	private String sex;

	private String mobile;

	private String email;

	/**
	 * 缓存，用于数据过滤
	 */
	private Set<SysRolePageDTO> roles;

	/**
	 * 缓存用于功能接口鉴权
	 */
	private Set<String> permissions;
}
