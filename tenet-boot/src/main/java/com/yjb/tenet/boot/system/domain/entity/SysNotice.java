package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通知公告
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_notice")
public class SysNotice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 标题 */
    private String title;

    /** 作者姓名 */
    private String author;

    /** 内容 */
    private String content;

    /** 封面 */
    private String cover;

    /** 摘要 */
    private String digest;

    private String ipAddr;

    /** 是否发布 */
    private String published;

}
