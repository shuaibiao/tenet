package com.yjb.tenet.boot.system.controller;

import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.convert.SysFileConvert;
import com.yjb.tenet.boot.system.application.manager.SysFileManager;
import com.yjb.tenet.boot.system.domain.entity.SysFile;
import com.yjb.tenet.boot.system.domain.query.SysFilePageQuery;
import com.yjb.tenet.boot.system.domain.vo.SysFilePageVO;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;

/**
 * 文件相关
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RequestMapping("/system/file")
@RestController
public class SysFileController {

	@Resource
	private SysFileManager sysFileManager;

	@DeleteMapping("/delete")
	public ResponseResult<Boolean> deleteFile(@RequestParam("id") Long id) throws Exception {
		sysFileManager.deleteFile(id);
		return ResponseResult.ok();
	}

	@PostMapping("/upload")
	public ResponseResult<String> upload(@RequestPart("file") MultipartFile file) throws IOException {
		String url = sysFileManager.createFile(file.getOriginalFilename(), null, file.getBytes());
		return ResponseResult.ok(url);
	}

	@GetMapping("/page")
	public ResponseResult<PageResult<SysFilePageVO>> getFilePage(@Valid SysFilePageQuery query) {
		PageResult<SysFile> pageResult = sysFileManager.page(query);
		return ResponseResult.ok(SysFileConvert.INSTANCE.convertPage(pageResult));
	}
}
