package com.yjb.tenet.boot.quartz.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.quartz.domain.entity.SysJobLog;

/**
 * <p>
 * 定时任务调度日志表 Mapper 接口
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
public interface SysJobLogMapper extends BaseMapper<SysJobLog> {

}
