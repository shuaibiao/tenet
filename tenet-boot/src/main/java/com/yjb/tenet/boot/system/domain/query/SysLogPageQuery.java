package com.yjb.tenet.boot.system.domain.query;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/31 14:53
 * @version 1.0
 */
@Data
public class SysLogPageQuery {

	private String type;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate beginDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;
}
