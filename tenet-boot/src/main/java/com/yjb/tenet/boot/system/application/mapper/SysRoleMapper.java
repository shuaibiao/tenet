package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRole;
import com.yjb.tenet.boot.system.domain.query.SysRolePageQuery;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-17
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

	/**
	 * 条件分页查询语句
	 * @param page
	 * @param query
	 * @return
	 */
	IPage<SysRolePageDTO> page(Page<SysRolePageDTO> page, @Param("query") SysRolePageQuery query);

	/**
	 * 根据用户id查询所有角色信息
	 * @param userId
	 * @return
	 */
	List<SysRolePageDTO> listByUserId(Long userId);
}
