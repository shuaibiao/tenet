package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 用户
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_user")
public class SysUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

	/** 部门ID */
    private Long deptId;

	/** 部门名 */
    private String deptName;

	/** 用户名 */
	private String username;

	/** 昵称/姓名 */
	private String nickname;

	/** 密码 */
	private String password;

	/** 联系电话 */
	private String mobile;

	/** 邮箱 */
	private String email;

	/** 性别 M:男，W:女 */
	private String sex;

	/** 账号状态（Y是，N否） */
	private String locked;

	/** 用户类型，1内置用户，2其它 */
	private String type;


}
