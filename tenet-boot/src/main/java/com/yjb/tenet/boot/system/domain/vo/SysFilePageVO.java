package com.yjb.tenet.boot.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/27 11:19
 * @version 1.0
 */
@Data
public class SysFilePageVO {

	private Long id;

	private String name;

	private String path;

	private String url;

	private String type;

	private Integer size;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

}
