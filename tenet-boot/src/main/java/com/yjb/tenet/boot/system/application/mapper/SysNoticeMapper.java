package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yjb.tenet.boot.system.domain.dto.SysNoticePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysNotice;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
public interface SysNoticeMapper extends BaseMapper<SysNotice> {


    IPage<SysNoticePageDTO> page(@Param("page") IPage<SysNoticePageDTO> page, @Param("title") String title);


}
