package com.yjb.tenet.boot.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 系统参数
 */
@Data
public class SysConfigPageVO {

    private Long id;

    /** 配置名 */
    private String configName;

    /** 键 */
    private String configKey;

    /** 值 */
    private String configValue;

    /** 是否为系统内置 */
    private String configSys;

    /** 备注 */
    private String remark;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;


}
