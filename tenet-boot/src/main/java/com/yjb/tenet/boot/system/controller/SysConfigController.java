package com.yjb.tenet.boot.system.controller;


import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.framework.mybatis.plus.core.util.MyBatisUtils;
import com.yjb.tenet.boot.system.application.convert.SysConfigConvert;
import com.yjb.tenet.boot.system.application.manager.SysConfigManager;
import com.yjb.tenet.boot.system.application.service.SysConfigService;
import com.yjb.tenet.boot.system.domain.dto.CheckMsg;
import com.yjb.tenet.boot.system.domain.entity.SysConfig;
import com.yjb.tenet.boot.system.domain.form.SysConfigAddForm;
import com.yjb.tenet.boot.system.domain.form.SysConfigEditForm;
import com.yjb.tenet.boot.system.domain.query.SysConfigPageQuery;
import com.yjb.tenet.boot.system.domain.vo.SysConfigPageVO;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 系统配置
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/config")
public class SysConfigController {

    @Resource
    private SysConfigService sysConfigService;

    @Resource
    private SysConfigManager sysConfigManager;

    /**
     * 分页
     * @return
     */
    @GetMapping("/page")
    public ResponseResult page(SysConfigPageQuery query){
        Page<SysConfig> pageResult = sysConfigService.page(MyBatisUtils.buildPage(query), Wrappers.<SysConfig>query().lambda()
                .eq(SysConfig::getDeleted, GlobalConstants.NOT_DELETED)
                .like(StrUtil.isNotBlank(query.getConfigName()), SysConfig::getConfigName, query.getConfigName()));

        List<SysConfigPageVO> list = SysConfigConvert.INSTANCE.convert(pageResult.getRecords());
        return ResponseResult.ok(new PageResult<>(list, pageResult.getTotal()));
    }

    /**
     * 删除
     * @param id
     * @return
     */
    @Log("删除系统参数")
    @DeleteMapping("{id:\\d+}")
    @SaCheckPermission("sys.config.del")
    public ResponseResult delete(@PathVariable Long id){
        sysConfigManager.delete(id);
        return ResponseResult.ok();
    }

    /**
     * 修改
     * @param form
     * @return
     */
    @Log("修改系统参数")
    @PutMapping
    @SaCheckPermission("sys.config.edit")
    public ResponseResult edit(@RequestBody @Valid SysConfigEditForm form){
        sysConfigManager.edit(form);
        return ResponseResult.ok();
    }

    /**
     * 新增
     * @param form
     * @return
     */
    @Log("新增系统参数")
    @PostMapping
    @SaCheckPermission("sys.config.add")
    public ResponseResult add(@RequestBody @Valid SysConfigAddForm form){
        sysConfigManager.add(form);
        return ResponseResult.ok();
    }

    /**
     * 根据key查找
     * @param key
     * @return
     */
    @GetMapping("/{key}")
    public ResponseResult getValueByKey(@PathVariable("key") String key){
        return ResponseResult.ok(sysConfigManager.getValueByKey(key));
    }

	/**
	 * 刷新缓存
	 * @return
	 */
	@Log("刷新系统参数缓存")
	@GetMapping("/refresh")
    @SaCheckPermission("sys.config.refresh")
	public ResponseResult refresh(){
		sysConfigManager.refresh();
		return ResponseResult.ok();
	}

	/**
	 * 校验 config key 是否存在
	 * @param configKey
	 * @return
	 */
	@GetMapping("/check/{configKey}")
	public ResponseResult check(@PathVariable String configKey){
		CheckMsg checkMsg = new CheckMsg();
		Long count = sysConfigService.lambdaQuery()
                .eq(SysConfig::getDeleted, GlobalConstants.NOT_DELETED)
                .eq(SysConfig::getConfigKey, configKey).count();
		if(count>0){
			checkMsg.setPass(false);
			return ResponseResult.ok(checkMsg);
		}
		checkMsg.setPass(true);
		return ResponseResult.ok(checkMsg);
	}



}
