package com.yjb.tenet.boot.system.infrastructure.log;

import java.lang.annotation.*;

/**
 * @author yinjinbiao
 * @version 1.0
 * @description
 * @create 2022/5/23 14:29
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {

	/**
	 * 描述
	 * @return {String}
	 */
	String value();
}
