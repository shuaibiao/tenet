package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.system.domain.dto.SysUserPageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysUser;
import com.yjb.tenet.boot.system.domain.query.SysUserPageQuery;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-17
 */
public interface SysUserMapper extends BaseMapper<SysUser> {

	/**
	 * 条件分页查询语句
	 * @param page
	 * @param query
	 * @return
	 */
	IPage<SysUserPageDTO> page(Page<SysUserPageDTO> page, @Param("query") SysUserPageQuery query);

}
