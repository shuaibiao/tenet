package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysUserMapper;
import com.yjb.tenet.boot.system.application.service.SysUserService;
import com.yjb.tenet.boot.system.domain.dto.SysUserPageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysUser;
import com.yjb.tenet.boot.system.domain.query.SysUserPageQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-17
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

	@Resource
	private SysUserMapper sysUserMapper;

	@Override
	public IPage<SysUserPageDTO> page(Page<SysUserPageDTO> page, SysUserPageQuery query) {
		return sysUserMapper.page(page, query);
	}
}
