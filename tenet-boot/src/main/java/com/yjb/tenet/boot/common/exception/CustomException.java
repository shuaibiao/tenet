package com.yjb.tenet.boot.common.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @Description 业务异常
 * @Author yin.jinbiao
 * @Date 2021/9/27 14:26
 * @Version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CustomException extends RuntimeException {

	/**
	 * 错误码
	 */
	private String errorCode;

	/**
	 * 明细错误码说明
	 */
	private String errorMsg;

	public CustomException(String errorCode, String errorMsg) {
		super(errorMsg);
		this.errorCode = errorCode;
		this.errorMsg = errorMsg;
	}
}