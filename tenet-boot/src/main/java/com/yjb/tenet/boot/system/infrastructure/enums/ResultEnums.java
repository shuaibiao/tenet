package com.yjb.tenet.boot.system.infrastructure.enums;

import lombok.Getter;

/**
 * @Description
 * @Author yin.jinbiao
 * @Date 2022/5/23 21:49
 * @Version 1.0
 */
@Getter
public enum  ResultEnums {

    SUCCESS("S","成功"),
        FAIL("F","失败");

    private final String value;
    
    private final String desc;

    ResultEnums(String value, String desc) {
        this.value = value;
        this.desc = desc;
    }

}
