package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;

import java.util.Set;

/**
 * @description 角色数据权限分配
 * @author yinjinbiao
 * @create 2022/5/25 9:41
 * @version 1.0
 */
@Data
public class SysRoleDataSopeForm {

	/**
	 * 角色id
	 */
	private Long roleId;

	/**
	 * 数据权限
	 */
	private String dataScope;

	/**
	 * 菜单列表
	 */
	private Set<Long> deptIds;

}
