package com.yjb.tenet.boot.system.controller;


import cn.hutool.extra.servlet.ServletUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.system.application.manager.SysNoticeMangager;
import com.yjb.tenet.boot.system.application.service.SysNoticeService;
import com.yjb.tenet.boot.system.domain.dto.SysNoticePageDTO;
import com.yjb.tenet.boot.system.domain.form.SysNoticeAddForm;
import com.yjb.tenet.boot.system.domain.form.SysNoticeEditForm;
import com.yjb.tenet.boot.system.infrastructure.log.Log;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * 通知管理
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController {

    @Resource
    private SysNoticeMangager sysNoticeMangager;

    @Resource
    private SysNoticeService sysNoticeService;

    /**
     * 分页
     * @param page
     * @return
     */
    @GetMapping("page")
    public ResponseResult page(Page<SysNoticePageDTO> page, String title) {
        return ResponseResult.ok(sysNoticeService.getPage(page,title));
    }

    /**
     * 根据id拿详情
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public ResponseResult getById(@PathVariable("id") Long id){
        return ResponseResult.ok(sysNoticeMangager.getById(id));
    }

    /**
     * 新增
     * @param form
     * @return
     */
    @Log("新增通知")
    @PostMapping
    public ResponseResult save(@RequestBody @Valid SysNoticeAddForm form, HttpServletRequest request){
        return ResponseResult.ok(sysNoticeMangager.save(form,ServletUtil.getClientIP(request)));
    }

    /**
     * 修改
     * @param form
     * @return
     */
    @Log("修改通知")
    @PutMapping
    public ResponseResult edit(@RequestBody @Valid SysNoticeEditForm form, HttpServletRequest request){
        return ResponseResult.ok(sysNoticeMangager.edit(form, ServletUtil.getClientIP(request)));
    }


    /**
     * 删除
     * @param id
     * @return
     */
    @Log("删除通知")
    @DeleteMapping("{id}")
    public ResponseResult delete(@PathVariable("id") Long id){
        return ResponseResult.ok(sysNoticeMangager.delete(id));
    }

    /**
     * 未读条数
     * @return
     */
    @GetMapping("myNotReadCount")
    public ResponseResult getNotReadList(){
        return ResponseResult.ok(sysNoticeMangager.notReadCount());
    }

    /**
     * 通知中心
     * @return
     */
    @GetMapping("myNotice")
    public ResponseResult isReadList(){
        return ResponseResult.ok(sysNoticeMangager.myNoticeList());
    }


    /**
     * 已读
     * @return
     */
    @Log("阅读了通知公告")
    @PostMapping("/read/{noticeId}")
    public ResponseResult isRead(@PathVariable("noticeId")Long noticeId){
        return ResponseResult.ok(sysNoticeMangager.save(noticeId));
    }

}
