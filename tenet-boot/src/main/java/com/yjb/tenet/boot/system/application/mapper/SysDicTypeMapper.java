package com.yjb.tenet.boot.system.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/7 16:46
 **/
public interface SysDicTypeMapper extends BaseMapper<SysDicType> {


}
