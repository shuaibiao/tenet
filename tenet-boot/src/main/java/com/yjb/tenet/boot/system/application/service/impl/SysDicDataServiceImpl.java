package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysDicDataMapper;
import com.yjb.tenet.boot.system.application.service.SysDicDataService;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;
import org.springframework.stereotype.Service;

/**
 * 字典项/字典数据
 * @Author 贾栋栋
 * @Date 2023/3/8 11:52
 **/
@Service
public class SysDicDataServiceImpl extends ServiceImpl<SysDicDataMapper, SysDicData> implements SysDicDataService {

}
