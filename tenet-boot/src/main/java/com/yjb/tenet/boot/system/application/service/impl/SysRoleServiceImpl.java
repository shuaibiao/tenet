package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysRoleMapper;
import com.yjb.tenet.boot.system.application.service.SysRoleService;
import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRole;
import com.yjb.tenet.boot.system.domain.query.SysRolePageQuery;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-17
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements SysRoleService {

	@Resource
	private SysRoleMapper sysRoleMapper;

	@Override
	public IPage<SysRolePageDTO> page(Page<SysRolePageDTO> page, SysRolePageQuery query) {
		return sysRoleMapper.page(page, query);
	}

	@Override
	public List<SysRolePageDTO> listByUserId(Long userId) {
		return sysRoleMapper.listByUserId(userId);
	}
}
