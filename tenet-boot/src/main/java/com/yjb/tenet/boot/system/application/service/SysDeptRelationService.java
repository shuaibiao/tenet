package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysDept;
import com.yjb.tenet.boot.system.domain.entity.SysDeptRelation;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-30
 */
public interface SysDeptRelationService extends IService<SysDeptRelation> {

	/**
	 * 维护部门关系
	 * @param sysDept
	 * @return
	 */
	boolean saveDeptRelation(SysDept sysDept);

	/**
	 * 更新部门关系
	 * @param relation
	 * @return
	 */
	boolean updateDeptRelation(SysDeptRelation relation);
}
