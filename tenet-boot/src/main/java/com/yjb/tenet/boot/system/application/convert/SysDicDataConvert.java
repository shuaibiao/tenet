package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.system.domain.dto.SysDicDataDTO;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;
import com.yjb.tenet.boot.system.domain.form.SysDicDataAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDicDataEditForm;
import com.yjb.tenet.boot.system.domain.vo.SysDicDataVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 字典项/字典数据
 * @Author 贾栋栋
 * @Date 2023/3/8 14:07
 **/
@Mapper
public interface SysDicDataConvert {

    SysDicDataConvert INSTANCE = Mappers.getMapper(SysDicDataConvert.class);

    List<SysDicDataDTO> convertDTO(List<SysDicData> list);

    List<SysDicDataVO> convertDTOToVO(List<SysDicDataDTO> list);

    List<SysDicDataVO> convertVO(List<SysDicData> list);

    SysDicData convert(SysDicDataAddForm from);

    SysDicData convert(SysDicDataEditForm from);

    SysDicDataDTO convert(SysDicData sysDicData);

}
