package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRole;
import com.yjb.tenet.boot.system.domain.query.SysRolePageQuery;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-15
 */
public interface SysRoleService extends IService<SysRole> {

	/**
	 * 分页方法
	 * @param page
	 * @param param
	 * @return
	 */
	IPage<SysRolePageDTO> page(Page<SysRolePageDTO> page, SysRolePageQuery param);

	/**
	 * 根据用户id查询所有角色
	 * @param userId
	 * @return
	 */
	List<SysRolePageDTO> listByUserId(Long userId);
}
