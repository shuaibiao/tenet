package com.yjb.tenet.boot.system.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;

/**
 * 字典项/字典数据
 * @Author 贾栋栋
 * @Date 2023/3/8 11:52
 **/
public interface SysDicDataMapper extends BaseMapper<SysDicData> {
}
