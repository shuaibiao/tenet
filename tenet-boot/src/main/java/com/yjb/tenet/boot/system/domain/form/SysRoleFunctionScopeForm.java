package com.yjb.tenet.boot.system.domain.form;

import lombok.Data;

import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/28 15:27
 * @version 1.0
 */
@Data
public class SysRoleFunctionScopeForm {

	/**
	 * 角色id
	 */
	private Long roleId;

	/**
	 * 菜单列表
	 */
	private List<Long> menuIds;

}
