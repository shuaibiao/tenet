package com.yjb.tenet.boot.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @description 存在一些内置数据，是不允许操作或者删除的。用数据类型来控制。
 * @author yinjinbiao
 * @create 2023/2/23 16:45
 * @version 1.0
 */
@Getter
@AllArgsConstructor
public enum DataTypeEnum {

	/**
	 * 内置角色
	 */
	SYSTEM("1"),
	/**
	 * 自定义角色
	 */
	CUSTOM("2");

	private final String value;
}
