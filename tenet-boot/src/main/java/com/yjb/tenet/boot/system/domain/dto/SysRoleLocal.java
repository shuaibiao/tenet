package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

/**
 * @description 角色缓存
 * @author yinjinbiao
 * @create 2022/5/30 18:31
 * @version 1.0
 */
@Data
public class SysRoleLocal {

	private Long id;

	private String roleCode;

	private String roleName;

	private String remark;

	private String dataScope;
}
