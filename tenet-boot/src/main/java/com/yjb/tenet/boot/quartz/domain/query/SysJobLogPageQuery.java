package com.yjb.tenet.boot.quartz.domain.query;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/5 10:21
 * @version 1.0
 */
@Data
public class SysJobLogPageQuery {

	private String jobName;

	private String jobGroup;

	private String status;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate beginDate;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate endDate;

}
