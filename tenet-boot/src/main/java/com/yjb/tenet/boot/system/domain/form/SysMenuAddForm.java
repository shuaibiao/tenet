package com.yjb.tenet.boot.system.domain.form;


import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/28 11:16
 * @version 1.0
 */
@Data
public class SysMenuAddForm {

	@Length(max=10, message = "菜单名称不能超出10个字符")
	@NotBlank(message = "菜单名称不能为空")
	private String name;

	@NotNull(message = "菜单父ID不能为空")
	private Long parentId;

	@Length(max=100, message = "菜单名称不能超出100个字符")
	private String permission;

	@Length(max=200, message = "地址不能超出200个字符")
	private String path;

	@NotBlank(message = "是否隐藏不能为空")
	private String hidden;

	private String icon;

	private Long orderNo;

	private String type;

	private String keepAlive;

	private String openType;
}
