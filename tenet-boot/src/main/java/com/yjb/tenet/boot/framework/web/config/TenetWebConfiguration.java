package com.yjb.tenet.boot.framework.web.config;

import com.yjb.tenet.boot.framework.web.handler.GlobalExceptionHandler;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/16 17:57
 * @version 1.0
 */
@AutoConfiguration
public class TenetWebConfiguration {

	/**
	 * 全局异常处理器
	 * @return
	 */
	@Bean
	public GlobalExceptionHandler globalExceptionHandler() {
		return new GlobalExceptionHandler();
	}
}
