package com.yjb.tenet.boot.system.domain.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/8 9:36
 **/
@Data
public class SysDicTypeVO {

    private Long id;

    /** 字典名称 */
    private String dicName;

    /** 字典编码 */
    private String dicCode;

    /** 备注 */
    private String remark;

    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;
}
