package com.yjb.tenet.boot.quartz.infrastructure.core;


import com.yjb.tenet.boot.quartz.domain.entity.SysJob;
import com.yjb.tenet.boot.quartz.infrastructure.utils.JobInvokeUtil;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 * 
 * @author ruoyi
 *
 */
public class QuartzJobExecution extends AbstractQuartzJob {

    @Override
    protected void doExecute(JobExecutionContext context, SysJob sysJob) throws Exception
    {
        JobInvokeUtil.invokeMethod(sysJob);
    }
}
