package com.yjb.tenet.boot.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/4/18 10:00
 * @version 1.0
 */
@Data
public class SysUserPageDTO {

	private Long id;

	private Long deptId;

	private String deptName;

	private String username;

	private String nickname;

	private String mobile;

	private String email;

	private String sex;

	private String locked;

	private List<SysRolePageDTO> roles;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

}
