package com.yjb.tenet.boot.framework.web.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.ZoneId;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/24 16:01
 * @version 1.0
 */
@Configuration
public class JacksonConfiguration {

	/**
	 * json 序列化器
	 * 1. 新增Long类型序列化规则，数值超过2^53-1，在JS会出现精度丢失问题，因此Long自动序列化为字符串类型
	 * 2. 新增LocalDateTime序列化、反序列化规则
	 */
	@Bean
	public Jackson2ObjectMapperBuilderCustomizer customizer() {
		return builder -> {
			builder.locale(Locale.CHINA);
			builder.timeZone(TimeZone.getTimeZone(ZoneId.systemDefault()));
			builder.simpleDateFormat("yyyy-MM-dd HH:mm:ss");
			builder.serializerByType(Long.class, ToStringSerializer.instance);
			// 序列化所有字段
			builder.serializationInclusion(JsonInclude.Include.ALWAYS);
			// JAVA8 日期时间模块
			builder.modules(new JavaTimeModule());
		};
	}


}
