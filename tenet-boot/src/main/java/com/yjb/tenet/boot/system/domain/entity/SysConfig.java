package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 系统参数
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_config")
public class SysConfig extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 配置名 */
    private String configName;

    /** 键 */
    private String configKey;

    /** 值 */
    private String configValue;

    /** 是否为系统内置 */
    private String configSys;

    /** 备注 */
    private String remark;


}
