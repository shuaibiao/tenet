package com.yjb.tenet.boot.system.domain.form;


import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * @description 新增用户表单
 * @author yinjinbiao
 * @create 2022/4/18 10:51
 * @version 1.0
 */
@Data
public class SysUserAddForm {

	@NotNull(message = "部门不能为空")
	private Long deptId;

	@NotBlank(message = "部门名称不能为空")
	private String deptName;

	@NotBlank(message = "账号不能为空")
	@Length(max=20, message = "账号不能超出20个字符")
	@Pattern(regexp = "^([0-9a-zA-Z_]{5,20}$)", message = "账号只能由英文字母、数字和下划线_组成，长度5~20位")
	private String username;

	@NotBlank(message = "昵称不能为空")
	@Length(max=8, message = "昵称不能超出8个字符")
	private String nickname;

	private String locked;

	@Pattern(regexp = "^(1[3-9]\\d{9}$)", message = "手机号格式不正确")
	@Length(max=11, message = "手机号不能超出11字符")
	private String mobile;

	@Email(message = "邮箱格式不正确")
	@Length(max=50, message = "邮箱不能超出50字符")
	private String email;

	private String sex;

	@NotNull(message = "角色不能为空")
	private List<Long> roles;

}
