package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_notice_reader")
public class SysNoticeReader implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 通知id */
    private Long noticeId;

    /** 用户id */
    private Long userId;

	/** 是否已读 */
	private String isRead;
}
