/**
 * SSE（Server-Sent Events，服务器推送事件）是一种用于在客户端和服务器之间建立单向通信的技术。它允许服务器端发送异步消息给客户端，而无需客户端明确地请求数据。
 * SSE 基于 HTTP 协议，使用简单的格式来传输文本数据，通常被用于实时通知、实时更新等场景。
 * 不同于 Websocket，只能单向通信数据。
 * @author yinjinbiao
 * @create 2024/4/28 11:46
 */
package com.yjb.tenet.boot.sse;