package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;

/**
 * @Author 贾栋栋
 * @Date 2023/3/8 9:41
 **/
@Data
public class SysDicTypePageQuery extends PageParam {


    /** 字典名称 */
    private String dicName;

    /** 字典编码 */
    private String dicCode;

}
