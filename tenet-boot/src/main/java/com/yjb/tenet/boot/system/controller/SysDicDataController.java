package com.yjb.tenet.boot.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaIgnore;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.common.pojo.ResponseResult;
import com.yjb.tenet.boot.framework.mybatis.plus.core.util.MyBatisUtils;
import com.yjb.tenet.boot.system.application.convert.SysDicDataConvert;
import com.yjb.tenet.boot.system.application.manager.SysDicDataManager;
import com.yjb.tenet.boot.system.application.service.SysDicDataService;
import com.yjb.tenet.boot.system.domain.dto.SysDicDataDTO;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;
import com.yjb.tenet.boot.system.domain.form.SysDicDataAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDicDataEditForm;
import com.yjb.tenet.boot.system.domain.query.SysDicDataPageQuery;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 字典项相关
 * @author yinjinbiao
 * @create 2021/12/15 17:21
 */
@RestController
@RequestMapping("/system/dic/data")
public class SysDicDataController {

    @Resource
    private SysDicDataService sysDicDataService;

    @Resource
    private SysDicDataManager sysDicDataManager;

    /**
     * 分页
     */
    @GetMapping("/page")
    public ResponseResult page(SysDicDataPageQuery query){

        Page<SysDicData> pageResult = sysDicDataService.page(MyBatisUtils.buildPage(query),
                Wrappers.<SysDicData>query().lambda()
                        .eq(SysDicData::getDeleted, GlobalConstants.NOT_DELETED)
                        .eq(StrUtil.isNotBlank(query.getDicCode()), SysDicData::getDicCode, query.getDicCode())
                        .like(StrUtil.isNotBlank(query.getDicLabel()), SysDicData::getDicLabel, query.getDicLabel())
                        .orderByAsc(SysDicData::getOrderNo));

        List<SysDicDataDTO> list = SysDicDataConvert.INSTANCE.convertDTO(pageResult.getRecords());

        return ResponseResult.ok(new PageResult<>(list, pageResult.getTotal()));
    }

    /**
     * 查询全部的字典数据
     */
    @SaIgnore
    @GetMapping("/list")
    public ResponseResult list(){
        List<SysDicDataDTO> list = sysDicDataManager.list();
        return ResponseResult.ok(SysDicDataConvert.INSTANCE.convertDTOToVO(list));
    }

    /**
     * 查询全部的字典数据
     */
    @SaIgnore
    @GetMapping("/list/{dicCode}")
    public ResponseResult getItemByCode(@PathVariable String dicCode){
        List<SysDicDataDTO> list = sysDicDataManager.getItemByCode(dicCode);
        return ResponseResult.ok(SysDicDataConvert.INSTANCE.convertDTOToVO(list));
    }

    /**
     * 新增
     */
    @SaCheckPermission("sys.dic.data.add")
    @PostMapping
    public ResponseResult add(@Valid @RequestBody SysDicDataAddForm form){
        sysDicDataManager.add(form);
        return ResponseResult.ok();
    }

    /**
     * 编辑
     */
    @PutMapping
    @SaCheckPermission("sys.dic.data.edit")
    public ResponseResult edit(@Valid @RequestBody SysDicDataEditForm form){
        sysDicDataManager.edit(form);
        return ResponseResult.ok();
    }

    /**
     * 删除
     */
    @DeleteMapping("/{id}")
    @SaCheckPermission("sys.dic.data.del")
    public ResponseResult delete(@PathVariable Long id){
        sysDicDataManager.delete(id);
        return ResponseResult.ok();
    }


    /**
     * 刷新缓存
     */
    @GetMapping("/refresh")
    @SaCheckPermission("sys.dic.data.refresh")
    public ResponseResult refresh(){
        sysDicDataManager.refresh();
        return ResponseResult.ok();
    }

}
