package com.yjb.tenet.boot.system.infrastructure.util;

import cn.hutool.extra.spring.SpringUtil;
import com.yjb.tenet.boot.system.application.manager.SysConfigManager;


/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/25 19:48
 * @version 1.0
 */
public class SysConfigUtils {

	/**
	 * 获取系统参数配置
	 * @param configKey
	 * @return
	 */
	public static String getConfigValue(String configKey){
		return SpringUtil.getBean(SysConfigManager.class).getValueByKey(configKey);
	}
}
