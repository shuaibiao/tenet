package com.yjb.tenet.boot.sse;

import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author yinjinbiao
 * @create 2024/4/28 13:55
 */
public class SseEmitterConstants {

    public static final Map<String, SseEmitter> sseEmitterMap = new ConcurrentHashMap<>();
}
