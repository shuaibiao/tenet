package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysDicData;

/**
 * 字典项/字典数据
 * @Author 贾栋栋
 * @Date 2023/3/8 11:50
 **/
public interface SysDicDataService extends IService<SysDicData> {


}
