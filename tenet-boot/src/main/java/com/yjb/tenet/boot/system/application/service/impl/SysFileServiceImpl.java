package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysFileMapper;
import com.yjb.tenet.boot.system.application.service.SysFileService;
import com.yjb.tenet.boot.system.domain.entity.SysFile;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-04-28
 */
@Service
public class SysFileServiceImpl extends ServiceImpl<SysFileMapper, SysFile> implements SysFileService {


}
