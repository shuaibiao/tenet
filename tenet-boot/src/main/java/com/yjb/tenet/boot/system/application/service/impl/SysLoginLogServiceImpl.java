package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysLoginLogMapper;
import com.yjb.tenet.boot.system.application.service.SysLoginLogService;
import com.yjb.tenet.boot.system.domain.entity.SysLoginLog;
import org.springframework.stereotype.Service;

/**
 * 登录日志
 * @author YJB
 */
@Service
public class SysLoginLogServiceImpl extends ServiceImpl<SysLoginLogMapper, SysLoginLog> implements SysLoginLogService {

}
