package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/3/14 16:27
 * @version 1.0
 */
@Data
public class SysLoginLogDTO {

	/**
	 * 日志主键
	 */
	private Long id;
	/**
	 * 日志类型
	 */
	private Integer logType;
	/**
	 * 链路追踪编号
	 */
	private String traceId;
	/**
	 * 用户编号
	 */
	private Long userId;
	/**
	 * 用户账号
	 *
	 * 冗余，因为账号可以变更
	 */
	private String username;
	/**
	 * 登录结果
	 */
	private Integer result;
	/**
	 * 用户 IP
	 */
	private String userIp;
	/**
	 * 浏览器 UA
	 */
	private String userAgent;

	private LocalDateTime createTime;

}
