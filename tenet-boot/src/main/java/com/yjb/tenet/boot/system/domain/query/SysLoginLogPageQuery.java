package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/3/14 15:22
 * @version 1.0
 */
@Data
public class SysLoginLogPageQuery extends PageParam {

	/**
	 * 用户 IP
	 */
	private String userIp;

	/**
	 * 用户账号
	 *
	 * 冗余，因为账号可以变更
	 */
	private String username;

	/**
	 * 登录结果
	 *
	 */
	private String status;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTimeStart;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTimeEnd;

}
