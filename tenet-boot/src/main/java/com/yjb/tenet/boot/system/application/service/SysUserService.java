package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.dto.SysUserPageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysUser;
import com.yjb.tenet.boot.system.domain.query.SysUserPageQuery;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-15
 */
public interface SysUserService extends IService<SysUser> {

	/**
	 * 分页方法
	 * @param page
	 * @param param
	 * @return
	 */
	IPage<SysUserPageDTO> page(Page<SysUserPageDTO> page, SysUserPageQuery param);

}
