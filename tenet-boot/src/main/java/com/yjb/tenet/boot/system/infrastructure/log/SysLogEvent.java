package com.yjb.tenet.boot.system.infrastructure.log;

import com.yjb.tenet.boot.system.domain.entity.SysLog;
import org.springframework.context.ApplicationEvent;

/**
 * @author 系统日志事件
 */
public class SysLogEvent extends ApplicationEvent {

	public SysLogEvent(SysLog source) {
		super(source);
	}

}
