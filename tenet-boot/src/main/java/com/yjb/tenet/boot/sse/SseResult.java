package com.yjb.tenet.boot.sse;

import lombok.Data;

/**
 * @author yinjinbiao
 * @create 2024/4/28 13:54
 */
@Data
public class SseResult<T> {

    public String code;

    /**
     * 消息
     */
    private String msg;

    /**
     * 返回对象
     */
    private T data;

}
