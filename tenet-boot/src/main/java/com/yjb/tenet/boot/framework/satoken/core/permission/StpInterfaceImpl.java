package com.yjb.tenet.boot.framework.satoken.core.permission;

import cn.dev33.satoken.stp.StpInterface;
import cn.hutool.core.util.ObjectUtil;
import com.yjb.tenet.boot.framework.satoken.core.LoginUser;
import com.yjb.tenet.boot.framework.satoken.core.utils.SecurityFrameworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/20 14:49
 * @version 1.0
 */
public class StpInterfaceImpl implements StpInterface {

	@Override
	public List<String> getPermissionList(Object loginId, String loginType) {
		LoginUser loginUser = SecurityFrameworkUtils.getLoginUser();
		if(ObjectUtil.isNull(loginUser)){
			return new ArrayList<>();
		}
		return loginUser.getPermissionCodes().stream().collect(Collectors.toList());
	}

	@Override
	public List<String> getRoleList(Object loginId, String loginType) {
		LoginUser loginUser = SecurityFrameworkUtils.getLoginUser();
		if(ObjectUtil.isNull(loginUser)){
			return new ArrayList<>();
		}
		return loginUser.getRoleCodes().stream().collect(Collectors.toList());
	}
}
