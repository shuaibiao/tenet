package com.yjb.tenet.boot.framework.file.core.client.s3;

import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HttpUtil;
import com.yjb.tenet.boot.framework.file.core.client.FileClient;
import io.minio.*;

import java.io.ByteArrayInputStream;

/** 基于 S3 协议的文件客户端，实现 MinIO、阿里云、腾讯云、七牛云、华为云等云服务
 * S3 协议的客户端，采用亚马逊提供的 software.amazon.awssdk.s3 库
 * @description
 * @author yinjinbiao
 * @create 2023/2/27 11:25
 * @version 1.0
 */
public class S3FileClient implements FileClient {

    public static final String ENDPOINT_QINIU = "qiniucs.com";
    public static final String ENDPOINT_ALIYUN = "aliyuncs.com";
    public static final String ENDPOINT_TENCENT = "myqcloud.com";

    private MinioClient client;

    private S3FileClientProperties s3FileClientProperties;

    public S3FileClient(S3FileClientProperties s3FileClientProperties) {
        this.s3FileClientProperties = s3FileClientProperties;
        // 补全 domain
        if (StrUtil.isEmpty(s3FileClientProperties.getDomain())) {
            s3FileClientProperties.setDomain(buildDomain());
        }
        // 初始化客户端
        client = MinioClient.builder()
                .endpoint(buildEndpointURL()) // Endpoint URL
                .region(buildRegion()) // Region
                .credentials(s3FileClientProperties.getAccessKey(), s3FileClientProperties.getAccessSecret()) // 认证密钥
                .build();
    }

    /**
     * 基于 endpoint 构建调用云服务的 URL 地址
     *
     * @return URI 地址
     */
    private String buildEndpointURL() {
        // 如果已经是 http 或者 https，则不进行拼接.主要适配 MinIO
        if (HttpUtil.isHttp(this.s3FileClientProperties.getEndpoint()) || HttpUtil.isHttps(this.s3FileClientProperties.getEndpoint())) {
            return this.s3FileClientProperties.getEndpoint();
        }
        return StrUtil.format("https://{}", this.s3FileClientProperties.getEndpoint());
    }

    /**
     * 基于 bucket + endpoint 构建访问的 Domain 地址
     *
     * @return Domain 地址
     */
    private String buildDomain() {
        // 如果已经是 http 或者 https，则不进行拼接.主要适配 MinIO
        if (HttpUtil.isHttp(this.s3FileClientProperties.getEndpoint()) || HttpUtil.isHttps(this.s3FileClientProperties.getEndpoint())) {
            return StrUtil.format("{}/{}", this.s3FileClientProperties.getEndpoint(), this.s3FileClientProperties.getBucket());
        }
        // 阿里云、腾讯云、华为云都适合。七牛云比较特殊，必须有自定义域名
        return StrUtil.format("https://{}.{}", this.s3FileClientProperties.getBucket(), this.s3FileClientProperties.getEndpoint());
    }

    /**
     * 基于 bucket 构建 region 地区
     *
     * @return region 地区
     */
    private String buildRegion() {
        // 阿里云必须有 region，否则会报错
        if (this.s3FileClientProperties.getEndpoint().contains(ENDPOINT_ALIYUN)) {
            return StrUtil.subBefore(this.s3FileClientProperties.getEndpoint(), '.', false)
                    .replaceAll("-internal", "")// 去除内网 Endpoint 的后缀
                    .replaceAll("https://", "");
        }
        // 腾讯云必须有 region，否则会报错
        if (this.s3FileClientProperties.getEndpoint().contains(ENDPOINT_TENCENT)) {
            return StrUtil.subAfter(this.s3FileClientProperties.getEndpoint(), ".cos.", false)
                    .replaceAll("." + ENDPOINT_TENCENT, ""); // 去除 Endpoint
        }
        return null;
    }

    @Override
    public String upload(byte[] content, String path, String type) throws Exception {
        // 执行上传
        client.putObject(PutObjectArgs.builder()
                .bucket(this.s3FileClientProperties.getBucket()) // bucket 必须传递
                .contentType(type)
                .object(path) // 相对路径作为 key
                .stream(new ByteArrayInputStream(content), content.length, -1) // 文件内容
                .build());
        // 拼接返回路径
        return this.s3FileClientProperties.getDomain() + "/" + path;
    }

    @Override
    public void delete(String path) throws Exception {
        client.removeObject(RemoveObjectArgs.builder()
                .bucket(this.s3FileClientProperties.getBucket()) // bucket 必须传递
                .object(path) // 相对路径作为 key
                .build());
    }

    @Override
    public byte[] getContent(String path) throws Exception {
        GetObjectResponse response = client.getObject(GetObjectArgs.builder()
                .bucket(this.s3FileClientProperties.getBucket()) // bucket 必须传递
                .object(path) // 相对路径作为 key
                .build());
        return IoUtil.readBytes(response);
    }

}
