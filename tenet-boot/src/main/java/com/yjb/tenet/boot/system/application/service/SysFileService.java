package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-04-28
 */
public interface SysFileService extends IService<SysFile> {

}
