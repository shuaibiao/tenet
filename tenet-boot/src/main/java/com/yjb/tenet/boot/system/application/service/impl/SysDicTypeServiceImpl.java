package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysDicTypeMapper;
import com.yjb.tenet.boot.system.application.service.SysDicTypeService;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;
import org.springframework.stereotype.Service;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/7 16:46
 **/
@Service
public class SysDicTypeServiceImpl extends ServiceImpl<SysDicTypeMapper, SysDicType> implements SysDicTypeService {


}
