package com.yjb.tenet.boot.system.application.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysFile;


/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-04-28
 */
public interface SysFileMapper extends BaseMapper<SysFile> {

}
