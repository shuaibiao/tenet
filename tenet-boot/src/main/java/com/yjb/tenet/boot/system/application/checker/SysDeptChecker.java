package com.yjb.tenet.boot.system.application.checker;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.exception.CustomException;
import com.yjb.tenet.boot.common.exception.enums.ErrorCodeEnum;
import com.yjb.tenet.boot.system.application.service.SysDeptService;
import com.yjb.tenet.boot.system.domain.entity.SysDept;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author 贾栋栋
 * @Date 2023/6/9 9:36
 **/
@Component
public class SysDeptChecker {

    @Resource
    private SysDeptService sysDeptService;


    /**
     * 删除校验
     */
    public SysDept checkForDelete(Long id){

        List<SysDept> list = sysDeptService.lambdaQuery()
                .eq(SysDept::getDeleted, GlobalConstants.NOT_DELETED)
                .eq(SysDept::getParentId, id)
                .list();
        if (CollectionUtil.isNotEmpty(list)) {
            throw new CustomException(ErrorCodeEnum.PARAM_ILLEGAL.getCode(), "该节点下存在子节点,请先删除子节点");
        }

        SysDept sysDept = sysDeptService.lambdaQuery()
                .eq(SysDept::getDeleted, GlobalConstants.NOT_DELETED)
                .eq(SysDept::getDeptId, id)
                .one();
        if (ObjectUtil.isNull(sysDept)){
            throw new CustomException(ErrorCodeEnum.PARAM_ILLEGAL.getCode(), "该数据不存在！");
        }
        return sysDept;

    }

}
