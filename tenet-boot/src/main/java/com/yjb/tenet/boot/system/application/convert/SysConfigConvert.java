package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.system.domain.entity.SysConfig;
import com.yjb.tenet.boot.system.domain.form.SysConfigAddForm;
import com.yjb.tenet.boot.system.domain.form.SysConfigEditForm;
import com.yjb.tenet.boot.system.domain.vo.SysConfigPageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author 贾栋栋
 * @Date 2023/6/8 17:59
 **/
@Mapper
public interface SysConfigConvert {

    SysConfigConvert INSTANCE = Mappers.getMapper(SysConfigConvert.class);

    List<SysConfigPageVO> convert(List<SysConfig> list);

    SysConfig convert(SysConfigAddForm form);

    SysConfig convert(SysConfigEditForm form);

}
