package com.yjb.tenet.boot.system.application.checker;

import cn.hutool.core.util.ObjectUtil;
import com.yjb.tenet.boot.common.constants.GlobalConstants;
import com.yjb.tenet.boot.common.exception.CustomException;
import com.yjb.tenet.boot.common.exception.enums.ErrorCodeEnum;
import com.yjb.tenet.boot.system.application.service.SysDicTypeService;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 字典 数据校验
 * @Author 贾栋栋
 * @Date 2023/3/7 16:52
 **/
@Component
public class SysDicTypeChecker {

    @Resource
    private SysDicTypeService sysDicTypeService;

    /**
     * 数据唯一校验
     * @param dicCode 编码数据唯一
     */
    public void checkForExist(String dicCode) {
        SysDicType one = sysDicTypeService.lambdaQuery()
                .eq(SysDicType::getDeleted, GlobalConstants.NOT_DELETED)
                .eq(SysDicType::getDicCode, dicCode)
                .one();

        if (ObjectUtil.isNotNull(one)){
            throw new CustomException(ErrorCodeEnum.BUSINESS_FAIL.getCode(), "字典编码已存在！");
        }

    }
}
