package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dept")
public class SysDept extends BaseEntity {
    
    @TableId(value = "dept_id", type = IdType.ASSIGN_ID)
    private Long deptId;

    /** 父级ID */
    private Long parentId;

    /** 部门名称 */
    private String deptName;

	/** 部门负责人账号 */
	private String deptLeader;

    /** 备注 */
    private String remark;

    /** 排序 */
    private Long orderNo;

    private String deptAlias;


}
