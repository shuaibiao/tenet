package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysNoticeReaderMapper;
import com.yjb.tenet.boot.system.application.service.SysNoticeReaderService;
import com.yjb.tenet.boot.system.domain.entity.SysNoticeReader;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
@Service
public class SysNoticeReaderServiceImpl extends ServiceImpl<SysNoticeReaderMapper, SysNoticeReader> implements SysNoticeReaderService {

}
