package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysNoticeReader;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
public interface SysNoticeReaderService extends IService<SysNoticeReader> {

}
