package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典数据
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_dic_data")
public class SysDicData extends BaseEntity {

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 字典编码 */
    private String dicCode;

    /** 字典标签 */
    private String dicLabel;

    /** 字典值 */
    private String dicValue;

    /** 排序 */
    private Long orderNo;

    /** 备注 */
    private String remark;

}
