package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/25 9:51
 * @version 1.0
 */
@Data
public class SysRoleDataScopeDTO {

	private Long roleId;

	private String dataScope;

	private List<Long> deptIds;

}
