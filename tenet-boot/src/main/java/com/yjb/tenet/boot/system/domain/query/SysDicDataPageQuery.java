package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;

/**
 * 字典项
 * @Author 贾栋栋
 * @Date 2023/3/8 14:48
 **/
@Data
public class SysDicDataPageQuery extends PageParam {

    /** 字典编码 */
    private String dicCode;

    /** 字典标签 */
    private String dicLabel;

}
