package com.yjb.tenet.boot.quartz.application.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.quartz.domain.entity.SysJobLog;

/**
 * <p>
 * 定时任务调度日志表 服务类
 * </p>
 *
 * @author yin.jinbiao
 * @since 2022-05-04
 */
public interface SysJobLogService extends IService<SysJobLog> {

}
