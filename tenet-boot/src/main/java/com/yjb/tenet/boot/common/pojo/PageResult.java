package com.yjb.tenet.boot.common.pojo;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Description 分页结果
 * @Author yin.jinbiao
 * @Date 2021/10/12 13:38
 * @Version 1.0
 */
@Data
public final class PageResult<T> implements Serializable {

    private List<T> records;

    private long total;

    public PageResult() {
    }

    public PageResult(List<T> records, long total) {
        this.records = records;
        this.total = total;
    }

    public PageResult(long total) {
        this.records = new ArrayList<>();
        this.total = total;
    }

    public static <T> PageResult<T> empty() {
        return new PageResult<>(0L);
    }

    public static <T> PageResult<T> empty(Long total) {
        return new PageResult<>(total);
    }

}
