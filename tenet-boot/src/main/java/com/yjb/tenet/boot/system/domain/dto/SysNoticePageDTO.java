package com.yjb.tenet.boot.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author zpc
 * @version 1.0
 * @description 分页dto
 * @create 2022/6/6 15:25
 */
@Data
public class SysNoticePageDTO {

    private Long id;

    /**
     * 标题
     */
    private String title;

    /**
     * 作者姓名
     */
    private String author;

    /**
     * 封面
     */
    private String cover;

    /**
     * 摘要
     */
    private String digest;

    private String ipAddr;

    /**
     * 是否发布
     */
    private String published;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime createTime;

    private String createBy;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private LocalDateTime updateTime;

}
