package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.system.domain.entity.SysMenu;
import com.yjb.tenet.boot.system.domain.form.SysMenuAddForm;
import com.yjb.tenet.boot.system.domain.form.SysMenuEditForm;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/22 9:02
 * @version 1.0
 */
@Mapper
public interface SysMenuConvert {

	SysMenuConvert INSTANCE = Mappers.getMapper(SysMenuConvert.class);

	SysMenu convert(SysMenuAddForm form);

	SysMenu convert(SysMenuEditForm form);
}
