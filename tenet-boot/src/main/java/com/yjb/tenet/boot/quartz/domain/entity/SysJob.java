package com.yjb.tenet.boot.quartz.domain.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 定时任务调度表 sys_job
 * 
 * @author ruoyi
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("sys_job")
public class SysJob implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 任务名称 */
    private String jobName;

    /** 任务组名 */
    private String jobGroup;

    /** 调用目标字符串 */
    private String invokeTarget;

    /** cron执行表达式 */
    private String cronExpression;

    /** cron计划策略 0=默认,1=立即触发执行,2=触发一次执行,3=不触发立即执行*/
    private String misfirePolicy;

    /** 是否并发执行（Y允许 N禁止） */
    private String concurrent;

    /** 任务状态（Y暂停 N正常） */
    private String pause;

    /**
     * 备注信息
     */
    private String remark;

    @TableField(fill = FieldFill.INSERT)
    private String createBy;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime createTime;

	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private LocalDateTime updateTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    private Long deleted;
}
