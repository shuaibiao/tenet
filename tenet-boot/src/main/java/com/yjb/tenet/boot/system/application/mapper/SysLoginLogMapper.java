package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysLoginLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author auto_generation
 * @since 2021-12-17
 */
public interface SysLoginLogMapper extends BaseMapper<SysLoginLog> {

}
