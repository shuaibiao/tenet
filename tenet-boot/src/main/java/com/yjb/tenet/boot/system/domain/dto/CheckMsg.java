package com.yjb.tenet.boot.system.domain.dto;

import lombok.Data;

/**
 * @description 校验数据是否通过 DTO
 * @author yinjinbiao
 * @create 2022/4/20 17:02
 * @version 1.0
 */
@Data
public class CheckMsg {

	/**
	 * 通过检查为true
	 */
	private boolean pass;
}
