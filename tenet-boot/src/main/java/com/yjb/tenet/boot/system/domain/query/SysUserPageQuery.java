package com.yjb.tenet.boot.system.domain.query;

import lombok.Data;

/**
 * @description 分页查询条件
 * @author yinjinbiao
 * @create 2022/4/18 10:02
 * @version 1.0
 */
@Data
public class SysUserPageQuery {

	private String username;

	private String nickname;

	private Long deptId;

}
