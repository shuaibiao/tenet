package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysDeptMapper;
import com.yjb.tenet.boot.system.application.service.SysDeptService;
import com.yjb.tenet.boot.system.domain.entity.SysDept;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zpc
 * @since 2022-05-20
 */
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

}
