package com.yjb.tenet.boot.framework.mybatis.plus.core.datascope;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @description 
 * @author yinjinbiao
 * @create 2022/5/27 13:55
 * @version 1.0
 */
@Aspect
@Component
public class DataScopeAspect {

	/**
	 * 前置通知，提取数据过滤需要的参数
	 * @param joinPoint
	 */
	@Before("@annotation(datascope)")
	public void doBefore(JoinPoint joinPoint, DataScope datascope){
		DataScopeContextHolder.setContext(datascope);
	}


	/**
	 * 最终通知 移除 ThreadLocal，防止影响线程复用与内存泄漏
	 */
	@After("@annotation(datascope)")
	public void clearThreadLocal(DataScope datascope){
		DataScopeContextHolder.clearContext();
	}

}
