package com.yjb.tenet.boot.system.infrastructure.enums;

import lombok.Getter;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/22 8:56
 * @version 1.0
 */
@Getter
public enum SysMenuRootEnum {

	ROOT(0L,"菜单根节点");

	private final Long value;

	private final String desc;

	SysMenuRootEnum(Long value, String desc) {
		this.value = value;
		this.desc = desc;
	}
}
