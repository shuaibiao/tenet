package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.system.domain.dto.SysRolePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysRole;
import com.yjb.tenet.boot.system.domain.form.SysRoleAddForm;
import com.yjb.tenet.boot.system.domain.form.SysRoleEditForm;
import com.yjb.tenet.boot.system.domain.vo.SysRolePageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/21 17:37
 * @version 1.0
 */
@Mapper
public interface SysRoleConvert {

	SysRoleConvert INSTANCE = Mappers.getMapper(SysRoleConvert.class);

	SysRole convert(SysRoleAddForm form);

	SysRole convert(SysRoleEditForm form);

	SysRolePageDTO convert(SysRole sysRole);

	List<SysRolePageDTO> convert(List<SysRole> list);

	List<SysRolePageVO> convert01(List<SysRolePageDTO> list);


}
