package com.yjb.tenet.boot.framework.satoken.core;

import lombok.Data;

import java.util.Set;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/18 18:43
 * @version 1.0
 */
@Data
public class LoginUser {

	private Long id;

	private String username;

	private Long tenantId;

	private Long deptId;

	private Set<String> roleCodes;

	private Set<String> permissionCodes;

	private Set<String> dataScopes;

	private Set<Long> dataScopesDeptIds;
}
