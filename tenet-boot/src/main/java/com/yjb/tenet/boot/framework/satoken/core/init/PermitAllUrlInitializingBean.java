package com.yjb.tenet.boot.framework.satoken.core.init;

import cn.dev33.satoken.annotation.SaIgnore;
import org.apache.commons.lang3.RegExUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

/**
 * @description 设置 Anonymous 注解允许匿名访问的url
 * @author yinjinbiao
 * @create 2022/7/26 9:10
 * @version 1.0
 */
public class PermitAllUrlInitializingBean implements InitializingBean, ApplicationContextAware {
	private static final Pattern PATTERN = Pattern.compile("\\{(.*?)\\}");

	private ApplicationContext applicationContext;

	private List<String> urls = new ArrayList<>();

	public String ASTERISK = "*";

	@Override
	public void afterPropertiesSet() {
		RequestMappingHandlerMapping mapping = applicationContext.getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);
		Map<RequestMappingInfo, HandlerMethod> map = mapping.getHandlerMethods();

		map.keySet().forEach(info -> {
			HandlerMethod handlerMethod = map.get(info);

			// 获取方法上边的注解 替代 @PathVariable 为 *
			SaIgnore method = AnnotationUtils.findAnnotation(handlerMethod.getMethod(), SaIgnore.class);
			Optional.ofNullable(method).ifPresent(anonymous -> info.getPatternValues()
					.forEach(url -> urls.add(RegExUtils.replaceAll(url, PATTERN, ASTERISK))));

			// 获取类上边的注解, 替代 @PathVariable 为 *
			SaIgnore controller = AnnotationUtils.findAnnotation(handlerMethod.getBeanType(), SaIgnore.class);
			Optional.ofNullable(controller).ifPresent(anonymous -> info.getPatternValues()
					.forEach(url -> urls.add(RegExUtils.replaceAll(url, PATTERN, ASTERISK))));
		});
	}

	@Override
	public void setApplicationContext(ApplicationContext context) throws BeansException
	{
		this.applicationContext = context;
	}

	public List<String> getUrls()
	{
		return urls;
	}

	public void setUrls(List<String> urls)
	{
		this.urls = urls;
	}
}
