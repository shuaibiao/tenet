package com.yjb.tenet.boot.system.infrastructure.constant;

/**
 * @Description 统一管理 Redis key
 * @Author yin.jinbiao
 * @Date 2022/5/23 22:01
 * @Version 1.0
 */
public class SysRedisKeyConstants {
	

    /** 系统配置 */
    public static final String SYS_CONFIG_KEY = "sys:config:";


	/** 字典CODE */

	public static final String SYS_DIC_KEY = "sys:dic:";

	public static final String SYS_DIC_KEY_ITEM = "sys:dic:item:";

	public static final String SYS_DIC_KEY_ALL = "sys:dic:all";


}
