package com.yjb.tenet.boot.system.domain.vo;

import lombok.Data;

/**
 * 字典数据
 */
@Data
public class SysDicDataVO  {

    /** 字典编码 */
    private String dicCode;

    /** 字典标签 */
    private String dicLabel;

    /** 字典值 */
    private String dicValue;



}
