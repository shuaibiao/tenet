package com.yjb.tenet.boot.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yjb.tenet.boot.common.BaseEntity;
import com.yjb.tenet.boot.system.infrastructure.enums.LoginLogTypeEnum;
import com.yjb.tenet.boot.system.infrastructure.enums.LoginResultEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * 登录日志表
 * 注意，包括登录和登出两种行为
 * @author YJB
 */
@TableName("sys_login_log")
@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class SysLoginLog extends BaseEntity {

    /**
     * 日志主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 日志类型
     *
     * 枚举 {@link LoginLogTypeEnum}
     */
    private Integer logType;

    /**
     * 用户编号
     */
    private Long userId;
    /**
     * 用户账号
     *
     * 冗余，因为账号可以变更
     */
    private String username;
    /**
     * 登录结果
     *
     * 枚举 {@link LoginResultEnum}
     */
    private Integer result;
    /**
     * 用户 IP
     */
    private String userIp;
	/**
	 * 浏览器 UA
	 */
	private String userAgent;

}
