package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.common.pojo.PageResult;
import com.yjb.tenet.boot.system.domain.entity.SysFile;
import com.yjb.tenet.boot.system.domain.vo.SysFilePageVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/27 11:25
 * @version 1.0
 */
@Mapper
public interface SysFileConvert {

	SysFileConvert INSTANCE = Mappers.getMapper(SysFileConvert.class);

	PageResult<SysFilePageVO> convertPage(PageResult<SysFile> pageResult);
}
