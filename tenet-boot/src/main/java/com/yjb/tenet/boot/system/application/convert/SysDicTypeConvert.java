package com.yjb.tenet.boot.system.application.convert;


import com.yjb.tenet.boot.system.domain.entity.SysDicType;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeAddForm;
import com.yjb.tenet.boot.system.domain.form.SysDicTypeEditForm;
import com.yjb.tenet.boot.system.domain.vo.SysDicTypeVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 字典类型
 * @Author 贾栋栋
 * @Date 2023/3/7 17:01
 **/
@Mapper
public interface SysDicTypeConvert {

    SysDicTypeConvert INSTANCE = Mappers.getMapper(SysDicTypeConvert.class);

    List<SysDicTypeVO> convert(List<SysDicType> list);

    SysDicType convert(SysDicTypeAddForm form);

    SysDicType convert(SysDicTypeEditForm form);

}
