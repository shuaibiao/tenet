package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysDicType;

/**
 * 字典类别
 * @Author 贾栋栋
 * @Date 2023/3/7 16:41
 **/
public interface SysDicTypeService extends IService<SysDicType> {



}
