package com.yjb.tenet.boot.system.application.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yjb.tenet.boot.system.domain.entity.SysNoticeReader;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
public interface SysNoticeReaderMapper extends BaseMapper<SysNoticeReader> {

}
