package com.yjb.tenet.boot.system.domain.query;

import com.yjb.tenet.boot.common.pojo.PageParam;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @description 
 * @author yinjinbiao
 * @create 2023/2/27 11:21
 * @version 1.0
 */
@Data
public class SysFilePageQuery extends PageParam {

	private String name;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTimeStart;

	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private LocalDateTime createTimeEnd;
}
