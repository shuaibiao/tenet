package com.yjb.tenet.boot.system.application.convert;

import com.yjb.tenet.boot.system.domain.entity.SysDept;
import com.yjb.tenet.boot.system.domain.form.SysDeptAddForm;
import com.yjb.tenet.boot.system.domain.vo.SysDeptVO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * @Author 贾栋栋
 * @Date 2023/6/9 9:30
 **/
@Mapper
public interface SysDeptConvert {

    SysDeptConvert INSTANCE = Mappers.getMapper(SysDeptConvert.class);

    List<SysDeptVO> convert(List<SysDept> list);

    SysDept convert(SysDeptAddForm form);

}
