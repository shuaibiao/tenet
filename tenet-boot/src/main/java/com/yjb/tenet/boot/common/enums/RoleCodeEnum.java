package com.yjb.tenet.boot.common.enums;

import cn.hutool.core.collection.CollectionUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Collection;

/**
 * 角色标识枚举
 *
 * @author YJB
 */
@Getter
@AllArgsConstructor
public enum RoleCodeEnum {

    SUPER_ADMIN("super_admin", "超级管理员");

    /**
     * 角色编码
     */
    private final String code;

    /**
     * 名字
     */
    private final String name;



    public static boolean isSuperAdmin(String code) {
        return SUPER_ADMIN.getCode().equals(code);
    }

	/**
	 * 判断用户角色是否存在超级管理员角色
	 * @param roleCodes
	 * @return
	 */
	public static boolean hasAnySuperAdmin(Collection<String> roleCodes) {
		if (CollectionUtil.isEmpty(roleCodes)) {
			return false;
		}
		return roleCodes.stream().anyMatch(roleCode -> RoleCodeEnum.isSuperAdmin(roleCode));
	}
}
