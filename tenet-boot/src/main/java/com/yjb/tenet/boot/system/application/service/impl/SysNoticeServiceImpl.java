package com.yjb.tenet.boot.system.application.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yjb.tenet.boot.system.application.mapper.SysNoticeMapper;
import com.yjb.tenet.boot.system.application.service.SysNoticeService;
import com.yjb.tenet.boot.system.domain.dto.SysNoticePageDTO;
import com.yjb.tenet.boot.system.domain.entity.SysNotice;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zpc
 * @since 2022-06-02
 */
@Service
public class SysNoticeServiceImpl extends ServiceImpl<SysNoticeMapper, SysNotice> implements SysNoticeService {

    @Resource
    private SysNoticeMapper sysNoticeMapper;

    @Override
    public IPage<SysNoticePageDTO> getPage(IPage<SysNoticePageDTO> page, String title) {
        return sysNoticeMapper.page(page,title);
    }
}
