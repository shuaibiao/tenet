package com.yjb.tenet.boot.system.application.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yjb.tenet.boot.system.domain.entity.SysDept;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zpc
 * @since 2022-05-20
 */
public interface SysDeptService extends IService<SysDept> {

}
