import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

/**
 * @author yjb
 * @description
 * @create 2024/9/27 11:44
 */
@Slf4j
public class InterruptTests implements Runnable{

    private volatile static boolean on = false;
    public static void main(String[] args) throws InterruptedException {
        Thread testThread = new Thread(new InterruptTests(),"InterruptionInJava");
        testThread.start();
        Thread.sleep(1000);
        InterruptTests.on = true;//妄图通过状态量来关闭线程，但是线程已经被阻塞住了。
        testThread.interrupt();//所以通过interrupt方法来设置线程为中断状态，导致正在阻塞的线程发出了中断异常，我们捕获它处理其它的逻辑后，线程成功结束。
        System.out.println("main end");
    }

    @Override
    public void run() {
        while(!on){
            try {
                Thread.sleep(10000000);
            } catch (InterruptedException e) {
                System.out.println("caught exception right now: "+e);
            }
        }
    }
}