import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.text.DecimalFormat;

/**
 * @author yjb
 * @description
 * @create 2024/9/23 16:49
 */
@Slf4j
public class NumberTests {

    @Test
    public void test(){
        Double chainComponentResult = 321.125456;
        String format = String.format("%.2f", chainComponentResult);
        log.info(format);
    }
}
