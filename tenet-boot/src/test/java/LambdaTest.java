import lombok.Data;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author yinjinbiao
 * @create 2024/5/6 10:46
 */
public class LambdaTest {

    @Test
    public void test(){
        List<User> list = new ArrayList<>();
        list.add(new User(1L,"张三",18));
        list.add(new User(2L,"李四",18));
        list.add(new User(3L,"王五",20));
        list.add(new User(4L,"赵六",20));
        list.add(new User(5L,"钱七",21));

        // 按年龄分组
        Map<Integer, List<User>> userAgeMap = list.stream()
                .collect(Collectors.groupingBy(User::getAge));
        System.out.println(userAgeMap);
        // 按年龄分组出所有的姓名
        Map<Integer, List<String>> userAgeName = list.stream()
                .collect(Collectors.groupingBy(User::getAge, Collectors.mapping(User::getName, Collectors.toList())));
        System.out.println(userAgeName);

        // 转成 Map<id,name> 的形式
        Map<Long, String> userMap = list.stream()
                .collect(Collectors.toMap(User::getId, User::getName));
        System.out.println(userMap);
    }
    
    
    @Data
    class User{
        private Long id;
        private String name;
        private Integer age;

        public User(long id, String name, int age) {
            this.id = id;
            this.name = name;
            this.age = age;
        }
    }
}
