import cn.hutool.core.date.DateUtil;
import org.junit.jupiter.api.Test;

import java.util.Date;

/**
 * @author yinjinbiao
 * @create 2024/4/18 16:43
 */

public class CronTest {

    @Test
    public void getCronExpression() {
        Date executeDate = DateUtil.tomorrow();
        String cronExpress = "";
        int year1 = cn.hutool.core.date.DateUtil.year(executeDate);
        int month1 = cn.hutool.core.date.DateUtil.month(executeDate) + 1;
        int day1 = cn.hutool.core.date.DateUtil.dayOfMonth(executeDate);
        int hour1 = cn.hutool.core.date.DateUtil.hour(executeDate, true);
        int minute1 = cn.hutool.core.date.DateUtil.minute(executeDate);
        int second1 = cn.hutool.core.date.DateUtil.second(executeDate);
        cronExpress = second1 + " " + minute1 + " " + hour1 + " " + day1 + " " + month1 + " " + "?" + " " + year1;
        System.out.println(cronExpress);
    }
}
