import cn.hutool.core.date.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author yinjinbiao
 * @create 2024/4/26 15:14
 */
@Slf4j
public class DateTest {

    @Test
    public void dateTest(){
        String start = "00:00:00";
        String end = "15:00:00";
        Date date = new Date();
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String invalidStartDateStr = sdf.format(date)+" "+start;
            String invalidEndDateStr = sdf.format(date)+" "+end;
            Date invalidStartDate = sdf2.parse(invalidStartDateStr);
            Date invalidEndDate = sdf2.parse(invalidEndDateStr);
            if(!DateUtil.isIn(date, invalidStartDate, invalidEndDate)){
               log.info("红外测温任务不在生效时间段");
            }else{
                log.info("红外测温任务在生效时间段");
            }
        } catch (ParseException e) {
            log.info("红外测温任务时间转换错误：param： {}");
        }
    }
}
