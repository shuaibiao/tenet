/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : localhost:3306
 Source Schema         : chorain-boot

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 06/07/2023 17:19:34
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for message_mail_account
-- ----------------------------
DROP TABLE IF EXISTS `message_mail_account`;
CREATE TABLE `message_mail_account`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `mail` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮箱',
  `username` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `password` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `host` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'SMTP 服务器域名',
  `port` int(11) NOT NULL COMMENT 'SMTP 服务器端口',
  `ssl_enable` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '\0' COMMENT '是否开启 SSL,Y启用，N禁用',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '邮箱账号表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_mail_account
-- ----------------------------
INSERT INTO `message_mail_account` VALUES (1, 'ydym_test@163.com', 'ydym_dstest@163.com', 'WBZTEINMIFVRYSOE', 'smtp.163.com', 465212, '1', 'admin', '2023-06-09 07:07:09', 'admin', '2023-06-09 07:07:09', 0);
INSERT INTO `message_mail_account` VALUES (2, '151516516@qq.com', 'fsd', '515161fd16516', 'nsdg.com', 5000, '1', 'admin', '2023-06-09 07:07:33', 'admin', '2023-06-09 07:07:33', 0);
INSERT INTO `message_mail_account` VALUES (3, 'jia_dongdong@chorain.com', 'jia_dongdong@chorain.com', 'AiBSdynC9rAV6cHy', 'smtp.exmail.qq.com', 465, '1', 'admin', '2023-06-09 07:07:55', 'admin', '2023-06-09 07:07:55', 0);
INSERT INTO `message_mail_account` VALUES (4, '1525834667@qq.com', '1525834667@qq.com', 'rclxlqzsqsqlbadd', 'smtp.qq.com', 465, '1', 'admin', '2023-06-09 07:08:16', 'admin', '2023-06-09 07:08:16', 0);
INSERT INTO `message_mail_account` VALUES (5, '451266738@qq.com', '小伙', '123456', 'fdf.com', 5512, '1', 'admin', '2023-06-09 07:08:36', 'admin', '2023-06-09 07:08:36', 0);

-- ----------------------------
-- Table structure for message_mail_log
-- ----------------------------
DROP TABLE IF EXISTS `message_mail_log`;
CREATE TABLE `message_mail_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `to_mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '接收邮箱地址',
  `account_id` bigint(20) NOT NULL COMMENT '邮箱账号编号',
  `from_mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '发送邮箱地址',
  `template_id` bigint(20) NOT NULL COMMENT '模板编号',
  `template_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `template_nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '模版发送人名称',
  `template_title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮件标题',
  `template_content` varchar(10240) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮件内容',
  `template_params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '邮件参数',
  `send_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '发送状态',
  `send_time` datetime NULL DEFAULT NULL COMMENT '发送时间',
  `send_message_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送返回的消息 ID',
  `send_exception` varchar(4096) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送异常',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '邮件日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_mail_log
-- ----------------------------
INSERT INTO `message_mail_log` VALUES (1, '1525834667@qq.com', 4, '1525834667@qq.com', 1, 'IOT_TEST', '测试', '测试IOT', '<p>您的设备103PC电脑<br>发生设备位置告警告警，<br>属于二级告警，<br>位于安徽省合肥市蜀山区创新产业园创新园，<br>当前设备状态sysLocation20，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '{\"alertCode\":\"20230420164611479961\",\"alertName\":\"设备位置告警\",\"areaAlias\":\"两江云计算数据中心>一期\",\"alertLevel\":\"二级告警\",\"location\":\"安徽省合肥市蜀山区创新产业园创新园\",\"time\":\"2023-06-12 01:10:44\",\"state\":\"sysLocation20\",\"deviceName\":\"103PC电脑\"}', 10, '2023-06-12 09:10:50', '<1920470657.1.1686532248130@tenet-message-biz-57c4965bd5-2p895>', NULL, NULL, '2023-06-12 01:10:46', NULL, '2023-06-12 01:10:50', b'0');
INSERT INTO `message_mail_log` VALUES (2, '1525834667@qq.com', 4, '1525834667@qq.com', 1, 'IOT_TEST', '测试', '测试IOT', '<p>您的设备103PC电脑<br>发生设备位置告警告警，<br>属于二级告警，<br>位于安徽省合肥市蜀山区创新产业园创新园，<br>当前设备状态sysLocation20，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '{\"alertCode\":\"20230420164611479961\",\"alertName\":\"设备位置告警\",\"areaAlias\":\"两江云计算数据中心>一期\",\"alertLevel\":\"二级告警\",\"location\":\"安徽省合肥市蜀山区创新产业园创新园\",\"time\":\"2023-06-12 01:12:50\",\"state\":\"sysLocation20\",\"deviceName\":\"103PC电脑\"}', 10, '2023-06-12 09:12:52', '<59039648.3.1686532370541@tenet-message-biz-57c4965bd5-2p895>', NULL, NULL, '2023-06-12 01:12:50', NULL, '2023-06-12 01:12:51', b'0');
INSERT INTO `message_mail_log` VALUES (3, '1525834667@qq.com', 4, '1525834667@qq.com', 1, 'IOT_TEST', '测试', '测试IOT', '<p>您的设备103PC电脑<br>发生设备位置告警告警，<br>属于二级告警，<br>位于安徽省合肥市蜀山区创新产业园创新园，<br>当前设备状态sysLocation20，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '{\"alertCode\":\"20230420164611479961\",\"alertName\":\"设备位置告警\",\"areaAlias\":\"两江云计算数据中心>一期\",\"alertLevel\":\"二级告警\",\"location\":\"安徽省合肥市蜀山区创新产业园创新园\",\"time\":\"2023-06-12 01:14:37\",\"state\":\"sysLocation20\",\"deviceName\":\"103PC电脑\"}', 10, '2023-06-12 09:14:39', '<1847385271.5.1686532477763@tenet-message-biz-57c4965bd5-2p895>', NULL, NULL, '2023-06-12 01:14:37', NULL, '2023-06-12 01:14:38', b'0');
INSERT INTO `message_mail_log` VALUES (4, '1525834667@qq.com', 4, '1525834667@qq.com', 1, 'IOT_TEST', '测试', '测试IOT', '<p>您的设备103PC电脑<br>发生设备位置告警告警，<br>属于二级告警，<br>位于安徽省合肥市蜀山区创新产业园创新园，<br>当前设备状态sysLocation20，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '{\"alertCode\":\"20230420164611479961\",\"alertName\":\"设备位置告警\",\"areaAlias\":\"两江云计算数据中心>一期\",\"alertLevel\":\"二级告警\",\"location\":\"安徽省合肥市蜀山区创新产业园创新园\",\"time\":\"2023-06-12 01:14:45\",\"state\":\"sysLocation20\",\"deviceName\":\"103PC电脑\"}', 10, '2023-06-12 09:14:47', '<168321163.7.1686532485948@tenet-message-biz-57c4965bd5-2p895>', NULL, NULL, '2023-06-12 01:14:45', NULL, '2023-06-12 01:14:46', b'0');
INSERT INTO `message_mail_log` VALUES (5, '451266738@qq.com', 4, '1525834667@qq.com', 1, 'IOT_TEST', '测试', '测试IOT', '<p>您的设备103PC电脑<br>发生电脑告警告警，<br>属于一级告警，<br>位于安徽省合肥市蜀山区创新产业园创新园，<br>当前设备状态sysLocation20，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '{\"alertCode\":\"20230704081312865892\",\"alertName\":\"电脑告警\",\"areaAlias\":\"两江云计算数据中心>一期\",\"alertLevel\":\"一级告警\",\"location\":\"安徽省合肥市蜀山区创新产业园创新园\",\"time\":\"2023-07-04 08:13:37\",\"state\":\"sysLocation20\",\"deviceName\":\"103PC电脑\"}', 10, '2023-07-04 16:13:40', '<1881487741.9.1688458419036@tenet-message-biz-57c4965bd5-2p895>', NULL, NULL, '2023-07-04 08:13:38', NULL, '2023-07-04 08:13:40', b'0');

-- ----------------------------
-- Table structure for message_mail_template
-- ----------------------------
DROP TABLE IF EXISTS `message_mail_template`;
CREATE TABLE `message_mail_template`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板名称',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `account_id` bigint(20) NOT NULL COMMENT '发送的邮箱账号编号',
  `nickname` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送人名称',
  `title` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板标题',
  `content` varchar(10240) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板内容',
  `params` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数数组',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开启状态 Y/N',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_code`(`code`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '邮件模版表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_mail_template
-- ----------------------------
INSERT INTO `message_mail_template` VALUES (1, '设备告警模板', 'IOT_TEST', 4, '测试', '测试IOT', '<p>您的设备{deviceName}<br>发生{alertName}告警，<br>属于{alertLevel}，<br>位于{location}，<br>当前设备状态{state}，<br>请及时处理<br><br><br>此邮件为tenet-cloud平台发送！！<br></p>', '[\"deviceName\",\"alertName\",\"alertLevel\",\"location\",\"state\"]', 'Y', NULL, 'admin', '2023-06-09 07:09:45', 'admin', '2023-06-09 07:09:45', 0);
INSERT INTO `message_mail_template` VALUES (2, '贾栋栋邮箱测试', 'MAIL-JDD', 4, '卓源', '邮箱测试', '<p>你是&nbsp;{key01}&nbsp;吗？<br><br><br><br>是的话，赶紧&nbsp;{key02}&nbsp;一下！</p>', '[\"key01\",\"key02\"]', 'Y', NULL, 'admin', '2023-06-09 07:10:15', 'admin', '2023-06-09 07:10:15', 0);

-- ----------------------------
-- Table structure for message_robot_channel
-- ----------------------------
DROP TABLE IF EXISTS `message_robot_channel`;
CREATE TABLE `message_robot_channel`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '渠道名称',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '渠道编码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开启状态 Y/N',
  `token` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '机器人的token',
  `secret` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '签名',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息机器人渠道' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_robot_channel
-- ----------------------------

-- ----------------------------
-- Table structure for message_robot_log
-- ----------------------------
DROP TABLE IF EXISTS `message_robot_log`;
CREATE TABLE `message_robot_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `channel_id` bigint(20) NOT NULL COMMENT '消息机器人编号',
  `channel_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息机器人编码',
  `template_id` bigint(20) NOT NULL COMMENT '模板编号',
  `template_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `template_type` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息机器人类型',
  `template_content` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息机器人内容',
  `template_params` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息机器人参数',
  `send_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '发送状态',
  `send_time` datetime NULL DEFAULT NULL COMMENT '发送时间',
  `send_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送结果的编码',
  `send_msg` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送结果的提示',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息机器人日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_robot_log
-- ----------------------------

-- ----------------------------
-- Table structure for message_robot_template
-- ----------------------------
DROP TABLE IF EXISTS `message_robot_template`;
CREATE TABLE `message_robot_template`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` tinyint(1) NOT NULL COMMENT '消息类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开启状态',
  `code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `name` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板名称',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板内容',
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数数组',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `channel_id` bigint(20) NOT NULL COMMENT '消息机器人编号',
  `channel_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '消息机器人编码',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_delete_code`(`deleted`, `code`) USING BTREE COMMENT '模板编码唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '消息机器人模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_robot_template
-- ----------------------------

-- ----------------------------
-- Table structure for message_sms_channel
-- ----------------------------
DROP TABLE IF EXISTS `message_sms_channel`;
CREATE TABLE `message_sms_channel`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `signature` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信签名',
  `code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '渠道编码',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开启状态 Y/N',
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `api_key` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信 API 的账号',
  `api_secret` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信 API 的秘钥',
  `callback_url` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信发送回调 URL',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信渠道' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_sms_channel
-- ----------------------------
INSERT INTO `message_sms_channel` VALUES (1, '卓易慧联', 'ALIYUN', 'Y', NULL, 'LTAI5tKDJutwk3Jj2ofKtFf3', 'fZ5od1mMcwH4tAszhNbJyQiXBTu6WC', NULL, '', '2023-03-20 17:12:58', '', '2023-03-20 17:12:58', 0);

-- ----------------------------
-- Table structure for message_sms_log
-- ----------------------------
DROP TABLE IF EXISTS `message_sms_log`;
CREATE TABLE `message_sms_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '编号',
  `channel_id` bigint(20) NOT NULL COMMENT '短信渠道编号',
  `channel_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信渠道编码',
  `template_id` bigint(20) NOT NULL COMMENT '模板编号',
  `template_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `template_type` tinyint(1) NOT NULL COMMENT '短信类型',
  `template_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信内容',
  `template_params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信参数',
  `api_template_id` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信 API 的模板编号',
  `mobile` varchar(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '手机号',
  `send_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '发送状态',
  `send_time` datetime NULL DEFAULT NULL COMMENT '发送时间',
  `send_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送结果的编码',
  `send_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '发送结果的提示',
  `api_send_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信 API 发送结果的编码',
  `api_send_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信 API 发送失败的提示',
  `api_request_id` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信 API 发送返回的唯一请求 ID',
  `api_serial_no` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '短信 API 发送返回的序号',
  `receive_status` tinyint(4) NOT NULL DEFAULT 0 COMMENT '接收状态',
  `receive_time` datetime NULL DEFAULT NULL COMMENT '接收时间',
  `api_receive_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'API 接收结果的编码',
  `api_receive_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'API 接收结果的说明',
  `create_by` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信日志' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_sms_log
-- ----------------------------
INSERT INTO `message_sms_log` VALUES (1, 1, 'ALIYUN', 1, 'SMS_258740082', 2, '您的设备1发生1告警，属于1，位于1，当前设备状态1，请及时处理。', '{\"deviceName\":\"1\",\"alertName\":\"1\",\"alertLevel\":\"1\",\"location\":\"1\",\"state\":\"1\"}', 'SMS_258740082', '18756400445', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 'sysadmin', '2023-07-06 17:11:25', 'sysadmin', '2023-07-06 17:11:25', 0);

-- ----------------------------
-- Table structure for message_sms_template
-- ----------------------------
DROP TABLE IF EXISTS `message_sms_template`;
CREATE TABLE `message_sms_template`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '开启状态',
  `code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板编码',
  `name` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板名称',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '模板内容',
  `params` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '参数数组',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `api_template_id` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信 API 的模板编号',
  `channel_id` bigint(20) NOT NULL COMMENT '短信渠道编号',
  `channel_code` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '短信渠道编码',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_delete_code`(`deleted`, `code`) USING BTREE COMMENT '模板编码唯一'
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '短信模板' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message_sms_template
-- ----------------------------
INSERT INTO `message_sms_template` VALUES (1, '2', 'Y', 'SMS_258740082', '设备告警通知', '您的设备{deviceName}发生{alertName}告警，属于{alertLevel}，位于{location}，当前设备状态{state}，请及时处理。', '[\"deviceName\",\"alertName\",\"alertLevel\",\"location\",\"state\"]', NULL, 'SMS_258740082', 1, 'ALIYUN', '', '2023-03-21 11:53:54', 'sysadmin', '2023-04-20 16:06:04', 0);

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `config_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '配置名',
  `config_key` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '键',
  `config_value` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '值',
  `config_sys` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统内置，Y是，N否',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_key`(`config_key`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES (1, '默认密码', 'DEFAULT_PASSWORD', '123456', 'Y', '新增用户时默认的密码', 'admin', '2022-05-25 16:46:58', 'sysadmin', '2023-06-09 17:01:55', 0);

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept`  (
  `dept_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) NOT NULL COMMENT '父级ID',
  `dept_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名称',
  `dept_leader` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '部门负责人账号',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `order_no` bigint(20) NULL DEFAULT NULL COMMENT '排序',
  `dept_alias` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '区域级联名称',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES (1667063949591322626, 0, '研发部', 'sunyawei', NULL, 1, '研发部', '2023-06-09 14:59:53', 'sysadmin', NULL, 'sysadmin', 0);
INSERT INTO `sys_dept` VALUES (1667071956966473729, 1667063949591322626, '下级部门', 'sunyawei', NULL, 1, '研发部>下级部门', '2023-06-09 15:31:42', 'sysadmin', NULL, 'sysadmin', 0);

-- ----------------------------
-- Table structure for sys_dept_relation
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept_relation`;
CREATE TABLE `sys_dept_relation`  (
  `ancestor` bigint(20) NOT NULL,
  `descendant` bigint(20) NOT NULL,
  PRIMARY KEY (`ancestor`, `descendant`) USING BTREE,
  UNIQUE INDEX `uk_ancestor_descendant`(`ancestor`, `descendant`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dept_relation
-- ----------------------------
INSERT INTO `sys_dept_relation` VALUES (1667063949591322626, 1667063949591322626);
INSERT INTO `sys_dept_relation` VALUES (1667063949591322626, 1667071956966473729);
INSERT INTO `sys_dept_relation` VALUES (1667071956966473729, 1667071956966473729);

-- ----------------------------
-- Table structure for sys_dic_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dic_data`;
CREATE TABLE `sys_dic_data`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dic_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dic_label` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dic_value` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `order_no` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_code_value`(`deleted`, `dic_code`, `dic_value`) USING BTREE COMMENT '同一个字典下值不能重复',
  UNIQUE INDEX `dic_code`(`deleted`, `dic_code`, `dic_label`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 152 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dic_data
-- ----------------------------
INSERT INTO `sys_dic_data` VALUES (13, 'IOT_UNIT', '无 / ', '', 1, NULL, '2022-09-09 16:07:35', 'sysadmin', '2022-11-24 15:06:08', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (14, 'IOT_UNIT', '毫克每千克 / mg/kg', 'mg/kg', 2, NULL, '2022-09-09 16:08:42', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (15, 'IOT_UNIT', '浊度 / NTU', 'NTU', 3, NULL, '2022-09-09 16:09:26', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (16, 'IOT_UNIT', 'PH值 / pH', 'pH', 4, NULL, '2022-09-09 16:09:54', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (17, 'IOT_UNIT', '土壤EC值 / dS/m', 'dS/m', 5, NULL, '2022-09-09 16:10:21', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (18, 'IOT_UNIT', '太阳总辐射 / W/', 'W/㎡', 6, NULL, '2022-09-09 16:11:11', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (19, 'IOT_UNIT', '降雨量 / mm/hour', 'mm/hour', 7, NULL, '2022-09-09 16:11:48', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (20, 'IOT_UNIT', '乏 / var', 'var', 8, NULL, '2022-09-09 16:12:08', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (21, 'IOT_UNIT', '厘泊 / cP', 'cP', 9, NULL, '2022-09-09 16:12:48', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (22, 'IOT_UNIT', '饱和度 / aw', 'aw', 10, NULL, '2022-09-09 16:13:09', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (23, 'IOT_UNIT', '个 / pcs', 'pcs', 11, NULL, '2022-09-09 16:13:32', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (24, 'IOT_UNIT', '厘斯 / cst', 'cst', 12, NULL, '2022-09-09 16:13:57', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (25, 'IOT_UNIT', '巴 / bar', 'bar', 13, NULL, '2022-09-09 16:14:11', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (26, 'IOT_UNIT', '纳克每升 / ppt', 'ppt', 14, NULL, '2022-09-09 16:14:32', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (27, 'IOT_UNIT', '微克每升 / ppb', 'ppb', 15, NULL, '2022-09-09 16:14:58', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (28, 'IOT_UNIT', '微西每厘米 / uS/cm', 'uS/cm', 16, NULL, '2022-09-09 16:15:23', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (29, 'IOT_UNIT', '牛顿每库伦 / N/C', 'N/C', 17, NULL, '2022-09-09 16:15:53', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (30, 'IOT_UNIT', '伏特每米 / V/m', 'V/m', 18, NULL, '2022-09-09 16:16:18', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (31, 'IOT_UNIT', '滴速 / ml/min', 'ml/min', 19, NULL, '2022-09-09 16:16:47', 'sysadmin', '2022-11-24 15:06:08', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (32, 'IOT_UNIT', '血压 / mmHg', 'mmHg', 20, NULL, '2022-09-09 16:17:27', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (33, 'IOT_UNIT', '血糖 / mmol/L', 'mmol/L', 21, NULL, '2022-09-09 16:17:57', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (34, 'IOT_UNIT', '毫米每秒 / mm/s', 'mm/s', 22, NULL, '2022-09-09 16:18:21', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (35, 'IOT_UNIT', '次 / count', 'count', 23, NULL, '2022-09-09 16:18:47', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (36, 'IOT_UNIT', '档 / gear', 'gear', 24, NULL, '2022-09-09 16:19:21', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (37, 'IOT_UNIT', '步 / stepCount', 'stepCount', 25, NULL, '2022-09-09 16:19:45', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (38, 'IOT_UNIT', '标准立方米每小时 / Nm3/h', 'Nm3/h', 26, NULL, '2022-09-09 16:20:42', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (39, 'IOT_UNIT', '千伏 / kV', 'kV', 27, NULL, '2022-09-09 16:21:15', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (40, 'IOT_UNIT', '千伏安 / kVA', 'kVA', 28, NULL, '2022-09-09 16:21:35', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (41, 'IOT_UNIT', '千乏 / kVar', 'kVar', 29, NULL, '2022-09-09 16:22:10', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (42, 'IOT_UNIT', '微瓦每平方厘米 / uw/cm2', 'w/cm2', 30, NULL, '2022-09-09 16:22:20', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (43, 'IOT_UNIT', '只 / 只', '只', 31, NULL, '2022-09-09 16:22:30', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (44, 'IOT_UNIT', '相对湿度 / %RH', '%RH', 32, NULL, '2022-09-09 16:22:43', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (45, 'IOT_UNIT', '立方米每秒 / m³/s', 'm³/s', 33, NULL, '2022-09-09 16:22:54', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (46, 'IOT_UNIT', '公斤每秒 / kg/s', 'kg/s', 34, NULL, '2022-09-09 16:23:07', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (47, 'IOT_UNIT', '转每分钟 / r/min', 'r/min', 35, NULL, '2022-09-09 16:23:16', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (48, 'IOT_UNIT', '吨每小时 / t/h', 't/h', 36, NULL, '2022-09-09 16:23:35', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (49, 'IOT_UNIT', '千卡每小时 / KCL/h', 'KCL/h', 37, NULL, '2022-09-09 16:23:39', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (50, 'IOT_UNIT', '升每秒 / L/s', 'L/s', 38, NULL, '2022-09-09 16:23:53', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (51, 'IOT_UNIT', '兆帕 / Mpa', 'Mpa', 39, NULL, '2022-09-09 16:24:06', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (52, 'IOT_UNIT', '立方米每小时 / m³/h', 'm³/h', 40, NULL, '2022-09-09 16:24:14', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (53, 'IOT_UNIT', '千乏时 / kvarh', 'kvarh', 41, NULL, '2022-09-09 16:24:26', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (54, 'IOT_UNIT', '微克每升 / μg/L', 'μg/L', 42, NULL, '2022-09-09 16:24:37', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (55, 'IOT_UNIT', '千卡路里 / kcal', 'kcal', 43, NULL, '2022-09-09 16:24:46', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (56, 'IOT_UNIT', '吉字节 / GB', 'GB', 44, NULL, '2022-09-09 16:24:54', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (57, 'IOT_UNIT', '兆字节 / MB', 'MB', 45, NULL, '2022-09-09 16:25:03', 'sysadmin', '2022-11-24 15:06:09', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (58, 'IOT_UNIT', '千字节 / KB', 'KB', 46, NULL, '2022-09-09 16:25:11', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (59, 'IOT_UNIT', '字节 / B', 'B', 47, NULL, '2022-09-09 16:25:21', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (60, 'IOT_UNIT', '微克每平方分米每天 / μg/(d㎡·d)', 'μg/(d㎡·d)', 48, NULL, '2022-09-09 16:25:31', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (61, 'IOT_UNIT', '百万分率 / ppm', 'ppm', 49, NULL, '2022-09-09 16:25:40', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (62, 'IOT_UNIT', '像素 / pixel', 'pixel', 50, NULL, '2022-09-09 16:26:04', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (63, 'IOT_UNIT', '照度 / Lux', 'Lux', 51, NULL, '2022-09-09 16:26:16', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (64, 'IOT_UNIT', '重力加速度 / grav', 'grav', 52, NULL, '2022-09-09 16:26:24', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (65, 'IOT_UNIT', '分贝 / dB', 'dB', 53, NULL, '2022-09-09 16:26:39', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (66, 'IOT_UNIT', '百分比 / %', '%', 54, NULL, '2022-09-09 16:26:48', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (67, 'IOT_UNIT', '流明 / lm', 'lm', 55, NULL, '2022-09-09 16:27:01', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (68, 'IOT_UNIT', '比特 / bit', 'bit', 56, NULL, '2022-09-09 16:27:11', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (69, 'IOT_UNIT', '克每毫升 / g/mL', 'g/mL', 57, NULL, '2022-09-09 16:27:23', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (70, 'IOT_UNIT', '克每升 / g/L', 'g/L', 58, NULL, '2022-09-09 16:27:34', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (71, 'IOT_UNIT', '毫克每升 / mg/L', 'mg/L', 59, NULL, '2022-09-09 16:27:45', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (72, 'IOT_UNIT', '微克每立方米 / μg/m³', 'μg/m³', 60, NULL, '2022-09-09 16:27:52', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (73, 'IOT_UNIT', '毫克每立方米 / mg/m³', 'mg/m³', 61, NULL, '2022-09-09 16:28:01', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (74, 'IOT_UNIT', '克每立方米 / g/m³', 'g/m³', 62, NULL, '2022-09-09 16:28:15', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (75, 'IOT_UNIT', '千克每立方米 / kg/m³', 'kg/m³', 63, NULL, '2022-09-09 16:28:24', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (76, 'IOT_UNIT', '纳法 / nF', 'nF', 64, NULL, '2022-09-09 16:28:32', 'sysadmin', '2022-11-24 15:06:10', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (77, 'IOT_UNIT', '皮法 / pF', 'pF', 65, NULL, '2022-09-09 16:28:40', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (78, 'IOT_UNIT', '微法 / μF', 'μF', 66, NULL, '2022-09-09 16:28:48', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (79, 'IOT_UNIT', '法拉 / F', 'F', 67, NULL, '2022-09-09 16:28:55', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (80, 'IOT_UNIT', '欧姆 / Ω', 'Ω', 68, NULL, '2022-09-09 16:29:03', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (81, 'IOT_UNIT', '微安 / μA', 'μA', 69, NULL, '2022-09-09 16:29:15', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (82, 'IOT_UNIT', '毫安 / mA', 'mA', 70, NULL, '2022-09-09 16:29:23', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (83, 'IOT_UNIT', '千安 / kA', 'kA', 71, NULL, '2022-09-09 16:29:31', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (84, 'IOT_UNIT', '安培 / A', 'A', 72, NULL, '2022-09-09 16:29:39', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (85, 'IOT_UNIT', '毫伏 / mV', 'mV', 73, NULL, '2022-09-09 16:29:47', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (86, 'IOT_UNIT', '伏特 / V', 'V', 74, NULL, '2022-09-09 16:29:54', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (87, 'IOT_UNIT', '毫秒 / ms', 'ms', 75, NULL, '2022-09-09 16:30:02', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (88, 'IOT_UNIT', '秒 / s', 's', 76, NULL, '2022-09-09 16:30:12', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (89, 'IOT_UNIT', '分钟 / min', 'min', 77, NULL, '2022-09-09 16:30:22', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (90, 'IOT_UNIT', '小时 / h', 'h', 78, NULL, '2022-09-09 16:30:32', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (91, 'IOT_UNIT', '日 / day', 'day', 79, NULL, '2022-09-09 16:30:44', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (92, 'IOT_UNIT', '周 / week', 'week', 80, NULL, '2022-09-09 16:30:51', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (93, 'IOT_UNIT', '月 / month', 'month', 81, NULL, '2022-09-09 16:31:00', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (94, 'IOT_UNIT', '年 / year', 'year', 82, NULL, '2022-09-09 16:31:07', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (95, 'IOT_UNIT', '节 / kn', 'kn', 83, NULL, '2022-09-09 16:31:15', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (96, 'IOT_UNIT', '千米每小时 / km/h', 'km/h', 84, NULL, '2022-09-09 16:31:25', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (97, 'IOT_UNIT', '米每秒 / m/s', 'm/s', 85, NULL, '2022-09-09 16:31:33', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (98, 'IOT_UNIT', '秒 / ″', '″', 86, NULL, '2022-09-09 16:31:44', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (99, 'IOT_UNIT', '分 / ′', '′', 87, NULL, '2022-09-09 16:31:54', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (100, 'IOT_UNIT', '度 / °', '°', 88, NULL, '2022-09-09 16:32:05', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (101, 'IOT_UNIT', '弧度 / rad', 'rad', 89, NULL, '2022-09-09 16:32:13', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (102, 'IOT_UNIT', '赫兹 / Hz', 'Hz', 90, NULL, '2022-09-09 16:32:23', 'sysadmin', '2022-11-24 15:06:11', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (103, 'IOT_UNIT', '微瓦 / μW', 'μW', 91, NULL, '2022-09-09 16:33:02', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (104, 'IOT_UNIT', '毫瓦 / mW', 'mW', 92, NULL, '2022-09-09 16:33:38', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (105, 'IOT_UNIT', '千瓦特 / kW', 'kW', 93, NULL, '2022-09-09 16:33:52', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (106, 'IOT_UNIT', '瓦特 / W', 'W', 94, NULL, '2022-09-09 16:34:01', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (107, 'IOT_UNIT', '卡路里 / cal', 'cal', 95, NULL, '2022-09-09 16:34:09', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (108, 'IOT_UNIT', '千瓦时 / kW·h', 'kW·h', 96, NULL, '2022-09-09 16:34:17', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (109, 'IOT_UNIT', '瓦时 / Wh', 'Wh', 97, NULL, '2022-09-09 16:34:25', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (110, 'IOT_UNIT', '电子伏 / eV', 'eV', 98, NULL, '2022-09-09 16:34:33', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (111, 'IOT_UNIT', '千焦 / kJ', 'kJ', 99, NULL, '2022-09-09 16:34:46', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (112, 'IOT_UNIT', '焦耳 / J', 'J', 100, NULL, '2022-09-09 16:35:24', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (113, 'IOT_UNIT', '华氏度 / ℉', '℉', 101, NULL, '2022-09-09 16:35:34', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (114, 'IOT_UNIT', '开尔文 / K', 'K', 102, NULL, '2022-09-09 16:35:41', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (115, 'IOT_UNIT', '吨 / t', 't', 103, NULL, '2022-09-09 16:35:49', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (116, 'IOT_UNIT', '摄氏度 / °C', '°C', 104, NULL, '2022-09-09 16:35:58', 'sysadmin', '2022-11-24 15:06:12', NULL, 0);
INSERT INTO `sys_dic_data` VALUES (119, 'IOT_UNIT', '1111111', '111111', 0, '', '2023-03-16 15:11:13', 'jdd', '2023-03-17 14:50:51', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (122, 'sms_channel_code', '阿里云短信', 'ALIYUN', 3, '', '2023-03-21 17:22:37', 'sysadmin', '2023-03-21 17:22:37', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (123, 'robot_channel_code', '企业微信', 'QY_WEIXIN', 1, '', '2023-03-22 17:57:44', 'sysadmin', '2023-03-22 17:58:03', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (124, 'robot_channel_code', '钉钉', 'DING_TALK', 2, '', '2023-03-22 17:57:58', 'sysadmin', '2023-03-22 17:57:58', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (126, 'robot_channel_code', '飞书', 'FEISHU', 3, '', '2023-03-23 14:32:39', 'sysadmin', '2023-03-23 14:32:39', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (127, 'APP_TYPE', '可视服务平台', 'visualization', 1, NULL, '2023-04-21 15:47:37', 'sysadmin', '2023-05-11 10:19:30', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (128, 'APP_TYPE', '基础支撑平台', 'foundation', 2, NULL, '2023-04-21 15:48:09', 'sysadmin', '2023-05-11 10:19:25', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (129, 'APP_TYPE', '移动应用中心', 'business', 3, NULL, '2023-04-21 15:48:49', 'sysadmin', '2023-05-11 10:19:19', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (132, 'mobile_app_type', 'H5应用', 'H5_app', 0, 'H5应用', '2023-05-23 17:28:37', 'sysadmin', '2023-05-23 17:28:37', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (133, 'ACT_CATEGORY', '测试流程', '0', 0, '', '2023-05-31 08:55:28', 'jdd01', '2023-05-31 08:55:28', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (134, 'ACT_CATEGORY', 'OA', '1', 1, '', '2023-05-31 08:55:58', 'jdd01', '2023-05-31 08:55:58', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (135, 'BPM_STATUS', '已驳回', '6', 0, '', '2023-05-31 16:47:43', 'sysadmin', '2023-05-31 16:47:43', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (136, 'BPM_STATUS', '已挂起', '5', 0, '', '2023-05-31 16:47:58', 'sysadmin', '2023-05-31 16:47:58', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (137, 'BPM_STATUS', '已取消', '4', 0, '', '2023-05-31 16:48:05', 'sysadmin', '2023-05-31 16:48:05', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (138, 'BPM_STATUS', '已完成', '3', 0, '', '2023-05-31 16:48:11', 'sysadmin', '2023-05-31 16:48:11', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (139, 'BPM_STATUS', '处理中', '2', 0, '', '2023-05-31 16:48:18', 'sysadmin', '2023-05-31 16:48:18', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (140, 'BPM_STATUS', '待提交', '1', 0, '', '2023-05-31 16:48:26', 'sysadmin', '2023-05-31 16:48:26', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (141, 'BPM_COMMENT', '不同意', '2', 0, '', '2023-05-31 16:48:48', 'sysadmin', '2023-05-31 16:48:48', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (142, 'BPM_COMMENT', '同意', '1', 0, '', '2023-05-31 16:48:54', 'sysadmin', '2023-05-31 16:48:54', 'sysadmin', 0);
INSERT INTO `sys_dic_data` VALUES (143, 'ACT_LEAVE_TYPE', '事假', '1', 1, '', '2023-06-06 09:38:34', 'jdd01', '2023-06-06 09:38:34', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (144, 'ACT_LEAVE_TYPE', '病假', '2', 2, '', '2023-06-06 09:38:43', 'jdd01', '2023-06-06 09:38:43', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (145, 'ACT_LEAVE_TYPE', '婚假', '3', 3, '', '2023-06-06 09:38:53', 'jdd01', '2023-06-06 09:38:53', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (146, 'ACT_LEAVE_TYPE', '产假', '4', 4, '', '2023-06-06 09:39:08', 'jdd01', '2023-06-06 09:39:08', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (147, 'ACT_LEAVE_TYPE', '陪产假', '5', 5, '', '2023-06-06 09:39:18', 'jdd01', '2023-06-06 09:39:18', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (148, 'ACT_LEAVE_TYPE', '其他', '6', 6, '', '2023-06-06 09:39:27', 'jdd01', '2023-06-06 09:39:27', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (149, 'ACT_LEAVE_TYPE', '丧假', '7', 7, '', '2023-06-06 09:39:43', 'jdd01', '2023-06-06 09:39:43', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (150, 'ACT_LEAVE_TYPE', '探亲假', '8', 8, '', '2023-06-06 09:39:57', 'jdd01', '2023-06-06 09:39:57', 'jdd01', 0);
INSERT INTO `sys_dic_data` VALUES (151, 'ACT_LEAVE_TYPE', '产检假', '9', 9, '', '2023-06-06 09:40:08', 'jdd01', '2023-06-06 09:40:08', 'jdd01', 0);

-- ----------------------------
-- Table structure for sys_dic_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dic_type`;
CREATE TABLE `sys_dic_type`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dic_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `dic_code` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `remark` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_diccode`(`dic_code`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_dic_type
-- ----------------------------
INSERT INTO `sys_dic_type` VALUES (1, '单位', 'IOT_UNIT', '物联网系统单位', '2023-03-08 09:03:30', 'sysadmin', '2023-03-21 17:20:45', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (6, '短信渠道类型', 'sms_channel_code', NULL, '2023-03-21 17:21:31', 'sysadmin', '2023-03-21 17:21:31', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (7, '群聊机器人', 'robot_channel_code', NULL, '2023-03-22 17:57:28', 'sysadmin', '2023-03-22 17:57:28', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (9, '应用类别', 'APP_TYPE', NULL, '2023-04-21 15:37:29', 'sysadmin', '2023-04-21 15:37:29', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (12, '移动端应用类别', 'mobile_app_type', '移动端应用类别', '2023-05-23 17:28:05', 'sysadmin', '2023-05-23 17:28:05', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (13, '工单中心类别', 'ACT_CATEGORY', '流程引擎相关', '2023-05-31 08:55:04', 'jdd01', '2023-05-31 08:55:04', 'jdd01', 0);
INSERT INTO `sys_dic_type` VALUES (14, '业务状态', 'BPM_STATUS', NULL, '2023-05-31 16:47:19', 'sysadmin', '2023-05-31 16:47:19', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (15, '审批结果', 'BPM_COMMENT', NULL, '2023-05-31 16:48:36', 'sysadmin', '2023-05-31 16:48:36', 'sysadmin', 0);
INSERT INTO `sys_dic_type` VALUES (16, '请假类型', 'ACT_LEAVE_TYPE', '流程管理-请假类型', '2023-06-06 09:38:19', 'jdd01', '2023-06-06 09:38:19', 'jdd01', 0);

-- ----------------------------
-- Table structure for sys_file
-- ----------------------------
DROP TABLE IF EXISTS `sys_file`;
CREATE TABLE `sys_file`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '文件编号',
  `name` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件名',
  `path` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件路径',
  `url` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文件 URL',
  `type` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL COMMENT '文件类型',
  `size` int(11) NOT NULL COMMENT '文件大小',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bit(1) NOT NULL DEFAULT b'0' COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '文件表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_file
-- ----------------------------
INSERT INTO `sys_file` VALUES (2, 'cover1686302808280.jpg', '5f78a9557f83ae512929c22db7bd6864727d9a68767b60361df55aadb33ab58a.jpg', 'https://shaxian.oss-cn-shanghai.aliyuncs.com/5f78a9557f83ae512929c22db7bd6864727d9a68767b60361df55aadb33ab58a.jpg', 'image/jpeg', 41401, 'sysadmin', '2023-06-09 17:26:48', 'sysadmin', '2023-06-09 17:26:48', b'0');

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否并发执行（Y是 N否）',
  `pause` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'Y' COMMENT '是否暂停(Y是，N否)',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '备注信息',
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job
-- ----------------------------

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '日志信息',
  `status` char(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'S' COMMENT '执行状态（S正常 F失败）',
  `exception_info` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '异常信息',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '定时任务调度日志表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '日志类型S-成功，F-失败',
  `title` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'ip地址',
  `user_agent` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '浏览器类型',
  `method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求方式',
  `request_uri` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `params` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `time` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '消耗时长',
  `exception` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 163 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_log
-- ----------------------------
INSERT INTO `sys_log` VALUES (132, 'S', '编辑菜单', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/menu', '', 63, NULL, 'sysadmin', '2023-06-09 14:06:06');
INSERT INTO `sys_log` VALUES (133, 'S', '编辑菜单', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 52, NULL, 'sysadmin', '2023-06-09 14:23:53');
INSERT INTO `sys_log` VALUES (134, 'S', '编辑菜单', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 83, NULL, 'sysadmin', '2023-06-09 14:37:07');
INSERT INTO `sys_log` VALUES (135, 'F', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 77, NULL, 'sysadmin', '2023-06-09 14:47:41');
INSERT INTO `sys_log` VALUES (136, 'F', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 58555, NULL, 'sysadmin', '2023-06-09 14:50:11');
INSERT INTO `sys_log` VALUES (137, 'F', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 69, NULL, 'sysadmin', '2023-06-09 14:51:00');
INSERT INTO `sys_log` VALUES (138, 'F', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 94942, NULL, 'sysadmin', '2023-06-09 14:55:28');
INSERT INTO `sys_log` VALUES (139, 'S', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 80, NULL, 'sysadmin', '2023-06-09 14:59:53');
INSERT INTO `sys_log` VALUES (140, 'S', '新增菜单', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/menu', '', 80, NULL, 'sysadmin', '2023-06-09 15:26:09');
INSERT INTO `sys_log` VALUES (141, 'S', '新增部门', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/dept', '', 162, NULL, 'sysadmin', '2023-06-09 15:31:42');
INSERT INTO `sys_log` VALUES (142, 'F', '编辑角色', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/role', '', 11, '内置角色无法操作！', 'sysadmin', '2023-06-09 16:25:02');
INSERT INTO `sys_log` VALUES (143, 'F', '编辑角色', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/role', '', 7, '内置角色无法操作！', 'sysadmin', '2023-06-09 16:25:38');
INSERT INTO `sys_log` VALUES (144, 'S', '新增角色', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role', '', 47, NULL, 'sysadmin', '2023-06-09 16:26:11');
INSERT INTO `sys_log` VALUES (145, 'S', '设置功能权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/function', '', 96, NULL, 'sysadmin', '2023-06-09 16:26:21');
INSERT INTO `sys_log` VALUES (146, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 108, NULL, 'sysadmin', '2023-06-09 16:26:31');
INSERT INTO `sys_log` VALUES (147, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 101, NULL, 'sysadmin', '2023-06-09 16:27:41');
INSERT INTO `sys_log` VALUES (148, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 59, NULL, 'sysadmin', '2023-06-09 16:32:48');
INSERT INTO `sys_log` VALUES (149, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 59, NULL, 'sysadmin', '2023-06-09 16:33:00');
INSERT INTO `sys_log` VALUES (150, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 68, NULL, 'sysadmin', '2023-06-09 16:34:13');
INSERT INTO `sys_log` VALUES (151, 'S', '设置数据权限', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/role/scope/data', '', 56, NULL, 'sysadmin', '2023-06-09 16:35:40');
INSERT INTO `sys_log` VALUES (152, 'S', '修改系统参数', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/config', '', 85, NULL, 'sysadmin', '2023-06-09 17:01:53');
INSERT INTO `sys_log` VALUES (153, 'S', '修改系统参数', '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/config', '', 41, NULL, 'sysadmin', '2023-06-09 17:01:55');
INSERT INTO `sys_log` VALUES (154, 'S', '新增通知', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/notice', '', 170, NULL, 'sysadmin', '2023-06-09 17:26:52');
INSERT INTO `sys_log` VALUES (155, 'S', '修改通知', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/notice', '', 60, NULL, 'sysadmin', '2023-06-09 17:26:55');
INSERT INTO `sys_log` VALUES (156, 'S', '阅读了通知公告', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/notice/read/2', '', 84, NULL, 'sysadmin', '2023-06-09 17:26:59');
INSERT INTO `sys_log` VALUES (157, 'S', '阅读了通知公告', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/notice/read/1', '', 64, NULL, 'sysadmin', '2023-06-09 17:27:03');
INSERT INTO `sys_log` VALUES (158, 'S', '编辑菜单', '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 71, NULL, 'sysadmin', '2023-06-09 17:27:17');
INSERT INTO `sys_log` VALUES (159, 'S', '新增菜单', '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'POST', '/api/system/menu', '', 55, NULL, 'sysadmin', '2023-07-06 16:44:35');
INSERT INTO `sys_log` VALUES (160, 'S', '编辑菜单', '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 63, NULL, 'sysadmin', '2023-07-06 16:44:46');
INSERT INTO `sys_log` VALUES (161, 'S', '编辑菜单', '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 48, NULL, 'sysadmin', '2023-07-06 16:44:54');
INSERT INTO `sys_log` VALUES (162, 'S', '编辑菜单', '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'PUT', '/api/system/menu', '', 36, NULL, 'sysadmin', '2023-07-06 16:47:19');

-- ----------------------------
-- Table structure for sys_login_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_login_log`;
CREATE TABLE `sys_login_log`  (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `log_type` bigint(20) NOT NULL COMMENT '日志类型',
  `trace_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '链路追踪编号',
  `user_id` bigint(20) NOT NULL DEFAULT 0 COMMENT '用户编号',
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '用户账号',
  `result` tinyint(4) NOT NULL COMMENT '登陆结果',
  `user_ip` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '用户 IP',
  `user_agent` varchar(512) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '浏览器 UA',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT '' COMMENT '更新者',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` bigint(20) NOT NULL DEFAULT 0 COMMENT '是否删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci COMMENT = '系统访问记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_login_log
-- ----------------------------
INSERT INTO `sys_login_log` VALUES (1, 100, '', 1, 'sysadmin', 10, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', NULL, '2023-06-08 16:29:47', NULL, '2023-06-08 16:29:47', 0);
INSERT INTO `sys_login_log` VALUES (2, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-08 16:35:07', 'sysadmin', '2023-06-08 16:35:07', 0);
INSERT INTO `sys_login_log` VALUES (3, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-08 16:36:11', 'sysadmin', '2023-06-08 16:36:11', 0);
INSERT INTO `sys_login_log` VALUES (4, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-08 16:39:06', 'sysadmin', '2023-06-08 16:39:06', 0);
INSERT INTO `sys_login_log` VALUES (5, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-08 17:49:18', 'sysadmin', '2023-06-08 17:49:18', 0);
INSERT INTO `sys_login_log` VALUES (6, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-08 17:52:34', 'sysadmin', '2023-06-08 17:52:34', 0);
INSERT INTO `sys_login_log` VALUES (7, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-09 10:07:05', 'sysadmin', '2023-06-09 10:07:05', 0);
INSERT INTO `sys_login_log` VALUES (8, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 10:44:13', 'sysadmin', '2023-06-09 10:44:13', 0);
INSERT INTO `sys_login_log` VALUES (9, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 10:44:23', 'sysadmin', '2023-06-09 10:44:23', 0);
INSERT INTO `sys_login_log` VALUES (10, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 10:45:06', 'sysadmin', '2023-06-09 10:45:06', 0);
INSERT INTO `sys_login_log` VALUES (11, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 10:47:52', 'sysadmin', '2023-06-09 10:47:52', 0);
INSERT INTO `sys_login_log` VALUES (12, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 10:50:49', 'sysadmin', '2023-06-09 10:50:49', 0);
INSERT INTO `sys_login_log` VALUES (13, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 11:04:49', 'sysadmin', '2023-06-09 11:04:49', 0);
INSERT INTO `sys_login_log` VALUES (14, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 14:01:15', 'sysadmin', '2023-06-09 14:01:15', 0);
INSERT INTO `sys_login_log` VALUES (15, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 14:05:34', 'sysadmin', '2023-06-09 14:05:34', 0);
INSERT INTO `sys_login_log` VALUES (16, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 14:23:17', 'sysadmin', '2023-06-09 14:23:17', 0);
INSERT INTO `sys_login_log` VALUES (17, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 14:23:24', 'sysadmin', '2023-06-09 14:23:24', 0);
INSERT INTO `sys_login_log` VALUES (18, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 14:27:49', 'sysadmin', '2023-06-09 14:27:49', 0);
INSERT INTO `sys_login_log` VALUES (19, 100, '', 1, 'sysadmin', 0, '192.168.5.106', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 15:09:41', 'sysadmin', '2023-06-09 15:09:41', 0);
INSERT INTO `sys_login_log` VALUES (20, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 15:55:44', 'sysadmin', '2023-06-09 15:55:44', 0);
INSERT INTO `sys_login_log` VALUES (21, 100, '', 1, 'sysadmin', 0, '192.168.5.103', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 16:03:28', 'sysadmin', '2023-06-09 16:03:28', 0);
INSERT INTO `sys_login_log` VALUES (22, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-06-09 16:11:10', 'sysadmin', '2023-06-09 16:11:10', 0);
INSERT INTO `sys_login_log` VALUES (23, 100, '', 1, 'sysadmin', 0, '192.168.5.114', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/111.0.0.0 Safari/537.36', 'sysadmin', '2023-06-15 15:34:20', 'sysadmin', '2023-06-15 15:34:20', 0);
INSERT INTO `sys_login_log` VALUES (24, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Apifox/1.0.0 (https://www.apifox.cn)', 'sysadmin', '2023-06-28 10:13:58', 'sysadmin', '2023-06-28 10:13:58', 0);
INSERT INTO `sys_login_log` VALUES (25, 100, '', 1, 'sysadmin', 0, '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36', 'sysadmin', '2023-06-30 14:04:44', 'sysadmin', '2023-06-30 14:04:44', 0);
INSERT INTO `sys_login_log` VALUES (26, 100, '', 1, 'sysadmin', 0, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-07-06 16:32:56', 'sysadmin', '2023-07-06 16:32:56', 0);
INSERT INTO `sys_login_log` VALUES (27, 100, '', 1, 'sysadmin', 0, '192.168.5.112', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36', 'sysadmin', '2023-07-06 17:18:41', 'sysadmin', '2023-07-06 17:18:41', 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '菜单名',
  `permission` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识符',
  `path` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路径',
  `parent_id` bigint(20) UNSIGNED NOT NULL COMMENT '父级ID',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '图标',
  `order_no` bigint(20) UNSIGNED NULL DEFAULT NULL COMMENT '排序',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '类型',
  `keep_alive` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '是否路由缓冲，Y-是，N-否',
  `open_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '打开方式，1-内部，2-外部',
  `hidden` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' COMMENT '是否隐藏 Y-是 N-否',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` bigint(20) UNSIGNED NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 446 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', NULL, '/system', 0, 'fa fa-sitemap', 999, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-04-28 11:03:08', '2023-06-08 10:18:16', 0);
INSERT INTO `sys_menu` VALUES (3, '用户管理', NULL, '/system/user', 1, 'el-icon-office-building', 1, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-04-28 11:08:59', '2023-06-09 11:27:57', 0);
INSERT INTO `sys_menu` VALUES (4, '菜单管理', NULL, '/system/menu', 1, 'fa fa-list-ul', 2, 'M', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-04-28 11:09:47', '2023-06-09 11:27:59', 0);
INSERT INTO `sys_menu` VALUES (6, '新增', 'sysuser.add', NULL, 3, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-04-28 11:12:38', '2023-06-08 10:18:16', 0);
INSERT INTO `sys_menu` VALUES (14, '部门管理', NULL, '/system/dept', 1, 'fa fa-institution', 3, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-24 11:19:38', '2023-06-09 11:28:01', 0);
INSERT INTO `sys_menu` VALUES (21, '角色管理', NULL, '/system/role', 1, 'fa fa-user-circle', 4, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-27 16:05:55', '2023-06-09 11:28:04', 0);
INSERT INTO `sys_menu` VALUES (22, '字典管理', NULL, '/system/dic', 1, 'fa fa-th-list', 5, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-30 19:55:25', '2023-06-09 11:28:55', 0);
INSERT INTO `sys_menu` VALUES (23, '参数设置', NULL, '/system/config', 1, 'el-icon-office-building', 6, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-30 19:56:31', '2023-06-09 11:29:11', 0);
INSERT INTO `sys_menu` VALUES (24, '日志管理', NULL, '/system/log', 1, 'fa fa-list-ol', 7, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-30 20:10:59', '2023-06-09 11:29:34', 0);
INSERT INTO `sys_menu` VALUES (25, '定时任务', NULL, '/system/timedtask', 1, 'fa fa-recycle', 8, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-30 20:11:43', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (26, '文件管理', NULL, '/system/file', 1, 'fa fa-folder', 9, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-30 20:12:21', '2023-06-09 11:30:01', 0);
INSERT INTO `sys_menu` VALUES (27, '修改', 'sysuser.edit', NULL, 3, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:12:49', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (28, '删除', 'sysuser.del', NULL, 3, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:13:12', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (29, '重置密码', 'sysuser.reset', NULL, 3, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:13:48', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (30, '新增', 'sysmenu.add', NULL, 4, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:14:18', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (31, '修改', 'sysmenu.edit', NULL, 4, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:14:47', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (32, '删除', 'sysmenu.del', NULL, 4, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:15:03', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (33, '新增', 'sysdept.add', NULL, 14, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:15:33', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (34, '修改', 'sysdept.edit', NULL, 14, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:15:51', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (35, '删除', 'sysdept.del', NULL, 14, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:16:11', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (36, '新增', 'sysrole.add', NULL, 21, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:17:12', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (37, '修改', 'sysrole.edit', NULL, 21, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:17:36', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (38, '删除', 'sysrole.del', NULL, 21, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:17:53', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (39, '数据权限', 'sysrole.data', NULL, 21, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:18:27', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (40, '新增', 'sysdic.add', NULL, 22, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:19:05', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (41, '修改', 'sysdic.edit', NULL, 22, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:19:21', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (42, '删除', 'sysdic.del', NULL, 22, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:19:34', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (43, '新增', 'sysconfig.add', NULL, 23, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:20:04', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (44, '修改', 'sysconfig.edit', NULL, 23, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:20:37', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (45, '删除', 'sysconfig.del', NULL, 23, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:20:50', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (46, '删除', 'syslog.del', NULL, 24, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:21:17', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (47, '新增', 'sysjob.add', NULL, 25, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:22:05', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (48, '修改', 'sysjob.edit', NULL, 25, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:22:17', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (49, '删除', 'sysjob.del', NULL, 25, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:22:28', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (50, '执行一次', 'sysjob.one', NULL, 25, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:22:41', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (51, '新增', 'sysfile.add', NULL, 26, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:23:30', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (52, '下载', 'sysfile.down', NULL, 26, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:23:45', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (53, '删除', 'sysfile.del', NULL, 26, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 19:23:57', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (54, '功能权限', 'sysrole.func', NULL, 21, NULL, 5, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 20:23:53', '2023-06-08 10:18:17', 0);
INSERT INTO `sys_menu` VALUES (55, '调度日志', 'sysjob.log', NULL, 25, NULL, 5, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-05-31 20:25:02', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (61, '缓存', 'sysconfig.cache', NULL, 23, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-01 20:17:41', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (62, '缓存', 'sysdic.cache', NULL, 22, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-01 20:18:12', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (63, '通知管理', NULL, '/system/notice', 1, 'el-icon-message-solid', 10, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-02 14:25:25', '2023-06-09 11:30:30', 0);
INSERT INTO `sys_menu` VALUES (65, '新增', 'sysnotice.add', NULL, 63, NULL, 1, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-06 13:19:53', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (66, '修改', 'sysnotice.edit', NULL, 63, NULL, 2, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-06 13:20:14', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (67, '删除', 'sysnotice.del', NULL, 63, NULL, 3, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-06 13:20:38', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (68, '发布', 'sysnotice.pub', NULL, 63, NULL, 4, 'B', 'Y', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-06-06 13:20:59', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (111, '大屏设计器', NULL, 'http://118.31.46.229:9095/', 1, 'fa fa-window-maximize', 99, 'M', 'N', '2', 'Y', NULL, 'sysadmin', 'sysadmin', '2022-06-10 14:59:49', '2023-06-08 10:20:15', 0);
INSERT INTO `sys_menu` VALUES (249, '协同管理', NULL, '/system/collaborate', 0, 'fa fa-cc', 12, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-12 11:21:08', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (250, '流程设计', NULL, '/system/collaborate/model', 249, 'fa fa-hacker-news', 1, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-12 11:21:43', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (251, '表单设计', NULL, '/system/collaborate/formd', 249, 'fa fa-cloud', 2, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-15 14:49:24', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (253, '办公中心', NULL, '/system/collaborate/officeCenter', 249, 'fa fa-sitemap', 4, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-26 13:40:46', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (254, '工单中心', NULL, '/system/collaborate/officeCenter/workorder', 253, 'el-icon-user-solid', 1, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-26 13:41:34', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (255, '我的申请', NULL, '/system/collaborate/officeCenter/myapply', 253, 'fa fa-battery-2', 2, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-26 13:42:04', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (256, '我的待办', NULL, '/system/collaborate/officeCenter/mycommission', 253, 'fa fa-battery-0', 3, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-26 13:43:02', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (257, '我的抄送', NULL, '/system/collaborate/officeCenter/mycc', 253, 'el-icon-monitor', 4, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2022-07-26 13:43:18', '2023-06-08 10:18:18', 0);
INSERT INTO `sys_menu` VALUES (260, '分配角色', 'sys.user.assign-roles', NULL, 3, NULL, 1, 'B', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2023-06-09 15:26:09', '2023-06-09 15:26:09', 0);
INSERT INTO `sys_menu` VALUES (391, '邮箱管理', NULL, '/message/mail', 445, 'fa fa-paw', 1, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-09 17:07:42', '2023-05-09 16:22:12', 0);
INSERT INTO `sys_menu` VALUES (392, '邮箱账号', NULL, '/message/mail/account', 391, 'el-icon-s-claim', 1, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-09 17:07:57', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (393, '邮箱模板', NULL, '/message/mail/template', 391, 'fa fa-battery-0', 2, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-09 17:08:17', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (394, '邮箱记录', NULL, '/message/mail/log', 391, 'fa fa-code-fork', 3, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-09 17:08:31', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (395, '新增', 'mail-account.add', NULL, 392, NULL, 1, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:47:40', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (396, '编辑', 'mail-account.edit', NULL, 392, NULL, 2, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:48:00', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (397, '删除', 'mail-account.del', NULL, 392, NULL, 3, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:48:12', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (398, '新增', 'mail-template.add', NULL, 393, NULL, 1, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:48:49', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (399, '编辑', 'mail-template.edit', NULL, 393, NULL, 2, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:49:19', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (400, '删除', 'mail-template.del', NULL, 393, NULL, 3, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:49:31', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (401, '详情', 'mail-log.detail', NULL, 394, NULL, 1, 'B', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-13 09:51:12', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (415, '短信管理', NULL, '/message/sms', 445, 'fa fa-ravelry', 2, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-16 15:36:19', '2023-05-09 16:22:12', 0);
INSERT INTO `sys_menu` VALUES (416, '短信渠道', NULL, '/message/sms/channel', 415, 'fa fa-reorder', 1, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-16 15:36:52', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (418, '短信模板', NULL, '/message/sms/template', 415, 'fa fa-paw', 2, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 09:52:58', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (419, '短信日志', NULL, '/message/sms/log', 415, 'fa fa-universal-access', 3, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 11:38:18', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (420, '新增', 'sms-channel.add', NULL, 416, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:48:24', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (421, '修改', 'sms-channel.edit', NULL, 416, NULL, 2, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:48:46', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (422, '删除', 'sms-channel.del', NULL, 416, NULL, 3, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:48:59', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (423, '新增', 'sms-template.add', NULL, 418, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:49:17', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (424, '修改', 'sms-template.edit', NULL, 418, NULL, 2, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:49:30', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (425, '删除', 'sms-template.del', NULL, 418, NULL, 3, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:49:46', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (426, '详情', 'sms-log.detail', NULL, 419, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-17 13:50:06', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (432, '群聊机器人', NULL, '/message/robot', 445, 'fa fa-video-camera', 3, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-22 17:10:43', '2023-05-09 16:22:12', 0);
INSERT INTO `sys_menu` VALUES (433, '机器人账号', NULL, '/message/robot/channel', 432, 'fa fa-cutlery', 1, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-22 17:11:38', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (434, '消息模板', NULL, '/message/robot/template', 432, 'el-icon-message-solid', 2, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-22 17:12:09', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (435, '发送日志', NULL, '/message/robot/log', 432, 'el-icon-document-add', 3, 'M', 'N', '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-22 17:12:38', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (436, '新增', 'robot-channel.add', NULL, 433, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 13:45:20', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (437, '修改', 'robot-channel.edit', NULL, 433, NULL, 2, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 13:46:11', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (438, '删除', 'robot-channel.del', NULL, 433, NULL, 3, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 13:46:37', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (439, '新增', 'robot-template.add', NULL, 434, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 14:06:29', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (440, '修改', 'robot-template.edit', NULL, 434, NULL, 2, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 14:07:04', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (441, '删除', 'robot-template.del', NULL, 434, NULL, 3, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 14:07:24', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (442, '详情', 'robot-log.detail', NULL, 435, NULL, 1, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-23 14:08:42', '2023-04-23 17:17:34', 0);
INSERT INTO `sys_menu` VALUES (443, '测试', 'mail-template.test', NULL, 393, NULL, 4, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-24 09:39:58', '2023-04-23 17:17:12', 0);
INSERT INTO `sys_menu` VALUES (444, '测试', 'sms-template.test', NULL, 418, NULL, 4, 'B', NULL, '1', 'N', '', 'sysadmin', 'sysadmin', '2023-03-24 09:40:26', '2023-04-23 17:16:21', 0);
INSERT INTO `sys_menu` VALUES (445, '消息系统', NULL, '/message', 0, 'fa fa-send-o', 1, 'M', 'N', '1', 'N', NULL, 'sysadmin', 'sysadmin', '2023-07-06 16:44:35', '2023-07-06 16:44:35', 0);

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '标题',
  `author` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '作者姓名',
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '内容',
  `cover` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '封面',
  `digest` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '摘要',
  `ip_addr` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `published` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否已发布：Y-是，N-否',
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES (1, 'jfgj', 'sysadmin', '<p>gdfgdf</p>', '/upload/df51c0411f50417dbd6706892d3f0f47.jpg', 'gdfgdf', '192.168.0.45', 'Y', '2022-07-14 17:22:18', 'sysadmin', '2022-07-14 17:22:18', 'sysadmin', 0);
INSERT INTO `sys_notice` VALUES (2, 'fdgfg', 'sysadmin', '<p>gdfgdfg</p>', 'https://shaxian.oss-cn-shanghai.aliyuncs.com/5f78a9557f83ae512929c22db7bd6864727d9a68767b60361df55aadb33ab58a.jpg', 'gdfgdfg', '127.0.0.1', 'Y', '2023-06-09 17:26:51', 'sysadmin', '2023-06-09 17:26:51', 'sysadmin', 0);

-- ----------------------------
-- Table structure for sys_notice_reader
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice_reader`;
CREATE TABLE `sys_notice_reader`  (
  `notice_id` bigint(20) NOT NULL COMMENT '通知id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `is_read` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '是否已读',
  PRIMARY KEY (`notice_id`, `user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_notice_reader
-- ----------------------------
INSERT INTO `sys_notice_reader` VALUES (1, 1, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 1, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 26, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 27, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 28, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 29, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 30, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 31, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 32, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 33, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 34, 'Y');
INSERT INTO `sys_notice_reader` VALUES (2, 35, 'Y');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_code` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色编码',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名',
  `data_scope` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '数据权限，默认全部数据权限',
  `data_scope_dept_ids` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '数据范围',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '2' COMMENT '数据类型，1内置角色，2自定义',
  `remark` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_code`(`role_code`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, 'super_admin', '超级管理员', '1', NULL, '1', NULL, 'sysadmin', '2022-07-08 14:30:27', '2023-06-09 09:14:48', 'sysadmin', 0);
INSERT INTO `sys_role` VALUES (4, 'dfhgsgfdhgf', 'kkkk', '5', '[1667071956966473729]', '2', NULL, 'sysadmin', '2023-06-09 16:26:11', '2023-06-09 16:26:11', 'sysadmin', 0);

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) UNSIGNED NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`, `menu_id`) USING BTREE,
  UNIQUE INDEX `uk_role_menu`(`role_id`, `menu_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (4, 249);
INSERT INTO `sys_role_menu` VALUES (4, 250);
INSERT INTO `sys_role_menu` VALUES (4, 251);
INSERT INTO `sys_role_menu` VALUES (4, 253);
INSERT INTO `sys_role_menu` VALUES (4, 254);
INSERT INTO `sys_role_menu` VALUES (4, 255);
INSERT INTO `sys_role_menu` VALUES (4, 256);
INSERT INTO `sys_role_menu` VALUES (4, 257);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `dept_id` bigint(20) UNSIGNED NOT NULL COMMENT '部门ID',
  `dept_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '部门名',
  `username` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户名',
  `nickname` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '昵称/姓名',
  `password` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '密码',
  `mobile` char(11) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `sex` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'M' COMMENT 'M:男，W:女',
  `locked` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT 'N' COMMENT '账号状态（Y是，N否）',
  `type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '2' COMMENT '用户类型，1内置用户，2其它',
  `create_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `update_by` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `create_time` datetime NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `deleted` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uk_username`(`username`, `deleted`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 7, '体育馆', 'sysadmin', 'sysadmin', 'e10adc3949ba59abbe56e057f20f883e', '19965411190', 'ydjhrtjhrtsfvu@gmail.com', 'M', 'N', '2', NULL, 'admin', '2022-04-20 15:50:58', '2023-06-08 16:30:34', 0);
INSERT INTO `sys_user` VALUES (26, 1547843855720591362, 'gdsgdg', 'sunyawei', '孙亚炜', '$2a$10$wbEVOflbVpg5Ezfq77psgugAye6aI8iYtcQv1tF9kfsrNA4CPIbe.', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:22:38', '2022-07-22 14:00:20', 0);
INSERT INTO `sys_user` VALUES (27, 1547843855720591362, 'gdsgdg', 'guojian', '郭建', '$2a$10$JVMs/j3l3jyRFMgVKRAEcutPN8SbUR3m6qafhxj7WIndp5V4OqRSq', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:22:51', '2022-07-22 23:50:40', 0);
INSERT INTO `sys_user` VALUES (28, 1547841349451984898, 'mfghm', 'tingting', '李婷婷', '$2a$10$I9zLZzlK0y.iS0EmgtHMm.s516UUskz7Ug2w0cVa9IdTh36.iARc.', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:01', '2022-07-25 10:03:42', 0);
INSERT INTO `sys_user` VALUES (29, 1547843855720591362, 'gdsgdg', 'qwqwqw', 'wqw', '$2a$10$93.KYxPaIbn6Vu6j1fKrd.DdMVhglewKQE9x5bbXW8z2I07TtXXSW', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:11', '2022-07-15 15:23:11', 0);
INSERT INTO `sys_user` VALUES (30, 1547843401301307394, 'hth', 'wqwqwqw', 'wqw', '$2a$10$/bTngi7qLXbW65qQnYxYxucxGf9hy5nLtMC/2F3goFhRs3YOhVh1u', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:20', '2022-07-15 15:23:20', 0);
INSERT INTO `sys_user` VALUES (31, 1547841349451984898, 'mfghm', 'wqeqew', 'qwqewe', '$2a$10$ILvbjDxhv8h.LFuNkgKFCeHpTkCBOFoizd96JX8jVDCdmKP3JQZWe', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:31', '2022-07-15 15:23:31', 0);
INSERT INTO `sys_user` VALUES (32, 1547843401301307394, 'hth', 'ewqeqwe', 'qwe', '$2a$10$BYmeD1l9OWpb.jOztHRn8eM68diypPceslWZpZipCimqqMIU.NpVu', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:40', '2022-07-15 15:23:40', 0);
INSERT INTO `sys_user` VALUES (33, 1547841349451984898, 'mfghm', 'eqweqw', 'ewqewq', '$2a$10$5DVYqSTI2nIGO.Q2B8fzKeJR1Qxrm2p/CMFxKm2a3HLMPzBXa.y1e', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:23:49', '2022-07-15 15:23:49', 0);
INSERT INTO `sys_user` VALUES (34, 1547841349451984898, 'mfghm', 'ewqeqwewq', 'ewqeq', '$2a$10$IZFCiRZxMpZfkPjRIoIGt.31wp6NMzNT76Xwpu3ZVlGwYgFmkEydO', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:24:00', '2022-07-15 15:24:00', 0);
INSERT INTO `sys_user` VALUES (35, 1547841349451984898, 'mfghm', 'eewdsa', 'dasd', '$2a$10$ApTcWqlXXM6w4KirVg55KubBs9Shmj2AJDSNXMj43D5i.ow0EQ2fe', NULL, NULL, 'M', 'N', '2', 'sysadmin', 'sysadmin', '2022-07-15 15:24:11', '2022-07-15 15:24:11', 0);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint(20) UNSIGNED NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`, `role_id`) USING BTREE,
  UNIQUE INDEX `uk_user_role`(`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (26, 1);
INSERT INTO `sys_user_role` VALUES (27, 1);
INSERT INTO `sys_user_role` VALUES (28, 1);
INSERT INTO `sys_user_role` VALUES (29, 1);
INSERT INTO `sys_user_role` VALUES (30, 1);
INSERT INTO `sys_user_role` VALUES (31, 1);
INSERT INTO `sys_user_role` VALUES (32, 1);
INSERT INTO `sys_user_role` VALUES (33, 1);
INSERT INTO `sys_user_role` VALUES (34, 1);
INSERT INTO `sys_user_role` VALUES (35, 1);

SET FOREIGN_KEY_CHECKS = 1;
