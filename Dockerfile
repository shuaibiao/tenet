FROM moxm/java:1.8-full

RUN mkdir -p /tenet-boot

WORKDIR /tenet-boot

ARG JAR_FILE=tenet-boot-encrypted.jar

ARG XJAR_FILE=xjar

COPY ${JAR_FILE} app.jar

COPY ${XJAR_FILE} xjar

EXPOSE 8082

ENV TZ=Asia/Shanghai JAVA_OPTS="-Xms128m -Xmx256m -Djava.security.egd=file:/dev/./urandom"

CMD ./xjar java $JAVA_OPTS -jar app.jar
