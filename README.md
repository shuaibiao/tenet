## 系统简介
Tenet 是一套全部开源的快速开发平台，毫无保留给个人及企业免费使用。

- 前后端分离。
- 基于 SpringBoot2 、 Sa-token 的 RBAC 权限管理系统
- 基于 Redis 的 Token 储存机制，可自动续期。
- 实现了数据权限自定义模块，提供了注解声明。
- 基于 Quartz 的任务调度平台，支持分布式任务调度。
- 提供前端 Vue2 + EleUI 的前端项目。

## 内置功能
- 系统管理
    - 用户管理
    - 菜单管理
    - 部门管理
    - 角色管理
    - 字典管理
    - 参数设置
    - 日志管理
    - 定时任务
    - 文件管理
    - 通知管理  

## 启动
### 后端启动
启动类于 tenet-boot-start 中，创建好你的数据库，执行 db 文件夹中的 sql 脚本，修改数据库连接与redis配置连接，启动后端服务。

输入：http://localhost:8082/api/doc.html 访问接口文档

### 前端启动
代码在 tenet-web-vue2文件夹下，安装node环境，安装必要的包，配置 vue.config.js 中的后端服务，启动即可。

## 部署
提供了 Dockerfile 与 docker-compose 方式，并且提供了 Jenkinsfile 的模板，如需使用，根据自身修改。

## 交流
有任何问题，请在 Issues 中留言，一一回复。