import Vue from "vue"
import App from "./App.vue"
import router from "./router"
import store, { setupStore, setupInfo } from "./store"
import { selfAdaption } from "@/utils"
import "normalize.css/normalize.css"
import "./permission"
import "./icons"
import DICT_TYPE from "@/utils/dict_settings"

import "./utils/global_component" //插件、全局组件
Vue.prototype.$DICT_TYPE = DICT_TYPE
selfAdaption() //自适应
setupStore() //初始化用户数据
setupInfo()

import { formatFuncRole } from "./utils"
Vue.prototype.$formatFR = formatFuncRole

Vue.config.productionTip = false
Vue.prototype.$code = "10000"

new Vue({
  router,
  store,
  render: (h) => h(App)
}).$mount("#app")
