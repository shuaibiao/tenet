import Vue from "vue"
import VueRouter from "vue-router"
import Layout from "@/views/layout"
import redirectRouter from "./redirectRouter"

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch((err) => err)
}

// replace
const originalReplace = VueRouter.prototype.replace
VueRouter.prototype.replace = function replace(location) {
  return originalReplace.call(this, location).catch((err) => err)
}
Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    component: Layout,
    hidden: true,
    redirect: "/redirect",
    meta: { title: "工作台首页", affix: true },
    children: redirectRouter
  },
  {
    path: "/redirect",
    component: Layout,
    redirect: "/redirect/index"
  },
  {
    path: "/login",
    component: () => import("@/views/login/index"),
    meta: { title: "登录", affix: true }
  }
]

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
})

export default router
