export default [
  {
    path: "/redirect/index",
    component: () => import("@/views/layout/Redirect/index"),
    meta: { title: "首页", affix: true, icon: "el-icon-s-home" },
    children: []
  },
  {
    path: "/noticeCenter",
    component: () => import("@/views/layout/Redirect/norticeCt"),
    meta: { title: "通知中心", affix: true, icon: "el-icon-chat-line-round" },
    children: []
  }
]
