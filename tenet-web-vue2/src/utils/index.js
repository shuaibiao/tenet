import store from "@/store"

//html fontsize
export function selfAdaption() {
  window.onresize = setHtmlFontSize
  window.onload = setHtmlFontSize
  function setHtmlFontSize() {
    const htmlWidth = document.documentElement.offsetWidth || window.innerWidth
    const htmlDom = document.documentElement
    htmlDom.style.fontSize = htmlWidth / 10 + "px"
  }
  setHtmlFontSize()
}
//数据权限解析
export function formatFuncRole(role) {
  const { permissionCodes } = store.getters.userInfo
  if (permissionCodes[0] === "*") return true
  const info = permissionCodes.find((e) => e === role)
  return info ? true : false
}
/**
 * @param {Function} func
 * @param {number} wait
 * @param {boolean} immediate
 * @return {*}
 */
export function debounce(func, wait, immediate) {
  let timeout, args, context, timestamp, result

  const later = function () {
    // 据上一次触发时间间隔
    const last = +new Date() - timestamp

    // 上次被包装函数被调用时间间隔 last 小于设定时间间隔 wait
    if (last < wait && last > 0) {
      timeout = setTimeout(later, wait - last)
    } else {
      timeout = null
      // 如果设定为immediate===true，因为开始边界已经调用过了此处无需调用
      if (!immediate) {
        result = func.apply(context, args)
        if (!timeout) context = args = null
      }
    }
  }

  return function (...args) {
    context = this
    timestamp = +new Date()
    const callNow = immediate && !timeout
    // 如果延时不存在，重新设定延时
    if (!timeout) timeout = setTimeout(later, wait)
    if (callNow) {
      result = func.apply(context, args)
      context = args = null
    }

    return result
  }
}
export function debounce2(func, delay) {
  var timeout
  return function () {
    clearTimeout(timeout)
    var context = this,
      args = arguments
    timeout = setTimeout(function () {
      func.apply(context, args)
    }, delay)
  }
}
