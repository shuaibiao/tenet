import Vue from "vue"

//全局组件
import BasePagination from "@/components/BasePagination"
import BaseDialog from "@/components/BaseDialog"
import BaseDrawer from "@/components/BaseDrawer"
import SvgIcon from "@/components/SvgIcon"
import Pagination from "@/components/Pagination"

// register globally

import ElementUI from "@/plugins/ElementUI.js"
import Avue from "@/plugins/Avue"
import iconPicker from "@/plugins/IconPicker"

Vue.use(ElementUI)
Vue.use(Avue)
Vue.use(iconPicker)

Vue.component("base-pagination", BasePagination)
Vue.component("base-dialog", BaseDialog)
Vue.component("base-drawer", BaseDrawer)
Vue.component("svg-icon", SvgIcon)
Vue.component("Pagination", Pagination)
