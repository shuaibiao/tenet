//本地缓存变量
const TOKEN = "token"
const TCOLOR = "themeColor"
const USERINFO = "userInfo"
const POINTPARAMS = "pointParams"
const DICT = "dict"

//图片预览路径
//文件上传
const UPLOAD_PATH = process.env.VUE_APP_BASE_URL + "/system/file/upload"
// 文件下载路径
const DOWNLOADAPI = `http://${location.host}${process.env.VUE_APP_BASE_URL}/sysfile`
//定时任务执行策略
const DATASCOPE = [
  {
    label: "全部",
    value: "0"
  },
  {
    label: "本级",
    value: "1"
  },
  {
    label: "下级",
    value: "2"
  },
  {
    label: "本级及下级",
    value: "3"
  },
  {
    label: "本人",
    value: "4"
  },
  {
    label: "自定义",
    value: "5"
  }
]
const MISFIREPOLICY = [
  {
    label: "默认",
    value: "0"
  },
  {
    label: "立即触发执行",
    value: "1"
  },
  {
    label: "触发一次执行",
    value: "2"
  },
  {
    label: "bu触发立即执行",
    value: "3"
  }
]
const THEMECOLOR = "#0ebf9c"
export {
  TOKEN,
  USERINFO,
  UPLOAD_PATH,
  DOWNLOADAPI,
  TCOLOR,
  THEMECOLOR,
  DICT,
  POINTPARAMS,
  MISFIREPOLICY,
  DATASCOPE
}
