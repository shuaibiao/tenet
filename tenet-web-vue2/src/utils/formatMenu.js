import Layout from "@/views/layout"
import ParentView from "@/components/ParentView"
import IframePage from "@/components/IframePage"

const loadView = (view) => {
  return (resolve) => require([`@/views${view}`], resolve)
}

export function menu2Router(menuList, first) {
  const propsDefault = {
    name: "name",
    path: "path",
    icon: "icon",
    children: "children",
    meta: "meta",
    keepAlive: "keepAlive",
    openType: "openType"
  }
  if (menuList.length === 0) return
  return menuList.map((menu) => {
    const path = (() => {
      if (!menu[propsDefault.path]) {
        return
      } else if (menu[propsDefault.path].substr(0, 4) == "http") {
        return "/iframe/" + menu.id
      } else {
        return menu[propsDefault.path]
      }
    })()

    const meta = {
      keepAlive: menu[propsDefault.keepAlive] == "N" ? false : true,
      title: menu[propsDefault.name],
      icon: menu[propsDefault.icon],
      openType: menu[propsDefault.openType],
      url: menu[propsDefault.path].substr(0, 4) == "http" ? menu[propsDefault.path] : "",
      id: menu.id
    }
    const children = menu[propsDefault.children]
    const isChild = children && children.length !== 0
    const router = {
      path,
      meta,
      component: (() => {
        if (first) {
          return Layout
        } else if (isChild && !first) {
          return ParentView
        } else if (path.indexOf("/iframe") !== -1) {
          return IframePage
        } else {
          return loadView(path)
        }
      })(),
      children: isChild ? (() => menu2Router(children, false))() : []
    }
    return router
  })
}

//获取该path在菜单中的index
export function menuToMenuIndex(routes, path) {
  let num = 0
  const res = []
  routes.forEach((e) => {
    const Arr = []
    const _recurseGetPath = (menu) => {
      menu.forEach((item) => {
        if (item.children.length) {
          _recurseGetPath(item.children)
        } else {
          Arr.push(item.path)
        }
      })
    }
    _recurseGetPath(e.children)
    res.push(Arr)
  })
  num = res.findIndex((e) => e.includes(path))
  return num
}

//获取layout路径path
export function routesToName(route) {
  const pathArr = []
  const _recurseGetPath = (menus) => {
    for (const menu of menus) {
      if (menu.children.length) {
        _recurseGetPath(menu.children)
      } else {
        pathArr.push(menu.path)
      }
    }
  }
  _recurseGetPath(route.children)
  return pathArr[0]
}

//解析component
// export function resolveComRoute(route) {
//   const pathArr = []
//   const _recurseGetPath = (menus) => {
//     for (const menu of menus) {
//       if (menu.children.length) {
//         _recurseGetPath(menu.children)
//       } else {
//         // pathArr.push(menu.component)
//         // console.log(Object.prototype.toString.call(menu.component))
//         // console.log(menu.component)
//       }
//     }
//   }
//   _recurseGetPath(route)
//   return pathArr[0]
// }

//获取所有菜单路径及名字（父目录名字+菜单名字）
export function routesToNP(route) {
  const namePathArr = []
  const _recurse = (menus) => {
    for (const menu of menus) {
      if (menu.children.length) {
        menu.children.forEach((e) => {
          if (!e.children.length) {
            namePathArr.push({
              path: e.path,
              name: [menu.meta.title, e.meta.title]
            })
          }
        })
        _recurse(menu.children)
      }
    }
  }
  _recurse(route)
  return namePathArr
}
