export default {
  IOT_UNIT: "IOT_UNIT", //单位
  SMS_CHANNEL_CODE: "sms_channel_code", // 短信渠道类型
  ROBOT_CHANNEL_CODE: "robot_channel_code", //群聊机器人
  APP_TYPE: "APP_TYPE", //应用类别
  MOBILE_APP_TYPE: "mobile_app_type" //应用类别
}
