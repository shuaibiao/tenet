/**
 * @desc  [自定义校验规则]
 * @example
 *  import { validateRule } from "@/utils/validateRules";
 *  rules: [
 *     { validator: validateRule.emailValue, trigger: 'blur'}
 *  ]
 */

export function validateDomain(rule, value, callback) {
  let reg =
    /^((http:\/\/)|(https:\/\/))?[a-zA-Z0-9][-a-zA-Z0-9]{0,62}(\.[a-zA-Z0-9][-a-zA-Z0-9]{0,62})+\.?/
  if (!value) {
    callback(new Error("域名不能为空"))
  } else if (!reg.test(value)) {
    callback(new Error("请输入正确格式的域名"))
  } else {
    callback()
  }
}
export function checkPhone(rule, value, callback) {
  const reg = /^1[3|4|5|7|8|9][0-9]{9}$/
  if (!value) {
    callback()
  } else {
    setTimeout(() => {
      if (!Number.isInteger(+value)) {
        callback(new Error("请输入数字值"))
      } else {
        if (reg.test(value)) {
          callback()
        } else {
          callback(new Error("电话号码格式不正确"))
        }
      }
    }, 100)
  }
}
export function checkEmail(rule, value, callback) {
  const reg = /^([a-z.A-Z0-9_-])+@([a-zA-Z0-9_-])+(.[a-zA-Z0-9_-])+/
  if (!value) {
    callback()
  } else {
    setTimeout(() => {
      if (reg.test(value)) {
        callback()
      } else {
        callback(new Error("请输入正确的邮箱格式"))
      }
    }, 100)
  }
}
/**
 * 校验 请输入中文、英文、数字包括下划线
 * 名称校验
 */
export function validatorNameCn(rule, value, callback) {
  let reg = /^[\u4E00-\u9FA5A-Za-z0-9_]+$/
  if (value && !reg.test(value)) {
    callback(new Error("请输入中文、英文、数字包括下划线"))
  } else {
    callback()
  }
}
/**
 * 校验首尾空白字符的正则表达式
 *
 */
export function checkSpace(rule, value, callback) {
  let reg = /[^\s]+$/
  if (!reg.test(value)) {
    callback(new Error("请输入非空格信息"))
  } else {
    callback()
  }
}
/**
 * 用户名
 */
//字母、数字、下划线组成，字母开头，4-16位
export function validatorAccount(rule, value, callback) {
  let reg = /^([0-9a-zA-Z_]{5,20}$)/
  if (!value) {
    callback(new Error("请输入账号"))
  } else if (!reg.test(value)) {
    callback(new Error("请输入正确格式的账号"))
  } else {
    callback()
  }
}
//密码
export function validatePassword(rule, value, callback) {
  let reg = /(?!^(\d+|[a-zA-Z]+|[~!@#$%^&*()_.]+)$)^[\w~!@#$%^&*()_.]{6,20}$/
  if (!value) {
    callback(new Error("请输入密码"))
  } else if (!reg.test(value)) {
    callback(new Error("请输入正确格式的密码"))
  } else {
    callback()
  }
}
//正数、最多2位小数
export function validateNum(rule, value, callback) {
  let reg = /^[0-9]+(.[0-9]{1,2})?$/
  if (!value) {
    callback(new Error("请输入数值"))
  } else if (!reg.test(value)) {
    callback(new Error("请输入正确格式的数值，最多保留两位小数"))
  } else {
    callback()
  }
}
//ipAddr
export function validateipAddr(rule, value, callback) {
  let reg =
    /^(?:(?:1[0-9][0-9]\.)|(?:2[0-4][0-9]\.)|(?:25[0-5]\.)|(?:[1-9][0-9]\.)|(?:[0-9]\.)){3}(?:(?:1[0-9][0-9])|(?:2[0-4][0-9])|(?:25[0-5])|(?:[1-9][0-9])|(?:[0-9]))$/
  if (!value) {
    callback(new Error("请输入ip"))
  } else if (!reg.test(value)) {
    callback(new Error("请输入正确格式的ip"))
  } else {
    callback()
  }
}
export function validatecarNumber(rule, value, callback) {
  if (!value) {
    return callback(new Error("车牌号码不能为空"))
  } else {
    const reg =
      /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/
    if (reg.test(value)) {
      callback()
    } else {
      return callback(new Error("请输入正确的车牌号码"))
    }
  }
}
