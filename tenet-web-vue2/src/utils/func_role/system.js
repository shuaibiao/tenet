const FUNCROLELIST = {
  user: {
    add: "sysuser.add",
    edit: "sysuser.edit",
    del: "sysuser.del",
    role: "sys.user.assign-roles",
    reset: "sysuser.reset"
  },
  menu: {
    add: "sysmenu.add",
    edit: "sysmenu.edit",
    del: "sysmenu.del"
  },
  dept: {
    add: "sysdept.add",
    edit: "sysdept.edit",
    del: "sysdept.del"
  },
  role: {
    add: "sysrole.add",
    edit: "sysrole.edit",
    del: "sysrole.del",
    data: "sysrole.data",
    func: "sysrole.func"
  },
  dic: {
    add: "sys.dic.type.add",
    edit: "sys.dic.type.edit",
    del: "sys.dic.type.del",
    cache: "sys.dic.type.refresh",
    data: "sys.dic.data"
  },
  dic_data: {
    add: "sys.dic.data.add",
    edit: "sys.dic.data.edit",
    del: "sys.dic.data.del"
  },
  config: {
    add: "sysconfig.add",
    edit: "sysconfig.edit",
    del: "sysconfig.del",
    cache: "sysconfig.cache"
  },
  log: {
    del: "syslog.del"
  },
  timedtask: {
    add: "sysjob.add",
    edit: "sysjob.edit",
    del: "sysjob.del",
    one: "sysjob.one",
    log: "sysjob.log"
  },
  file: {
    add: "sysfile.add",
    down: "sysfile.down",
    del: "sysfile.del"
  },
  notice: {
    add: "sysnotice.add",
    edit: "sysnotice.edit",
    pub: "sysnotice.pub",
    del: "sysnotice.del"
  },
  configureManage: {
    conf: "sysconfigure.conf"
  }
}
export default FUNCROLELIST
