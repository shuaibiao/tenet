const FUNCROLELIST = {
  mail: {
    account: {
      add: "mail-account.add",
      edit: "mail-account.edit",
      del: "mail-account.del"
    },
    template: {
      add: "mail-template.add",
      edit: "mail-template.edit",
      test: "mail-template.test",
      del: "mail-template.del"
    },
    log: {
      detail: "mail-log.detail"
    }
  },
  sms: {
    channel: {
      add: "sms-channel.add",
      edit: "sms-channel.edit",
      del: "sms-channel.del"
    },
    template: {
      add: "sms-template.add",
      edit: "sms-template.edit",
      test: "sms-template.test",
      del: "sms-template.del"
    },
    log: {
      detail: "sms-log.detail"
    }
  },
  robot: {
    channel: {
      add: "robot-channel.add",
      edit: "robot-channel.edit",
      del: "robot-channel.del"
    },
    template: {
      add: "robot-template.add",
      edit: "robot-template.edit",
      del: "robot-template.del"
    },
    log: {
      detail: "robot-log.detail"
    }
  }
}
export default FUNCROLELIST
