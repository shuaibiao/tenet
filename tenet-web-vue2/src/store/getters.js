const getters = {
  //user
  token: (state) => state.user.token,
  userInfo: (state) => state.user.userInfo,
  //permission
  routes: (state) => state.permission.routes,
  sidebarMenu: (state) => state.permission.sidebarMenu,
  //dic
  dictList: (state) => {
    return function (v) {
      return state.dict.dictList[v]
    }
  },
  //notice
  noticeValue: (state) => state.notice.noticeValue
  //settings
}
export default getters
