import { THEMECOLOR, TCOLOR } from "@/utils/settings"
export default {
  namespaced: true,
  state: {
    theme: localStorage.getItem(TCOLOR) || THEMECOLOR
  },
  mutations: {
    SET_COLOR(state, payload) {
      localStorage.setItem(TCOLOR, payload)
      state.theme = payload
    }
  }
}
