import { dicDataList } from "@/api/login/index"
import { DICT } from "@/utils/settings"
export default {
  namespaced: true,
  state: {
    dictList: JSON.parse(localStorage.getItem(DICT)) || {}
  },
  mutations: {
    SET_DICT(state, payload) {
      const data = {}
      payload.forEach((e) => {
        const item = {
          ...e,
          value: e.dicValue,
          label: e.dicLabel
        }
        if (!data[e.dicCode]) {
          data[e.dicCode] = []
        }
        data[e.dicCode].push(item)
      })
      state.dictList = data
      localStorage.setItem(DICT, JSON.stringify(data))
    }
  },
  actions: {
    async getDictAction({ commit }) {
      const { data } = await dicDataList()
      commit("SET_DICT", data)
    }
  }
}
