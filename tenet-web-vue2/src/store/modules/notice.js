import { notReadNum } from "@/api/system/notice"
export default {
  namespaced: true,
  state: {
    noticeValue: 0
  },
  mutations: {
    SET_NUM(state, payload) {
      state.noticeValue = Number(payload)
    }
  },
  actions: {
    async getNoticeNumAction({ commit }) {
      const { data } = await notReadNum()
      commit("SET_NUM", data)
    }
  }
}
