import Vue from "vue"
import Vuex from "vuex"
import getters from "./getters"

Vue.use(Vuex)

const modules = {}
//动态加载模块
const modulesFiles = require.context("./modules", true, /\.js$/)
modulesFiles.keys().map((key) => {
  // 获取名字
  //$1 and $2 correspond to the contents captured by the small regular expressions in the parentheses in order.
  const moduleName = key.replace(/^\.\/(.*)\.\w+$/, "$1")
  //获取模块
  const module = require(`./modules/${moduleName}.js`)
  modules[`${moduleName}`] = module.default
})

const store = new Vuex.Store({
  modules,
  getters
})

export function setupStore() {
  store.dispatch("user/getInitialDataAction")
}
export function setupInfo() {
  store.dispatch("dict/getDictAction")
}

export default store
