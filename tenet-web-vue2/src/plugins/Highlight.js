import hljs from "highlight.js/lib/core"
import "highlight.js/styles/github.css"

import xml from "highlight.js/lib/languages/xml.js"
hljs.registerLanguage("xml", xml)

export default hljs
