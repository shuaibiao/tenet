import router from "./router"
import store from "./store"
import NProgress from "@/plugins/NProgress"
import { TOKEN } from "./utils/settings"

NProgress.configure({ showSpinner: false }) // NProgress Configuration

const whiteList = ["/login"]

router.beforeEach((to, from, next) => {
  NProgress.start()
  if (localStorage.getItem(TOKEN)) {
    /* has token*/
    if (to.path === "/login") {
      next({ path: "/" })
      NProgress.done()
    } else {
      if (!store.getters.userInfo) {
        store.dispatch("user/getInitialDataAction")
      } else {
        next()
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      next()
    } else {
      next(`/login`)
      NProgress.done()
    }
  }
})

router.afterEach(() => {
  // finish progress bar
  NProgress.done()
})
