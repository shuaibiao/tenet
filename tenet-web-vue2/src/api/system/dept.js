import request from "../index"

// 部门树
// GET /system/dept/tree
// 接口ID：63848407
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848407
export function treeList(params) {
  return request({
    url: "/system/dept/tree",
    method: "get",
    params
  })
}
// 新增部门
// POST /system/dept
// 接口ID：63848408
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848408
export function add(data) {
  return request({
    url: "/system/dept",
    method: "post",
    data
  })
}
// 修改部门
// PUT /system/dept
// 接口ID：63848409
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848409
export function edit(data) {
  return request({
    url: "/system/dept",
    method: "put",
    data
  })
}
// 删除部门
// DELETE /system/dept/{id}
// 接口ID：63848410
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848410
export function del(id) {
  return request({
    url: "/system/dept/" + id,
    method: "delete"
  })
}
//我的部门树
// export function myTree() {
//   return request({
//     url: "/sysdept/myTree",
//     method: "get"
//   })
// }
