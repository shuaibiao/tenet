import request from "../index"

//列表
export function logList(params) {
  return request({
    url: "/system/log/page",
    method: "get",
    params
  })
}
//删除
export function del(id) {
  return request({
    url: "/system/log/" + id,
    method: "delete"
  })
}
