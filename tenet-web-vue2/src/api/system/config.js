import request from "../index"

//列表
export function paramsList(params) {
  return request({
    url: "/system/config/page",
    method: "get",
    params
  })
}
//保存
export function add(data) {
  return request({
    url: "/system/config",
    method: "post",
    data
  })
}
//编辑
export function edit(data) {
  return request({
    url: "/system/config",
    method: "put",
    data
  })
}
//删除
export function del(id) {
  return request({
    url: "/system/config/" + id,
    method: "delete"
  })
}
//获取系统参数
export function getSysParam(key) {
  return request({
    url: "/system/config/" + key,
    method: "get"
  })
}
//刷新缓存
export function refresh() {
  return request({
    url: "/system/config/refresh",
    method: "get"
  })
}
