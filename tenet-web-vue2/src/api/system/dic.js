import request from "../index"

//列表
export function dicList(params) {
  return request({
    url: "/system/dic/type/page",
    method: "get",
    params
  })
}

// 字典项列表
export function dicData(params) {
  return request({
    url: "/system/dic/data/page",
    method: "get",
    params
  })
}

//保存
export function add(data) {
  return request({
    url: "/system/dic/type",
    method: "post",
    data
  })
}

//保存字典项
export function addData(data) {
  return request({
    url: "/system/dic/data",
    method: "post",
    data
  })
}

//编辑
export function edit(data) {
  return request({
    url: "/system/dic/type",
    method: "put",
    data
  })
}

//编辑字典项
export function editData(data) {
  return request({
    url: "/system/dic/data",
    method: "put",
    data
  })
}

//删除
export function del(id) {
  return request({
    url: "/system/dic/type/" + id,
    method: "delete"
  })
}

//删除字典项
export function delData(id) {
  return request({
    url: "/system/dic/data/" + id,
    method: "delete"
  })
}

//刷新缓存
export function refresh() {
  return request({
    url: "/system/dic/data/refresh",
    method: "get"
  })
}
