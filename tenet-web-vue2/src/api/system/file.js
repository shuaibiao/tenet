import request from "../index"

//文件列表
export function fileList(params) {
  return request({
    url: "/system/file/page",
    method: "get",
    params
  })
}
//文件 //elementui自动则不走此接口
export function uploadFile(data) {
  return request({
    url: "/system/file/upload",
    method: "post",
    data
  })
}
//文件下载
// export function downloadFile(bucketName, fileName) {
//   return request({
//     url: `/sysfile/${bucketName}/${fileName}`,
//     method: "get"
//   })
// }
// //本地文件下载（excel模板、默认头像等）
// export function downloadLocalFile(fileName) {
//   return request({
//     url: `/sysfile/local/${fileName}`,
//     method: "get"
//   })
// }
//删除文件
export function delFile(id) {
  return request({
    url: `/system/file/delete`,
    method: "delete",
    params: {
      id
    }
  })
}
