import request from "../index"

// 分页查询
// GET /system/user/page
// 接口ID：63848425
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848425
export function userPage(params) {
  return request({
    url: "/system/user/page",
    method: "get",
    params
  })
}
// 新增用户
// POST /system/user
// 接口ID：63848426
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848426
export function add(data) {
  return request({
    url: "/system/user",
    method: "post",
    data
  })
}
// 编辑用户
// PUT /system/user
// 接口ID：63848427
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848427
export function edit(data) {
  return request({
    url: "/system/user",
    method: "put",
    data
  })
}
// 删除用户
// DELETE /system/user/{id}
// 接口ID：63848428
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848428
export function del(id) {
  return request({
    url: "/system/user/" + id,
    method: "delete"
  })
}
// 重置密码
// PUT /system/user/reset/{id}
// 接口ID：63848429
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848429
export function reset(id) {
  return request({
    url: "/system/user/reset/" + id,
    method: "put"
  })
}
// 设置角色
// POST /system/user/assignRoles
// 接口ID：64387369
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-64387369
export function assignRoles(data) {
  return request({
    url: "/system/user/assignRoles",
    method: "post",
    data
  })
}
