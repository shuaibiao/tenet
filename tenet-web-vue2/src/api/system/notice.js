import request from "../index"

//通知
export function page(params) {
  return request({
    url: "/system/notice/page",
    method: "get",
    params
  })
}
//根据id拿详情
export function getById(id) {
  return request({
    url: "/system/notice/" + id,
    method: "get"
  })
}
//新增通知
export function add(data) {
  return request({
    url: "/system/notice",
    method: "post",
    data
  })
}
//修改通知
export function edit(data) {
  return request({
    url: "/system/notice",
    method: "put",
    data
  })
}
//删除
export function del(id) {
  return request({
    url: "/system/notice/" + id,
    method: "delete"
  })
}
//未读条数
export function notReadNum() {
  return request({
    url: "/system/notice/myNotReadCount",
    method: "get"
  })
}
//通知中心
export function isReadList() {
  return request({
    url: "/system/notice/myNotice",
    method: "get"
  })
}
//已读
export function reader(id) {
  return request({
    url: "/system/notice/read/" + id,
    method: "post"
  })
}
