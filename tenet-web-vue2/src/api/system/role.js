import request from "../index"

// 分页查询
// GET /system/role/page
// 接口ID：63848415
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848415
export function rolePage(params) {
  return request({
    url: "/system/role/page",
    method: "get",
    params
  })
}
// 新增角色
// POST /system/role
// 接口ID：63848416
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848416
export function add(data) {
  return request({
    url: "/system/role",
    method: "post",
    data
  })
}
// 编辑角色
// PUT /system/role
// 接口ID：63848417
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848417
export function edit(data) {
  return request({
    url: "/system/role",
    method: "put",
    data
  })
}
// 删除角色
// DELETE /system/role/{id}
// 接口ID：63848419
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848419
export function del(id) {
  return request({
    url: "/system/role/" + id,
    method: "delete"
  })
}

//查询所有角色
//
//
//
export function roleList() {
  return request({
    url: "/system/role/list",
    method: "get"
  })
}

// 设置数据权限
// POST /system/role/scope/data
// 接口ID：63848422
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848422
export function setRoleData(data) {
  return request({
    url: "/system/role/scope/data",
    method: "post",
    data
  })
}
// 查询角色功能权限
// GET /system/role/scope/function/{roleId}
// 接口ID：63848423
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848423
export function getRoleFeatureById(roleId) {
  return request({
    url: "/system/role/scope/function/" + roleId,
    method: "get"
  })
}
// 设置功能权限
// POST /system/role/scope/function
// 接口ID：63848424
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848424
export function setRoleFeature(data) {
  return request({
    url: "/system/role/scope/function",
    method: "post",
    data
  })
}
