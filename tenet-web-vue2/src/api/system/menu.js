import request from "../index"

//菜单列表
export function menuList(params) {
  return request({
    url: "/system/menu/tree",
    method: "get",
    params
  })
}
//保存菜单
export function add(data) {
  return request({
    url: "/system/menu",
    method: "post",
    data
  })
}
//编辑菜单
export function edit(data) {
  return request({
    url: "/system/menu",
    method: "put",
    data
  })
}
//删除菜单
export function del(id) {
  return request({
    url: "/system/menu/" + id,
    method: "delete"
  })
}
