import request from "../index"

// 获取滑块验证码
// POST /captcha/get
// 接口ID：63848401
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848401
export function reqGet(data) {
  return request(
    {
      url: "/system/captcha/get",
      method: "post",
      data
    },
    true
  )
}
// 滑块验证码验证
// POST /captcha/check
// 接口ID：64074832
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-64074832
export function reqCheck(data) {
  return request(
    {
      url: "/system/captcha/check",
      method: "post",
      data
    },
    true
  )
}
// 滑块验证码登录
// POST /auth/captcha-login
// 接口ID：63848405
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848405
export function login(data, params) {
  return request(
    {
      url: "/system/auth/captcha-login",
      method: "post",
      data,
      params
    },
    true
  )
}
// 退出
// GET /auth/logout
// 接口ID：63848406
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848406
export function logout() {
  return request({
    url: "/system/auth/logout",
    method: "get"
  })
}
// 个人信息
// GET /user/myInfo
// 接口ID：63848403
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848403
export function userInfo() {
  return request({
    url: "/system/user/myInfo",
    method: "get"
  })
}
// 编辑个人信息
// POST /user/editMyInfo
// 接口ID：63848402
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848402
export function editMyInfo(data) {
  return request({
    url: "/system/user/editMyInfo",
    method: "post",
    data
  })
}
// 我的菜单树
// GET /menu/me
// 接口ID：63848404
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-63848404
// export function myMenu() {
//   return request({
//     url: "/system/menu/me",
//     method: "get"
//   })
// }

// 全局字典
// GET /dic/data/list
// 接口ID：66910239
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-66910239
export function dicDataList() {
  return request({
    url: "/system/dic/data/list",
    method: "get"
  })
}

// 全局单个字典
// GET /dic/data/list/{dicCode}
// 接口ID：67044542
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67044542
// export function dicDataOne(data) {
//   return request({
//     url: "/system/dic/data/list/" + data,
//     method: "get"
//   })
// }

//我的菜单树
export function myMenu() {
  return request({
    url: "/system/menu/me",
    method: "get"
  })
}
