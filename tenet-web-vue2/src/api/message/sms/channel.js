import request from "../../index"

// 创建短信渠道
export function add(data) {
  return request({
    url: "/message/sms/channel",
    method: "post",
    data: data
  })
}

// 更新短信渠道
export function edit(data) {
  return request({
    url: "/message/sms/channel",
    method: "put",
    data: data
  })
}

// 删除短信渠道
export function del(id) {
  return request({
    url: "/message/sms/channel/" + id,
    method: "delete"
  })
}

// 获得短信渠道
export function getSmsChannel(id) {
  return request({
    url: "/system/sms-channel/get?id=" + id,
    method: "get"
  })
}

// 获得短信渠道分页
export function page(query) {
  return request({
    url: "/message/sms/channel/page",
    method: "get",
    params: query
  })
}

// 精简列表
export function simplePage() {
  return request({
    url: "/message/sms/channel/list",
    method: "get"
  })
}
