import request from "../../index"

// 创建短信模板
export function add(data) {
  return request({
    url: "/message/sms/template",
    method: "post",
    data: data
  })
}

// 更新短信模板
export function edit(data) {
  return request({
    url: "/message/sms/template",
    method: "put",
    data: data
  })
}

// 删除短信模板
export function del(id) {
  return request({
    url: "/message/sms/template/" + id,
    method: "delete"
  })
}

// // 获得短信模板
// export function getSmsTemplate(id) {
//   return request({
//     url: "/system/sms-template/get?id=" + id,
//     method: "get"
//   })
// }

// 获得短信模板分页
export function page(query) {
  return request({
    url: "/message/sms/template/page",
    method: "get",
    params: query
  })
}

export function send(data) {
  return request({
    url: "/message/sms/template/send-sms",
    method: "post",
    data
  })
}

// 简单查询
export function selectTemplateSms() {
  return request({
    url: "/message/sms/template/list",
    method: "get"
  })
}
