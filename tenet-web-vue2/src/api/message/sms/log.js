import request from "../../index"

//分页
export function page(query) {
  return request({
    url: "/message/sms/log/page",
    method: "get",
    params: query
  })
}
