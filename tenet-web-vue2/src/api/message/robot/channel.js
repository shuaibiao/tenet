import request from "../../index"

//消息机器人渠道分页
export function page(query) {
  return request({
    url: "/message/robot/channel/page",
    method: "get",
    params: query
  })
}

// 创建消息机器人渠道
export function add(data) {
  return request({
    url: "/message/robot/channel",
    method: "post",
    data: data
  })
}

// 修改消息机器人渠道
export function edit(data) {
  return request({
    url: "/message/robot/channel",
    method: "put",
    data: data
  })
}
// 删除消息机器人渠道
export function del(id) {
  return request({
    url: "/message/robot/channel/" + id,
    method: "delete"
  })
}

// 精简列表
export function simplePage() {
  return request({
    url: "/message/robot/channel/list",
    method: "get"
  })
}
