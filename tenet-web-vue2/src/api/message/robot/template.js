import request from "../../index"

// 消息机器人模板分页
export function page(query) {
  return request({
    url: "/message/robot/template/page",
    method: "get",
    params: query
  })
}

// 创建消息机器人模板
export function add(data) {
  return request({
    url: "/message/robot/template",
    method: "post",
    data: data
  })
}

// 修改消息机器人模板
export function edit(data) {
  return request({
    url: "/message/robot/template",
    method: "put",
    data: data
  })
}
// 删除消息机器人模板
export function del(id) {
  return request({
    url: "/message/robot/template/" + id,
    method: "delete"
  })
}
// 消息机器人模板测试
export function send(data) {
  return request({
    url: "/message/robot/template/send-msg",
    method: "post",
    data
  })
}
// 简单查询
export function selectTemplateRobot() {
  return request({
    url: "/message/robot/template/list",
    method: "get"
  })
}
