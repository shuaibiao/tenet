import request from "../../index"

// 获得邮件日志
export function getById(id) {
  return request({
    url: "/message/mail/log/" + id,
    method: "get"
  })
}

// 获得邮件日志分页
export function page(query) {
  return request({
    url: "/message/mail/log/page",
    method: "get",
    params: query
  })
}
