import request from "../../index"

// 新增
// POST /message/mail/account
// 接口ID：67138583
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67138583
export function add(data) {
  return request({
    url: "/message/mail/account",
    method: "post",
    data: data
  })
}

// 修改
// PUT /message/mail/account
// 接口ID：67150149
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67150149
export function edit(data) {
  return request({
    url: "/message/mail/account",
    method: "put",
    data: data
  })
}

// 删除
// DELETE /message/mail/account/{id}
// 接口ID：67150617
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67150617
export function del(id) {
  return request({
    url: "/message/mail/account/" + id,
    method: "delete"
  })
}

// 查询单条
// GET /message/mail/account/{id}
// 接口ID：67104119
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67104119
export function getById(id) {
  return request({
    url: "/message/mail/account/" + id,
    method: "get"
  })
}

// 分页查询
// GET /message/mail/account/page
// 接口ID：67088773
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67088773
export function page(query) {
  return request({
    url: "/message/mail/account/page",
    method: "get",
    params: query
  })
}

// 简单查询
// GET /message/mail/account/list
// 接口ID：67099457
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67099457
export function getSimpleMailAccountList() {
  return request({
    url: "/message/mail/account/list",
    method: "get"
  })
}
