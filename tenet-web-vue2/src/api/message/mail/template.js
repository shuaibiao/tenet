import request from "../../index"

// 创建邮件模版
export function add(data) {
  return request({
    url: "/message/mail/template",
    method: "post",
    data: data
  })
}

// 更新邮件模版
export function edit(data) {
  return request({
    url: "/message/mail/template",
    method: "put",
    data: data
  })
}

// 删除邮件模版
export function del(id) {
  return request({
    url: "/message/mail/template/" + id,
    method: "delete"
  })
}

// 获得邮件模版
export function getById(id) {
  return request({
    url: "/message/mail/template/" + id,
    method: "get"
  })
}

// 获得邮件模版分页
export function page(query) {
  return request({
    url: "/message/mail/template/page",
    method: "get",
    params: query
  })
}

//wu
// 发送测试邮件
export function sendMail(data) {
  return request({
    url: "/system/mail-template/send-mail",
    method: "post",
    data: data
  })
}
// 简单查询
// GET /system/mail-template/list-all-simple
// 接口ID：67206829
// 接口地址：https://www.apifox.cn/web/project/2335920/apis/api-67206829
export function selectTemplateEmail() {
  return request({
    url: "/message/mail/template/list",
    method: "get"
  })
}
export function send(data) {
  return request({
    url: "/message/mail/template/send-msg",
    method: "post",
    data
  })
}
